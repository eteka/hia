<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrevesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('slug')->nullable();
            $table->string('chapeau')->nullable();
            $table->string('photo')->nullable();
            $table->longText('corps')->nullable();
            $table->boolean('etat')->nullable();
            $table->index(['id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('breves');
    }
}
