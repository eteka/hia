<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalerieImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galerie_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->longText('detail')->nullable();
            $table->boolean('etat');
            $table->integer('id_user');
            $table->index(['id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galerie_images');
    }
}
