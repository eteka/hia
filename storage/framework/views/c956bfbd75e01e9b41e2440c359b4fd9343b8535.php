<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/plugins/summernote/dist/summernote.css')); ?>" rel="stylesheet">
<style type="text/css">
.note-popover .popover-content, .panel-heading.note-toolbar{
    background: #eaeff5;
} 
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/plugins/summernote/dist/summernote.js')); ?>"></script>

<script type="text/javascript">
var $editor = $('.summernote');

$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
//    height: 250,
    height: 200,
    placeholder: 'Rédigez votre actualité',
    dialogsInBody: true,
    lang: 'fr-FR',
});


</script>

<?php $__env->stopSection(); ?>

<div class="form-group row  <?php echo e($errors->has('titre') ? 'has-error' : ''); ?>">
    <?php echo Form::label('titre', trans('actualite.titre'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-9">
        <?php echo Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]); ?>


        <?php echo $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<div class="form-group row  <?php echo e($errors->has('chapeau') ? 'has-error' : ''); ?>">
    <?php echo Form::label('chapeau', trans('actualite.chapeau'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-9">
        <?php echo Form::textarea('chapeau', null, ['class' => 'form-control form-control-sm','rows'=>3]); ?>


        <?php echo $errors->first('chapeau', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<div class="row">   
    <div class="col-md-6">   
<div class="form-group row  <?php echo e($errors->has('photo') ? 'has-error' : ''); ?>">
    <?php echo Form::label('photo', trans('actualite.photo'), ['class' => 'col-md-4 text-right control-label']); ?> 
    <div class="col-md-8">
        <?php echo Form::file('photo',  ['class' => 'form-control form-control-sm','rows'=>3]); ?>


        <?php echo $errors->first('photo', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
    </div>
    <div class="col-md-6">   
        <div class="form-group row  <?php echo e($errors->has('categorie') ? 'has-error' : ''); ?>">
        <?php echo Form::label('categorie_id', trans('actualite.categorie'), ['class' => 'col-md-4 text-right control-label']); ?> 
        <div class="col-md-6">
            <?php echo Form::select('categorie_id',isset($cats)?$cats:[], null, ['class' => 'custom-select custom-select-sm ']); ?>


            <?php echo $errors->first('categorie_id', '<p class="form-text text-danger help-block">:message</p>'); ?>

        </div>
    </div>
    </div>
</div>


<div class="form-group row  <?php echo e($errors->has('corps') ? 'has-error' : ''); ?>">
    <?php echo Form::label('corps', trans('actualite.corps'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-9">
        <?php echo Form::textarea('corps', null, ['class' => 'form-control form-control-sm summernote','rows'=>3]); ?>


        <?php echo $errors->first('corps', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<div class="form-group row  <?php echo e($errors->has('etat') ? 'has-error' : ''); ?>">
    <?php echo Form::label('etat', trans('actualite.etat'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-9">
        <div class="row">
 <div class="col-md-2">
        <div class="custom-control custom-radio ">
        <?php echo Form::radio('etat', '1', true,['id'=>'etat_i','class'=>"custom-control-input type_sit"]); ?>

          <label class="custom-control-label" for='etat_i'>Activé </label>
        </div>
</div>

 <div class="col-md-2">
        <div class="custom-control custom-radio ">
        <?php echo Form::radio('etat', '0', false,['id'=>'etat_j','class'=>"custom-control-input type_sit"]); ?>

          <label class="custom-control-label" for='etat_j'>Désactivé </label>
        </div>
</div>
</div>

        <?php echo $errors->first('etat', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>


<div class="form-group row">
	<div class="col-md-2">
	</div>
    <div class="col-md-4">
        <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']); ?>

    </div>
</div>
