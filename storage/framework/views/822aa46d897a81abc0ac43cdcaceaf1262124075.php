<?php $__env->startSection('title',isset($titre)?$titre:''." - Profil du compte"); ?>
<?php $__env->startSection('content'); ?>
<style>
	#head-profil{
		height: 230px;
		background: #fff
	}
	.img-border {
    border: 8px solid #edf5f3;
    box-shadow: 0px 0 5px #aaa;
}
.iborder{
	border:1px solid #dddddd;
}
#cover_title,#cover_title .text-muted,#cover_title p{
	color: #ffffff;
	text-shadow: 0 0 3px #000000;
}
#cover_titles{
	background: linear-gradient(to top,rgba(0,0,0,.8) 0,rgba(0,0,0,0) 100%);
	color: #ffffff;
	position: absolute;
	bottom: 0;
	width: 100%;
}
.nav-tabs .nav-link.active {
    border-left: 0.325rem solid #444bf8;
    border-top: 0 solid #444bf8;
    background: #f2f2f2;
    font-weight: 900;
    }
</style>
	<div class="row">

		<?php echo $__env->make('admin.includes.profil-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="col-lg-4">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header"> Paramètres du compte </h6>
                      <!-- .nav -->
                      <nav class="nav nav-tabs flex-column text-uppercases">
                        <a href="<?php echo e(route('profil',$compte->pseudo)); ?>" class="nav-link active"><span class="ti-user"></span> Profile</a>
                        <a href="<?php echo e(route('edit-profil',$compte->pseudo)); ?>" class="nav-link"><span class="ti-pencil"></span> Modifier mon compte </a>
                        <a href="<?php echo e(route('password',$compte->pseudo)); ?>" class="nav-link"><span class="ti-lock"></span>  Changer mot de passe</a>
                      </nav>
                      <!-- /.nav -->
                    </div>
                    <!-- /.card -->
        </div>
        <div class="col-lg-8">
                    <!-- .card -->
                     <?php echo Form::open(['route' => ['postprofil',$compte->pseudo], 'class' => 'form-horizontal', 'files' => true]); ?>


                    <div class="card card-fluid">
                      <h6 class="card-header"> Mon Profile </h6>
                      <!-- .card-body -->
                      <div class="card-body">
                      	 <?php if($errors->any()): ?>
                                <ul class="alert alert-danger">
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?>
                        <!-- .media -->
                        <div class="media mb-3">
                          <!-- avatar -->
                          <div class="user-avatar user-avatar-xl fileinput-button">
                            <a href="user-profile.html" class="user-avatar user-avatar-xl">
			                	<?php if(!empty(Auth::user()->image)): ?>
				                  <img class="_img-border rounded-circle iborder" height="100px" src="<?php echo e(asset(Auth::user()->image)); ?>" alt="<?php echo e(Auth::user()->prenom); ?>">
								<?php else: ?>
								<img class="_img-border rounded-circle iborder" height="100px" src="<?php echo e(asset('uploads/statics/default-image.jpg')); ?>" alt="<?php echo e(Auth::user()->prenom); ?>">
								<?php endif; ?>
			                </a>
                            <?php echo Form::file('photo',  ['class' => 'form-controlform-control-sm','id'=>"fileupload-avatar"]); ?>


                        </div>
                          <!-- /avatar -->
                          <!-- .media-body -->
                          <div class="media-body pl-3">
                            <h3 class="card-title"> Avatar public </h3>
                            <h6 class="card-subtitle text-muted"> Cliquez pour changer votre photo. </h6>
                            <p class="card-text">
                              <small>JPG, GIF or PNG 400x400, &lt; 2 MB.</small>
                            </p>
                            <!-- The avatar upload progress bar -->
                            <div id="progress-avatar" class="progress progress-xs fade">
                              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <!-- /avatar upload progress bar -->
                          </div>
                          <!-- /.media-body -->
                        </div>
                        <!-- /.media -->
                        <!-- form -->
                       
                          <?php echo e(csrf_field()); ?>

                          <div class="form-row">
                            <!-- form column -->
                            <label for="input01" class="col-md-3">Photo de couverture</label>
                            <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <div class="custom-file">
                              	<?php echo Form::file('couverture',  ['class' => 'custom-file-input','id'=>""]); ?>

                                <label class="custom-file-label" for="input01">Choose cover</label>
                              </div>
                              <small class="text-muted">Ajoutez une photo de couverture image, JPG 1200x300</small>
                            </div>
                            <!-- /form column -->
                          </div>
                          <!-- /form row -->
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input02" class="col-md-3">Entreprise</label>
                            <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <input type="text" name="entreprise" class="form-control" name="" id="input02" value="<?php echo e($compte->entreprise); ?>"> </div>
                            <!-- /form column -->
                          </div>
                          <!-- /form row -->
                          <!-- form row -->
                          <div class="form-row">
                            <!-- form column -->
                            <label for="input03" class="col-md-3">Profile Heading</label>
                            <!-- /form column -->
                            <!-- form column -->
                            <div class="col-md-9 mb-3">
                              <textarea type="text" name="poste" class="form-control" id="input03"><?php echo e($compte->poste); ?></textarea>
                              <small class="text-muted">La description de votre fonction en 300 chars max.</small>
                            </div>
                            <!-- /form column -->
                          </div>
                          <!-- /form row -->
                          <hr>
                          <!-- .form-actions -->
                          <div class="form-actions">
                            <button type="submit" class="btn btn-primary ml-auto">Modifier le profile</button>
                          </div>
                          <!-- /.form-actions -->
                        </form>
                        <!-- /form -->
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                  </div>
		
		
		
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>