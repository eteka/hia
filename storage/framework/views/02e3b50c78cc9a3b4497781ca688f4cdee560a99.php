<?php if(isset($data,$data->titre)): ?>
<?php $__env->startSection('title',$data->titre); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
							<div class="">
								<small class="<?php echo e(isset($color)?$color:'color-3'); ?> text-white pad10 text-small -category">MEMBRE DE L'ADMINISTRATION	</small>
								<h2 class="page-header"><?php echo e($data->nom." ".$data->prenom); ?></h2>
							</div>
							<?php if(!empty($data->photo)): ?>

							<div class="clearfix">
								<div class="img-crop">
									<img src="<?php echo e(asset($data->photo)); ?>" class="img-responsive" alt="">
									<div class="img-credits">
										<div class="col-md-10">
											<div class="post-title"><h1><?php echo e($data->nom." ".$data->prenom); ?></h1></div>
											<div class="post-info clearfix">
												
												<!--span class="sepr">-</span-->
												<span class="date"><i class="fa fa-user-o"></i> <?php echo e($data->poste); ?></span>
											</div>
										</div>
									</div>
								</div><!-- img-crop -->
							</div>
							<?php endif; ?>
							
							<div class="row" style="transform: none;">
								<!-- Post Content Start -->
								<div class="fbt-col-lg-9 col-md-12 single-post-container clearfix">
									<div class="row">
										<div class="col-md-12">
											
											<div class="clearfix"></div>
											

											<div class="post-text-content clearfix">
												<?php echo $data->biographie; ?>

											</div>

											
											<div class="clearfix"></div>
									
											
											
											<!--div class="post-share bottom clearfix">
												<ul>
													<li><a class="facebook df-share" data-sharetip="Share on Facebook!" href="#" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i> <span class="social-text">Facebook</span></a></li>
													<li><a class="twitter df-share" data-hashtags="" data-sharetip="Share on Twitter!" href="#" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> <span class="social-text">Tweeter</span></a></li>
													<li><a class="google df-pluss" data-sharetip="Share on Google+!" href="#" rel="nofollow" target="_blank"><i class="fa fa-google-plus"></i> <span class="social-text">Google+</span></a></li>
													<li><a class="pinterest df-pinterest" data-sharetip="Pin it" href="#" target="_blank"><i class="fa fa-pinterest-p"></i> <span class="social-text">Pinterest</span></a></li>
												</ul>
											</div--><!-- Post Share Bottom End -->
											
											
											
											<!-- Related Posts Start -->
											<div class="fbt-related-posts clearfix">
												<!--div class="title-wrapper color-3">
													<h2><span>You Might Also Like</span></h2>
												</div-->
												
												<!-- Carousel Start -->
												<!--div class="main-carousel">
													<div class="carousel-content-box owl-wrapper clearfix">
														<div class="owl-carousel owl-theme" data-num="3" style="opacity: 1; display: block;">
															<div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 2370px; left: 0px; display: block;"><div class="owl-item" style="width: 237px;"><div class="item fbt-hr-crs">
																<div class="post-item clearfix">
																	<div class="img-thumb">
																		<a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-10.jpg')"></div></a>
																	</div>
																	<div class="post-content">
																		<a href="classic-post.html">
																			<h3>Etiam duis nunc dui ad sagittis, mauris at rem, in nunc.</h3>
																		</a>
																		<div class="post-info clearfix">
																			<span>Feb 23, 2016</span>
																			<span>-</span>
																			<span class="rating">
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star-half-o"></i>
																				<i class="fa fa-star-o"></i>
																				<i class="fa fa-star-o"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div></div><div class="owl-item" style="width: 237px;"><div class="item fbt-hr-crs">
																<div class="post-item clearfix">
																	<div class="img-thumb">
																		<a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-19.jpg')"></div></a>
																	</div>
																	<div class="post-content">
																		<a href="classic-post.html">
																			<h3>The age of first-time mothers is rising faster in the US.</h3>
																		</a>
																		<div class="post-info clearfix">
																			<span>Sep 26, 2016</span>
																			<span>-</span>
																			<span class="rating">
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star-half-o"></i>
																				<i class="fa fa-star-o"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div></div><div class="owl-item" style="width: 237px;"><div class="item fbt-hr-crs">
																<div class="post-item clearfix">
																	<div class="img-thumb">
																		<a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-17.jpg')"></div></a>
																	</div>
																	<div class="post-content">
																		<a href="classic-post.html">
																			<h3>Nam iusto delicata ne, eam dolore singulis maiestatis ex.</h3>
																		</a>
																		<div class="post-info clearfix">
																			<span>Aug 13, 2016</span>
																			<span>-</span>
																			<span class="rating">
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star-o"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div></div><div class="owl-item" style="width: 237px;"><div class="item fbt-hr-crs">
																<div class="post-item clearfix">
																	<div class="img-thumb">
																		<a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-33.jpg')"></div></a>
																	</div>
																	<div class="post-content">
																		<a href="classic-post.html">
																			<h3>Lorem ipsum dolor sit amet, nunc neque proin, gravida.</h3>
																		</a>
																		<div class="post-info clearfix">
																			<span>Jun 5, 2016</span>
																			<span>-</span>
																			<span class="rating">
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star-o"></i>
																				<i class="fa fa-star-o"></i>
																				<i class="fa fa-star-o"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div></div><div class="owl-item" style="width: 237px;"><div class="item fbt-hr-crs">
																<div class="post-item clearfix">
																	<div class="img-thumb">
																		<a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-4.jpg')"></div></a>
																	</div>
																	<div class="post-content">
																		<a href="classic-post.html">
																			<h3>Imperdiet mauris nec, donec adipiscing tempus laoreet.</h3>
																		</a>
																		<div class="post-info clearfix">
																			<span>May 23, 2016</span>
																			<span>-</span>
																			<span class="rating">
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																				<i class="fa fa-star"></i>
																			</span>
																		</div>
																	</div>
																</div>
															</div></div></div></div>
															
															
															
															
														<div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev"></div><div class="owl-next"></div></div></div></div>
													</div>
												</div--><!-- Carousel End -->
											</div><!-- Related Posts End -->
											
											<!-- Comment Box Start -->
											<!--div class="fbt-contact-box">
												<div class="title-wrapper color-6">
													<h2><span>Leave Your Comment</span></h2>
												</div>
												<form id="comment-form">
													<div class="row">
														<div class="col-md-4">
															<label for="name">Name*</label>
															<input id="name" name="name" type="text">
														</div>
														<div class="col-md-4">
															<label for="mail">E-mail*</label>
															<input id="mail" name="mail" type="text">
														</div>
														<div class="col-md-4">
															<label for="website">Website</label>
															<input id="website" name="website" type="text">
														</div>
													</div>
													<label for="comment">Comment*</label>
													<textarea id="comment" name="comment"></textarea>
													<button type="submit" id="submit-contact">
														<i class="fa fa-comment"></i> Post Comment
													</button>
												</form>
											</div-->
										
										</div>
									</div>
								</div><!-- Post Content End -->
								
								<div class="pad10">	
								<!-- Post Reviews Sidebar Start -->
								<?php if(isset($suggestions)): ?>
								<div class="widget fbt-vc-inner clearfix">
							<div class="widget-title ">
								<h4>A lire également</h4>
							</div>
							<?php $__currentLoopData = $suggestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="post-item small">
								<div class="row">
									<div class="col-sm-4 col-xs-3">
										<div class="img-thumb">
											<a href="classic-post.html"><div class="fbt-resize" style="background-image: url(<?php echo e(asset($s->photo)); ?>)"></div></a>
										</div>
									</div>
									<div class="col-sm-8 col-xs-9 no-padding-left">
										<div class="post-content">
											<a href="<?php echo e(route('web.actualite',$s->slug)); ?>">
												<h3><?php echo e(str_limit($s->titre,90)); ?></h3>
											</a>
											<div class="post-info clearfix">
												<span class="fa fa-clock-o"></span>
												<span><?php echo e(date('d M, Y',strtotime($s->created_at))); ?></span>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
								<?php endif; ?>
							</div>
							</div>
							
						</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		<?php echo $__env->make("web.includes.right1", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>