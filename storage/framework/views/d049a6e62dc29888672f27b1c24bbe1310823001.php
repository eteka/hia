
<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/plugins/summernote/dist/summernote.css')); ?>" rel="stylesheet">
<style type="text/css">
.note-popover .popover-content, .panel-heading.note-toolbar{
    background: #eaeff5;
} 
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/plugins/summernote/dist/summernote.js')); ?>"></script>

<script type="text/javascript">
var $editor = $('.summernote');

$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
//    height: 250,
    height: 300,
    placeholder: 'Rédigez le contenu',
    dialogsInBody: true,
    lang: 'fr-FR',
});


</script>

<?php $__env->stopSection(); ?>

<div class="form-group row  <?php echo e($errors->has('titre') ? 'has-error' : ''); ?>">
    <?php echo Form::label('titre', trans('contenupage.titre'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-8">
        <?php echo Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','readonly'=>"readonly"]); ?>


        <?php echo $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<!--div class="form-group row  <?php echo e($errors->has('slug') ? 'has-error' : ''); ?>">
    <?php echo Form::label('slug', trans('contenupage.slug'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-8">
        <?php echo Form::text('slug', null, ['class' => 'form-control form-control-sm','rows'=>3]); ?>


        <?php echo $errors->first('slug', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div-->
<div class="form-group row  <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
    <?php echo Form::label('image', trans('contenupage.image'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-8">
        <?php echo Form::file('image',  ['class' => 'form-control form-control-sm', 'rows'=>3]); ?>


        <?php echo $errors->first('image', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<div class="form-group row  <?php echo e($errors->has('legende') ? 'has-error' : ''); ?>">
    <?php echo Form::label('legende', trans('contenupage.legende'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-8">
        <?php echo Form::text('legende', null, ['class' => 'form-control form-control-sm','rows'=>3]); ?>


        <?php echo $errors->first('legende', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>
<div class="form-group row  <?php echo e($errors->has('contenu') ? 'has-error' : ''); ?>">
    <?php echo Form::label('contenu', trans('contenupage.contenu'), ['class' => 'col-md-2 text-right control-label']); ?> :
    <div class="col-md-8">
        <?php echo Form::textarea('contenu', null, ['class' => 'form-control form-control-sm summernote','rows'=>3]); ?>


        <?php echo $errors->first('contenu', '<p class="form-text text-danger help-block">:message</p>'); ?>

    </div>
</div>


<div class="form-group row">
	<div class="col-md-2">
	</div>
    <div class="col-md-4">
        <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']); ?>

    </div>
</div>
