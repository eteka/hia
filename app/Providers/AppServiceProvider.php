<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use \App\Models\Communique ;
use \App\Models\Actualite ;
use \App\Models\Breve ;
use \App\Models\Categorie ;
use \App\Models\ContenuPage ;
use \Carbon\Carbon;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultstringlength(191);
        Carbon::setLocale('fr');
        setlocale(LC_TIME,'French');

        $top_communiques=Communique::where('etat',1)->latest()->take(10)->get();
        $top_actus=Actualite::latest()->where('etat',1)->take(10)->get();
        $top4_actus=Actualite::select('titre','photo','slug','created_at')->where('etat',1)->latest()->take(4)->get();
        $categories=Categorie::latest()->take(10)->get();
        $presentation=ContenuPage::where('slug','presentation')->first();
        $breves=Breve::latest()->where('etat',1)->take(6)->get();

        View::share(compact('top_communiques','top_actus','categories','top4_actus','presentation','breves'));
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
