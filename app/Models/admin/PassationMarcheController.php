<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PassationMarche;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class PassationMarcheController extends Controller
{
    protected $page="";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $passationmarche = PassationMarche::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('fichier', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
                ->orderBy('type')
				->paginate($perPage);
        } else {
            $passationmarche = PassationMarche::orderBy('type')->paginate($perPage);
        }
        $page=$this->page;
        return view('admin.passation-marche.index', compact('passationmarche','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.passation-marche.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required|min:3|max:250',
            'type' => 'required',
            'etat' => 'required',
			'fichier' => 'required|mimes:pdf,docx,doc'
		]);
        $requestData = $request->all();
        $requestData["slug"]=str_slug($requestData['titre']);


         if ($request->hasFile('fichier')) {
                if($file=$request['fichier'] ){
                 $url='/uploads/fichiers/passation-marches/';
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['fichier'] = $url.$fileName;
            }
        }

        //dd($requestData);
        $did=Auth::user()?Auth::user()->id:0;
        
        $requestData["id_user"]=$did;
        
        PassationMarche::create($requestData);

        Session::flash('success', 'PassationMarche ajouté avec succès !');

        return redirect('admin/passation-marche');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $passationmarche = PassationMarche::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.passation-marche.show', compact('passationmarche','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $passationmarche = PassationMarche::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.passation-marche.edit', compact('passationmarche','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'titre' => 'required|min:3|max:250',
            'type' => 'required',
            'etat' => 'required',
            'fichier' => 'mimes:pdf,docx,doc,zip,rar,gzip'
        ]);
        $requestData = $request->all();
        $requestData["slug"]=str_slug($requestData['titre']);


         if ($request->hasFile('fichier')) {
                if($file=$request['fichier'] ){
                 $url='/uploads/fichiers/passation-marches/';
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['fichier'] = $url.$fileName;
            }
        }

        $passationmarche = PassationMarche::findOrFail($id);
        $passationmarche->update($requestData);

        Session::flash('info', 'La Mise à jour de "PassationMarche" a été effectuée  !');

        return redirect('admin/passation-marche');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PassationMarche::destroy($id);

        Session::flash('danger', 'La suppression de "PassationMarche" a été effectuée !');

        return redirect('admin/passation-marche');
    }
}
