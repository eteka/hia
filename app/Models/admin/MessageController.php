<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Message;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class MessageController extends Controller
{
    protected $page="message";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $message = Message::where('nom', 'LIKE', "%$keyword%")
				->orWhere('prenom', 'LIKE', "%$keyword%")
				->orWhere('tel', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('objet', 'LIKE', "%$keyword%")
				->orWhere('message', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $message = Message::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.message.index', compact('message','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.message.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'email' => 'required|email',
			'nom' => 'required',
			'prenom' => 'required'
		]);
        $requestData = $request->all();
        

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Message::create($requestData);

        Session::flash('success', 'Message ajouté avec succès !');

        return redirect('admin/message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.message.show', compact('message','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $message = Message::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.message.edit', compact('message','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'email' => 'required|email',
			'nom' => 'required',
			'prenom' => 'required'
		]);
        $requestData = $request->all();
        
        $message = Message::findOrFail($id);
        $message->update($requestData);

        Session::flash('info', 'La Mise à jour de "Message" a été effectuée  !');

        return redirect('admin/message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Message::destroy($id);

        Session::flash('danger', 'La suppression de "Message" a été effectuée !');

        return redirect('admin/message');
    }
}
