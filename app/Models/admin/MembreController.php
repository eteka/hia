<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Membre;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class MembreController extends Controller
{
    protected $page="membre";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $membre = Membre::where('nom', 'LIKE', "%$keyword%")
				->orWhere('prenom', 'LIKE', "%$keyword%")
				->orWhere('poste', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('biographie', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $membre = Membre::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.membre.index', compact('membre','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.membre.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required',
			'prenom' => 'required',
			'photo' => 'required|image'
		]);
        $requestData = $request->all();

        
            $url='/uploads/membres/';
        if ($request->hasFile('photo')) {
            if($file=$request['photo'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Membre::create($requestData);

        Session::flash('success', 'Membre ajouté avec succès !');

        return redirect('admin/membre');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $membre = Membre::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.membre.show', compact('membre','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $membre = Membre::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.membre.edit', compact('membre','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom' => 'required',
			'prenom' => 'required',
			'photo' => 'required|image'
		]);
        $requestData = $request->all();
        
        $url='/uploads/membres/';
        if ($request->hasFile('photo')) {
            if($file=$request['photo'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }

        $membre = Membre::findOrFail($id);
        $membre->update($requestData);

        Session::flash('info', 'La Mise à jour de "Membre" a été effectuée  !');

        return redirect('admin/membre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Membre::destroy($id);

        Session::flash('danger', 'La suppression de "Membre" a été effectuée !');

        return redirect('admin/membre');
    }
}
