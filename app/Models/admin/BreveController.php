<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Breve;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class BreveController extends Controller
{
    protected $page="breve";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $breve = Breve::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('chapeau', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('corps', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $breve = Breve::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.breve.index', compact('breve','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        return view('admin.breve.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
            'corps' => 'required',
			'etat' => 'required|in:0,1'
		]);
         
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=\mb_substr($lml,0,250);
        
        $url="uploads/breves/";
        if ($request->hasFile('photo')) {

            if($file=$request['photo'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Breve::create($requestData);

        Session::flash('success', 'Breve ajouté avec succès !');

        return redirect('admin/breve');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $breve = Breve::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.breve.show', compact('breve','replace',"page"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $breve = Breve::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.breve.edit', compact('breve','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
         $this->validate($request, [
            'titre' => 'required',
            'corps' => 'required',
            'etat' => 'required|in:0,1'
        ]);
        
        $requestData = $request->all();
         $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=\mb_substr($lml,0,250);
        

        $url="uploads/breves/";
        if ($request->hasFile('photo')) {

            if($file=$request['photo']){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }

        $breve = Breve::findOrFail($id);
        $breve->update($requestData);

        Session::flash('info', 'La Mise à jour de "Breve" a été effectuée  !');

        return redirect('admin/breve');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Breve::destroy($id);

        Session::flash('danger', 'La suppression de "Breve" a été effectuée !');

        return redirect('admin/breve');
    }
}
