<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\GalerieImage;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class GalerieImageController extends Controller
{
    protected $page="image";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $galerieimage = GalerieImage::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $galerieimage = GalerieImage::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.galerie-image.index', compact('galerieimage','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $page=$this->page;
        $replace="/create";
        return view('admin.galerie-image.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'etat' => 'required|in:0,1'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);
        

        if ($request->hasFile('image')) {
           $url='/uploads/galerie-images/';
            if($file=$request['image'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url. $fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        GalerieImage::create($requestData);

        Session::flash('success', 'Image ajoutée à la galerie avec succès !');

        return redirect('admin/galerie-image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $galerieimage = GalerieImage::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.galerie-image.show', compact('galerieimage','replace',"page"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galerieimage = GalerieImage::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.galerie-image.edit', compact('galerieimage','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'etat' => 'required|in:0,1'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);
        

        if ($request->hasFile('image')) {
            $url='/uploads/galerie-images/';
            if($file=$request['image'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url. $fileName;
            }
        }

        $galerieimage = GalerieImage::findOrFail($id);
        $galerieimage->update($requestData);

        Session::flash('info', 'La Mise à jour de "Galerie Image" effectuée  !');

        return redirect('admin/galerie-image');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        GalerieImage::destroy($id);

        Session::flash('danger', 'La suppression  d\'Image effectuée !');

        return redirect('admin/galerie-image');
    }
}
