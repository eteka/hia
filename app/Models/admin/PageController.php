<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Page;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class PageController extends Controller
{
    protected $page="page";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $lpage = Page::latest()->where(function ($query) use ($uid) {
                $query->where('id_user', '=', $uid);
            })->where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('legende', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $lpage = Page::latest()->where('id_user', '=', $uid)->paginate($perPage);
        }
        $page=$this->page;
        return view('admin.page.index', compact('lpage','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.page.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);  

       $url='/uploads/image';
        if ($request->hasFile('image')) {
            if($file=$request['image']){
                $uploadPath = public_path( $url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] =  $url.$fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Page::create($requestData);

        Session::flash('success', 'Page ajoutée avec succès !');

        return redirect('admin/page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.page.show', compact('page','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lpage = Page::findOrFail($id);

        $replace="/".$id."/edit";
        $page=$this->page;

        return view('admin.page.edit', compact('lpage','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required'
		]);
        $requestData = $request->all();
        
        $url='/uploads/image';
        if ($request->hasFile('image')) {
            if($file=$request['image']){
                $uploadPath = public_path( $url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] =  $url.$fileName;
            }
        }
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);  

        $page = Page::findOrFail($id);
        $page->update($requestData);

        Session::flash('info', 'La Mise à jour de la "Page" a été effectuée  !');

        return redirect('admin/page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Page::destroy($id);

        Session::flash('danger', 'La suppression de la "Page" a été effectuée !');

        return redirect('admin/page');
    }
}
