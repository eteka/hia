<?php

namespace App\Http\Middleware;

use Closure;
use \App\User ;
use Cookie;
use Auth;
use View;
use Session;

class AdminMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user() && Auth::user()->etat=='1' && Auth::user()->type=="ADMIN"){
        View::share(compact('promoteur','type_pubs','sfd_id'));       
        return $next($request);
        }else{
             Session::flash('danger','Vous n\'êtes pas autorisé à effectuer cette opération');
            return redirect()->to(url('admin'));
        }
        
        
        
       
    }
}
