<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use \App\User ;
use \App\Models\DossierCredit;
use Auth;
use View;

class DossierCreditMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            $dossier_id=$request->id;
            $dossier=DossierCredit::find($dossier_id);
            if(empty($dossier)){
                Session::flash('warning', "Aucun dossier valide n'a été sélectionné ");
                return redirect()->back();
            }
            
        } else{
             return redirect("login"); 
        }  
        return $next($request);

       
    }
}
