<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User ;
use \App\Models\PassationMarche;
use \App\Models\GalerieVideo;
use \App\Models\GalerieImage;
use \App\Models\ContenuPage;
use \App\Models\Communique;
use \App\Models\Prestation;
use \App\Models\Actualite;
use \App\Models\Carrousel;
use \App\Models\Categorie ;
use \App\Models\Ressource ;
use \App\Models\Message ;
use \App\Models\Membre as Personnel ;
use \App\Models\Breve ;
use \App\Models\Membre ;
use \App\Models\Page ;

use Session;
use Response;
use Auth;


class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $page="home";
    public function __construct()
    {
        $this->middleware('web');
    }
    public function tp(Request $request)
    {
        
        return ('TP');
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page='home';
        $une=Actualite::where('etat',1)->latest()->first();
        $news=Actualite::where('etat',1)->take(12)->get();
        $carrousels=Carrousel::where('etat',1)->latest()->take(10)->get();
        $mot_bienvenu=ContenuPage::where('slug','mot-de-bienvenue')->first();
        $galeries=GalerieImage::latest()->where('etat',1)->take(6)->get();
        $videos=GalerieVideo::latest()->where('etat',1)->take(3)->get();
        $ressources=Ressource::where('etat',1)->latest()->take(6)->get();
        
        $personnels=Membre::latest()->take(6)->get();
        //dd($galeries);
        
        $service1=ContenuPage::where('slug','services-medicotechniques')->first();
        $service2=ContenuPage::where('slug','informations-utiles')->first();

       // dd($top_communiques);
        return view('web.index',compact('carrousels','page','mot_bienvenu','service1','service2','une','galeries','news','ressources','personnels','videos'));
    }
    /*  * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCommunique($slug)
    {
        $page='home';
        $data=Communique::where('etat',1)->where('slug',$slug)->first();
        //dd($communique);
        return view('web.communique',compact('data','page'));
    }
      public function index2()
    {
        return view('web.index2');
    }
      public function presentation()
    {
        $slug='presentation';
        $data=Page::where('slug',$slug)->first();
        if(empty($data)){
            abort(404);
        }
        return view('web.presentation',compact('data'));
    } 
    public function getPage($slug)
    {
        $presentation=['presentation','la-direction-generale','etudes-projets-et-rapports','partenariats-et-jumelages','organisation-des-travaux'];
        $services=['services-medicotechniques','services-administratifs-et-financiers'];
        $infos=['informations-utile','autres-informations'];
        $prmp=['demandes-de-cotation','avis-dattributions'];
        $page="home";
        if(in_array($slug, $presentation)){$page="presentation";}
        if(in_array($slug, $services)){$page="services";}
        if(in_array($slug, $infos)){$page="infos";}
        if(in_array($slug, $prmp)){$page="prmp";}
       
        $data=Page::where('slug',$slug)->first();
        if(empty($data)){
            abort(404);
        }
        return view('web.page',compact('data','page'));
    } 
    public function getPagePrmp($slug)
    {
       
       $page="prmp";
       $nslug=strtoupper($slug);
        $data=PassationMarche::where('etat',1)->where('slug',$slug)->first();
        //dd($data);
        if(empty($data)){
            abort(404);
        }
         $cats=[
                "PPMP"=>"Plan de passation des marchés publics",
                "DC"=>"Demandes de cotations et autres",
                "APAC"=>"Avis public d'Appel à candidature",
                "PVA"=>"PV d'attribution et autres",
            ];
            $titre= isset($cats[$nslug])?$cats[$nslug]:'';
        return view('web.passation-marche',compact('data','page','titre'));
    }
    public function getServices($slug)
    {
        $data=Prestation::where('slug',$slug)->first();
        if(empty($data)){
            abort(404);
        }
        $page="services";   
        return view('web.page',compact('data','page'));
    }
    public function getServieAdminFin()
    {
        $datas=Prestation::where('categorie',2)->paginate(25);
        $page="services";
        return view('web.services_admin_fin',compact('datas','page'));
    }
    public function getServieMedTech()
    {
        $datas=Prestation::where('categorie',1)->paginate(25);
        $page="services";
        return view('web.services_med_tech',compact('datas','page'));
    }
    public function contact(Request $request)
    {
        $sendmessage=FALSE;
        $send=$request->input('send');
        if($send!=''){
            $sendmessage=TRUE;
        }
        $page='contact';
         $bcontact=ContenuPage::where('slug','contact-hia')->first();;
        
        return view('web.contact',compact('sendmessage','page','bcontact'));
    }
    public function save_contact(Request $request)
    {
         $this->validate($request, [
            'objet' => 'required|min:3|max:250',
            'nom' => 'required|min:2|max:150',
            'prenom' => 'required|min:2|max:150',
            'message' => 'required|min:5|max:1000',
        ]);
         //dd($request);
        $requestData = $request->all();
        

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Message::create($requestData);

        Session::flash('success', 'Message ajouté avec succès !');

        return redirect('contact?send=ok');
    }

     public function actualite($slug)
    {
        $data=Actualite::where('etat',1)->where('slug',$slug)->first();
        if(empty($data)){abort(404);}
       // $suggestions =Actualite::orderByRaw("RAND()")->where('id','!=',$data->id)->take(4)->get();
        $suggestions =Actualite::where('etat',1)->inRandomOrder()->where('id','!=',$data->id)->take(4)->get();
        $page='actualite';
        //dd($suggestions);
        return view('web.actualite',compact('data','page','suggestions'));
    } 
     public function actualites()
    {
        $datas=Actualite::latest()->paginate(12);
        if(empty($datas)){abort(404);}
        //dd($datas);
        $page='actualite';
        return view('web.actualites',compact('datas','page'));
    } 
    public function getCatActu($slug)
    {
        $cat=Categorie::where('slug',$slug)->first();
        if(empty($cat)){dd('catégorie non valide');}

        $datas=Actualite::where('categorie_id',$cat->id)->paginate(12);
        if(empty($datas)){abort(404);}
       // dd($datas);
        $color='color-'.rand(1,15);
        $page='actualite';

        return view('web.cat_actualite',compact('datas','page','cat','color'));
    } 
    public function getBreve($slug)
    {
        $data=Breve::where('etat',1)->where('slug',$slug)->first();
        if(empty($data)){abort(404);}
        
        return view('web.actualite',compact('data'));
    }
    public function getBreves()
    {
        $datas=Breve::where('etat',1)->latest()->paginate(12);
        if(empty($datas)){abort(404);}
        
        return view('web.breves',compact('datas'));
    }
    public function getGalerieImage()
    {
        $datas=GalerieImage::latest()->paginate(18);
        return view('web.galerie_images',compact('datas'));
    }
    public function getpersonnel($id)
    {
        $data=Personnel::find($id);
        if(empty($data)){dd('Personnel non valide');}
        return view('web.personnel',compact('data'));
    }
    public function getpersonnels()
    {
        $datas=Personnel::orderBy('nom','prenom')->latest()->paginate(12);
        if(empty($datas)){dd('Personnel non valide');}
       // dd($datas);
        return view('web.personnels',compact('datas'));
    }
    public function getRessources()
    {
       $datas=Ressource::where('etat',1)->latest()->paginate(12);
        if(empty($datas)){dd('Personnel non valide');}
        //dd($datas);
        return view('web.ressources',compact('datas'));
    } 
    public function getPrmp($slug)
    { 
        $nslug=strtoupper($slug);
        //dd($nslug);
       $datas=PassationMarche::where('etat',1)->where('type','like',$nslug)->latest()->paginate(12);
        if(empty($datas)){abort(404);}
        //dd($datas);
        $cats=[
                "PPMP"=>"Plan de passation des marchés publics",
                "DC"=>"Demandes de cotations et autres",
                "APAC"=>"Avis public d'Appel à candidature",
                "PVA"=>"PV d'attribution et autres",
            ];
            $titre= isset($cats[$nslug])?$cats[$nslug]:'';
            //dd($titre);
            $type=strtolower($slug);
        return view('web.passation-marches',compact('datas','titre','type'));
    }
    public function getDownload($id)
    {
        $data=Ressource::where('etat',1)->where('id',$id)->first();
         if(empty($data)){dd('Fichier indisponible pour le moment');}
        $imgurl=$data->fichier;

        
    

        if(!empty($imgurl)){

            $newdwn=intval($data->nb_download)+1;

            $data->update(["nb_download"=>$newdwn]);
            $filepath = public_path($imgurl);

            if(file_exists($filepath)){
                // dd($filepath);
                return Response::download($filepath);
            }else{
                return "Le fichier n'existe pas ou a été déplacé.  Merci";
            }
        }
       // dd($imgurl);
        Session::flash('success', 'Fichier téléchargé !');
        return redirect()->back();
    }
    public function getPrmpDownload($id)
    {
        $data=PassationMarche::where('etat',1)->where('id',$id)->first();
         if(empty($data)){dd('Fichier indisponible pour le moment');}
        $imgurl=$data->fichier;

        
    

        if(!empty($imgurl)){

            $newdwn=intval($data->nb_download)+1;

            $data->update(["nb_download"=>$newdwn]);
            $filepath = public_path($imgurl);

            if(file_exists($filepath)){
                // dd($filepath);
                return Response::download($filepath);
            }else{
                return "Le fichier n'existe pas ou a été déplacé.  Merci";
            }
        }
       // dd($imgurl);
        Session::flash('success', 'Fichier téléchargé !');
        return redirect()->back();
    }
    public function search(Request $keyword)
    {
        $perPage=12;
        $keyword=$keyword->input('search');

        $datas = Actualite::where('titre', 'LIKE', "%$keyword%")
                ->orWhere('slug', 'LIKE', "%$keyword%")
                ->orWhere('chapeau', 'LIKE', "%$keyword%")
                ->orWhere('photo', 'LIKE', "%$keyword%")
                ->orWhere('corps', 'LIKE', "%$keyword%")
                ->orWhere('etat', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        return view('web.search',compact('datas'));
    }
     public function avisAttribution()
    {
        return view('web.presentation');
    }

}
