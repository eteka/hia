<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Carrousel;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class CarrouselController extends Controller
{
     protected $page="carousel";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $carrousel = Carrousel::where('titre', 'LIKE', "%$keyword%")
				->orWhere('sous_titre', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $carrousel = Carrousel::paginate($perPage);
        }
         $page=$this->page;
        return view('admin.carrousel.index', compact('carrousel','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        return view('admin.carrousel.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
            'image' => 'required|image',
            'etat' => 'required|in:0,1',
			'sous_titre' => 'required'
		]);
        $requestData = $request->all();
        
        $requestData['url']="";
        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $url='uploads/sliders/';
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['url']=$requestData['image'] = $url. $fileName;
            }
        }
       // dd($requestData);


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=\mb_substr($lml,0,250);
        
        Carrousel::create($requestData);

        Session::flash('success', 'Carrousel ajouté avec succès !');

        return redirect('admin/carrousel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $carrousel = Carrousel::findOrFail($id);
        $replace="/".$id;
         $page=$this->page;
        return view('admin.carrousel.show', compact('carrousel','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $carrousel = Carrousel::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.carrousel.edit', compact('carrousel','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
            'etat' => 'required|in:0,1',
			'sous_titre' => 'required'
		]);
        $requestData = $request->all();
        $requestData['url']="";
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=\mb_substr($lml,0,250);

        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $url='uploads/sliders/';
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['url']=$requestData['image'] = $url. $fileName;
            }

        }
        //dd($requestData);
        $carrousel = Carrousel::findOrFail($id);
        $carrousel->update($requestData);

        Session::flash('info', 'La Mise à jour de "Carrousel" a été effectuée  !');

        return redirect('admin/carrousel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Carrousel::destroy($id);

        Session::flash('danger', 'La suppression de "Carrousel" a été effectuée !');

        return redirect('admin/carrousel');
    }
}
