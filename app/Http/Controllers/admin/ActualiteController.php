<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Actualite;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class ActualiteController extends Controller
{
    protected $page="actu";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
           /*where(function ($query) use ($uid) {
                $query->where('id_user', '=', $uid);
            })->*/
             $actualite = Actualite::latest()->where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('chapeau', 'LIKE', "%$keyword%")
				->orWhere('photo', 'LIKE', "%$keyword%")
				->orWhere('categorie', 'LIKE', "%$keyword%")
				->orWhere('corps', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $actualite = Actualite::latest()->paginate($perPage);
        }
        $page=$this->page;
        return view('admin.actualite.index', compact('actualite','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $cats=Categorie::pluck('nom','id');

        $page=$this->page;
        return view('admin.actualite.create',compact('replace','page','cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
            'corps' => 'required', 
            'photo' => 'required|image', 
            'etat' => 'required|in:1,0'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
                if($file=$request['photo'] ){
                 $url='uploads/actualites/';
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
         $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=\mb_substr($lml,0,250);
        
        Actualite::create($requestData);

        Session::flash('success', 'Actualite ajouté avec succès !');

        return redirect('admin/actualite');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $actualite = Actualite::findOrFail($id);
        $replace="/".$id;
         $page=$this->page;
        return view('admin.actualite.show', compact('actualite','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $actualite = Actualite::findOrFail($id);
        $replace="/".$id."/edit";
        $cats=Categorie::pluck('nom','id');
        
        $page=$this->page;
        return view('admin.actualite.edit', compact('actualite','replace','page','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'corps' => 'required', 
            'etat' => 'required|in:1,0'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);  
       // dd($requestData);      

       
        if ($request->hasFile('photo')) {
                if($file=$request['photo'] ){
                 $url='uploads/actualites/';
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo'] = $url.$fileName;
            }
        }
        //dd($requestData);

        $actualite = Actualite::findOrFail($id);
        $actualite->update($requestData);

        Session::flash('info', 'La Mise à jour de "Actualité" a été effectuée  !');

        return redirect('admin/actualite');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Actualite::destroy($id);

        Session::flash('danger', 'La suppression de "Actualite" a été effectuée !');

        return redirect('admin/actualite');
    }
}
