<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContenuPage;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class ContenuPageController extends Controller
{
    protected $page="contenu";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $contenupage = ContenuPage::orderBy('titre')->where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('legende', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $contenupage = ContenuPage::orderBy('titre')->paginate($perPage);
        }
        $page=$this->page;
        return view('admin.contenu-page.index', compact('contenupage','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.contenu-page.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'image' => 'required'
		]);
        $requestData = $request->all();
        
        $url='uploads/pages/';
        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url.$fileName;
            }else{
                $requestData['image']="";
            }
        }else{
                $requestData['image']="";
            }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        ContenuPage::create($requestData);

        Session::flash('success', 'ContenuPage ajouté avec succès !');

        return redirect('admin/contenu-page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contenupage = ContenuPage::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.contenu-page.show', compact('contenupage','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contenupage = ContenuPage::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.contenu-page.edit', compact('contenupage','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'image' => 'image',
            'contenu' => 'required'
		]);
        $requestData = $request->all();
        

         $url='uploads/pages/';
        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url.$fileName;
            }else{
                $requestData['image']="";
            }
        }else{
                $requestData['image']="";
            }
       
         $contenupage = ContenuPage::findOrFail($id);
        if($requestData['image']==""){
            $requestData['image']=$contenupage->image;
        }
         //dd($requestData);
       
        $contenupage->update($requestData);

        Session::flash('info', 'La Mise à jour de "ContenuPage" a été effectuée  !');

        return redirect('admin/contenu-page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ContenuPage::destroy($id);

        Session::flash('danger', 'La suppression de "ContenuPage" a été effectuée !');

        return redirect('admin/contenu-page');
    }
}
