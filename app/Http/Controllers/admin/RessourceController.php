<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ressource;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class RessourceController extends Controller
{
    protected $page="res";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $ressource = Ressource::where('titre', 'LIKE', "%$keyword%")
				->orWhere('code', 'LIKE', "%$keyword%")
				->orWhere('fichier', 'LIKE', "%$keyword%")
				->orWhere('format', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $ressource = Ressource::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.ressource.index', compact('ressource','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.ressource.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'fichier' =>'required|mimes:pdf,docx,doc,zip,rar,gzip'
		]);
        $requestData = $request->all();
        
        $url='uploads/ressources/';
        if ($request->hasFile('fichier')) {
            if($file=$request['fichier'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['fichier'] = $url.$fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Ressource::create($requestData);

        Session::flash('success', 'Ressource ajouté avec succès !');

        return redirect('admin/ressource');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ressource = Ressource::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.ressource.show', compact('ressource','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ressource = Ressource::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.ressource.edit', compact('ressource','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'fichier' => 'required|mimes:pdf,docx,doc,zip,rar,gzip'
		]);
        $requestData = $request->all();
        
$url='uploads/ressources/';
        if ($request->hasFile('fichier')) {
            if($file=$request['fichier'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['fichier'] = $url.$fileName;
            }
        }

        $ressource = Ressource::findOrFail($id);
        $ressource->update($requestData);

        Session::flash('info', 'La Mise à jour de "Ressource" a été effectuée  !');

        return redirect('admin/ressource');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ressource::destroy($id);

        Session::flash('danger', 'La suppression de "Ressource" a été effectuée !');

        return redirect('admin/ressource');
    }
}
