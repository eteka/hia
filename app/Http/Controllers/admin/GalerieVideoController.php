<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\GalerieVideo;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class GalerieVideoController extends Controller
{
    protected $page="video";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $galerievideo = GalerieVideo::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('url_video_youtube', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->orWhere('etat', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $galerievideo = GalerieVideo::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.galerie-video.index', compact('galerievideo','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.galerie-video.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'etat' => 'required|in:0,1'
		]);
        $requestData = $request->all();
        

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);
        
        GalerieVideo::create($requestData);

        Session::flash('success', 'GalerieVideo ajouté avec succès !');

        return redirect('admin/galerie-video');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $galerievideo = GalerieVideo::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.galerie-video.show', compact('galerievideo','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galerievideo = GalerieVideo::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.galerie-video.edit', compact('galerievideo','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'etat' => 'required|in:0,1'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250);
        
        $galerievideo = GalerieVideo::findOrFail($id);
        $galerievideo->update($requestData);

        Session::flash('info', 'La Mise à jour de "GalerieVideo" a été effectuée  !');

        return redirect('admin/galerie-video');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        GalerieVideo::destroy($id);

        Session::flash('danger', 'La suppression de "GalerieVideo" a été effectuée !');

        return redirect('admin/galerie-video');
    }
}
