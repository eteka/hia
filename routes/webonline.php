<?php
#Auth::routes(); 
/*\Artisan::call('key:generate');
\Artisan::call('cache:clear');
\Artisan::call('config:clear');*/
#\Artisan::call('config:cache');
  //\Artisan::call('cache:clear');
  //
  //
  //dd('');

Route::get('/key', function () {
	$code=\Artisan::call('key:generate');
	$code=($code==0)?\Artisan::call('config:clear'):$code;
	$code=($code==0)?\Artisan::call('config:cache'):$code;
	return $code;
});
Route::get('/phpinfo', function () {
echo  phpinfo();
});
Route::get('/down', function () {
  \Artisan::call('down');
});
Route::get('/clear', function () {
  \Artisan::call('key:generate');
  \Artisan::call('config:clear');
  \Artisan::call('cache:clear');
  \Artisan::call('view:clear');
 
  \Artisan::call('key:generate');
 \Artisan::call('config:cache');
  return 0;
});
Auth::routes();

Route::get('/',"PageController@index");
Route::get('/page',"PageController@index");
Route::get('/page2',"PageController@index2");
Route::get('/communique/{slug}.html',"PageController@getCommunique")->name('web.get_communique');
Route::get('/actualites',"PageController@actualites")->name('web.actualites');
Route::get('/actualite/categorie/{slug}',"PageController@getCatActu")->name('web.actualite.categorie');
Route::get('/personnel-{id}.html',"PageController@getpersonnel")->name('web.personnel');
Route::get('/ressources/download/{id}',"PageController@getDownload")->name('web.ressource.download');
Route::get('/ressources.html',"PageController@getRessources")->name('web.ressources');
Route::get('/personnels.html',"PageController@getpersonnels")->name('web.personnels');
Route::get('/actualite/{slug}.html',"PageController@actualite")->name('web.actualite');
Route::get('/galerie-image/{slug}',"PageController@getImage")->name('web.igalerie');
Route::get('/breve/{slug}.html',"PageController@getBreve")->name('web.breve');
Route::get('/breves',"PageController@getBreves")->name('web.breves');
Route::get('/partenaires',  'PageController@partenaires')->name('web.partenaires');
Route::get('/partenaires',  'PageController@partenaires')->name('web.partenaires');
#Route::get('/services-administratifs-et-financiers.html',  'PageController@getServieAdminFin')->name('web.service_admin_fin');
Route::get('/services-medicotechniques.html',  'PageController@getServieMedTech')->name('web.service_med_tech');
Route::get('/services/{slug}.html',  'PageController@getServices')->name('web.services');
Route::get('/{slug}.html',  'PageController@getPage')->name('web.getpage');
Route::get('/galerie/images',  'PageController@getGalerieImage')->name('web.galerie_images');
Route::get('/galerie/videos',  'PageController@directionGenerale')->name('web.galerie_videos');

Route::get('/search', 'PageController@search')->name('web.search');;
Route::get('/contact', 'PageController@contact')->name('web.contact');
Route::post('/contact', 'PageController@save_contact')->name('web.savecontact');

Route::get('/tp',"PageController@tp");
Route::get('/home',"PageController@home")->middleware('auth');
Route::get('/admin',"AdminController@index")->middleware('auth');
Route::group(['middleware'=>["auth",'Admin']],function(){
    
    Route::resource('admin/publication', 'admin\\PublicationController');
    Route::resource('admin/actualite', 'admin\\ActualiteController');
    Route::resource('admin/carrousel', 'admin\\CarrouselController');
    Route::resource('admin/page', 'admin\\PageController');
    Route::resource('admin/communique', 'admin\\CommuniqueController');
    Route::resource('admin/breve', 'admin\\BreveController');
    Route::resource('admin/carrousel', 'admin\\CarrouselController');
    Route::resource('admin/galerie-image', 'admin\\GalerieImageController');
    Route::resource('admin/galerie-video', 'admin\\GalerieVideoController');
    Route::resource('admin/message', 'admin\\MessageController');
    Route::resource('admin/ressource', 'admin\\RessourceController');
    Route::resource('admin/page', 'admin\\PageController');
    Route::resource('admin/page', 'admin\\PageController');
    Route::resource('admin/contenu-page', 'admin\\ContenuPageController');
    Route::resource('admin/categorie', 'admin\\CategorieController');
    Route::resource('admin/membre', 'admin\\MembreController');
    Route::resource('admin/galerie-image', 'admin\\GalerieImageController');
    Route::resource('admin/prestation', 'admin\\PrestationController');
});
