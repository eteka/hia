<?php
#Auth::routes();


Route::get('/key', function () {
	$code=\Artisan::call('key:generate');
	$code=($code==0)?\Artisan::call('config:clear'):$code;
	$code=($code==0)?\Artisan::call('config:cache'):$code;
	return $code;
});
Route::get('/ok', function () {
  $u=\Request();
echo  phpinfo();
  //dd($u);
});
Route::get('/clear', function () {
  \Artisan::call('key:generate');
  \Artisan::call('config:clear');
  \Artisan::call('cache:clear');
  \Artisan::call('view:clear');
  \Artisan::call('config:cache');
  \Artisan::call('key:generate');

  return 0;
});
Auth::routes();

Route::get('/',"PageController@getIndex");
Route::get('/tp',"PageController@tp");
Route::group(['middleware'=>"auth"],function(){
  Route::get('/admin',"AdminController@index")->name('admin.index');;
Route::get('/home', 'HomeController@index')->name('home');
#################################################################
# PAGES INTERNES
#################################################################
/*Route::get('admin/promoteur/statistiques', 'AdminController@getStat')->name('promoteur.stat');
Route::get('admin/promoteur/bilan', 'AdminController@getBilan')->name('promoteur.bilan');
Route::get('admin/promoteur/parametre', 'AdminController@getParam')->name('admin.param');

Route::get('admin/sfd/statistiques', 'AdminController@getSfdStat')->name('sfd.stat');
Route::get('admin/sfd/bilan', 'AdminController@getSfdBilan')->name('sfd.bilan');
Route::get('admin/sfd/parametre', 'AdminController@getSfdParam')->name('sfd.param');*/

############################################################################################
# DOSSIER DE CREDIT
############################################################################################
Route::resource('admin/dossier-credit', 'admin\DossierCreditController');
Route::get('admin/dossier-credits/en-cours', 'admin\DossierCreditController@getDossierEnCours');
Route::get('admin/dossier-credits/finalises', 'admin\DossierCreditController@getDossierFinalises');
Route::get('admin/dossier-credit/{id}/show', 'admin\DossierCreditController@showDossier')->name('showDossier');
Route::get('admin/pieces-jointes', 'admin\DossierCreditController@getPieces');
Route::get('admin/{id}/lettre-demande', 'admin\LettreDemandeController@create')->name('LettreDemande.create');
Route::post('admin/{id}/lettre-demande', 'admin\LettreDemandeController@store')->name('LettreDemande.store');
Route::get('admin/{id}/lettre-demande/{lettre_id}/edit', 'admin\LettreDemandeController@edit')->name('LettreDemande.edit');
Route::patch('admin/{id}/lettre-demande/{lettre_id}/edit', 'admin\LettreDemandeController@update')->name('LettreDemande.update');


Route::resource('admin/users', 'admin\UsersController');
Route::get('admin/{id}/lettre-demande-grouppement', 'admin\LettreDemandeGrouppementController@create')->name('lettregrouppement.index');
Route::get('admin/{id}/lettre-demande-grouppement/create', 'admin\LettreDemandeGrouppementController@create')->name('lettregrouppement.create');
Route::post('admin/{id}/lettre-demande-grouppement/create', 'admin\LettreDemandeGrouppementController@store')->name('lettregrouppement.store');
Route::get('admin/{id}/lettre-demande-grouppement/{lettre_id}/edit', 'admin\LettreDemandeGrouppementController@edit')->name('lettregrouppement.edit');
Route::patch('admin/{id}/lettre-demande-grouppement/{lettre_id}/update', 'admin\LettreDemandeGrouppementController@update')->name('lettregrouppement.update');

Route::get('admin/{id}/membres/', 'admin\MembreController@index')->name('membregrouppement.index');
Route::get('admin/{id}/membre/create', 'admin\MembreController@create')->name('membregrouppement.create');
Route::post('admin/{id}/membre/', 'admin\MembreController@store')->name('membregrouppement.store');
Route::get('admin/{id}/membre/{m_id}/show', 'admin\MembreController@show')->name('membregrouppement.show');
Route::get('admin/{id}/membre/{m_id}/edit', 'admin\MembreController@edit')->name('membregrouppement.edit');
Route::patch('admin/{id}/membre/{m_id}/', 'admin\MembreController@update')->name('membregrouppement.update');
/*'admin/'.$item->id.'/lettre-demande/'.$lettre->id.'/edit*/
#Route::get('admin/{id}/emprumteur', 'admin\EmprumteurController@new');
#Route::get('admin/{id}/emprumteur', 'PageController@getNew');
#Route::get('admin/{id}/emprumteur', 'PageController@new');
Route::get('admin/{id}/i-projet', 'admin\IProjetController@create');
Route::post('admin/{id}/i-projet', 'admin\IProjetController@store')->name('iprojet.store');
Route::get('admin/{id}/i-projet/{proj_id}/edit', 'admin\IProjetController@edit')->name('iprojet.edit');
Route::patch('admin/{id}/i-projet/{proj_id}/edit', 'admin\IProjetController@update')->name('iprojet.update');
Route::get('admin/{id}/individu', 'admin\IndividuController@create');
Route::post('admin/{id}/individu', 'admin\IndividuController@store');
Route::get('admin/{id}/individu/{ind_id}/edit', 'admin\IndividuController@edit');
Route::patch('admin/{id}/individu/{ind_id}/edit', 'admin\IndividuController@update')->name('individu.update');
Route::get('admin/{id}/groupement', 'admin\GroupementController@create');
Route::post('admin/{id}/groupement', 'admin\GroupementController@store')->name('groupement.store');
Route::get('admin/{id}/groupement/{group_id}/edit', 'admin\GroupementController@edit')->name('groupement.edit');
Route::patch('admin/{id}/groupement/{group_id}/edit', 'admin\GroupementController@update')->name('groupement.update');
Route::get('admin/{id}/epargne-dispo', 'admin\EpargneDispoController@create');
Route::post('admin/{id}/epargne-dispo', 'admin\EpargneDispoController@store')->name('epargne.store');
Route::get('admin/{id}/epargne-dispo/{epargne_id}/edit', 'admin\EpargneDispoController@edit')->name('epargne.edit');
Route::patch('admin/{id}/epargne-dispo/{epargne_id}/edit', 'admin\EpargneDispoController@update')->name('epargne.update');
Route::get('admin/{id}/experience-credit', 'admin\ExperienceCreditController@create');
Route::post('admin/{id}/experience-credit', 'admin\ExperienceCreditController@store')->name('experience.store');
Route::get('admin/{id}/experience-credit/{exp_id}/edit', 'admin\ExperienceCreditController@edit')->name('experience.edit');
Route::patch('admin/{id}/experience-credit/{exp_id}/edit', 'admin\ExperienceCreditController@update')->name('experience.update');
Route::get('admin/{id}/evaluation-garantites', 'admin\EvaluationGarantiteController@index');
Route::get('admin/{id}/evaluation-garantite', 'admin\EvaluationGarantiteController@create')->name('evaluation.create');
Route::get('admin/{id}/evaluation-garantite/{ev_id}/edit', 'admin\EvaluationGarantiteController@edit')->name('evaluationgarantie.edit');
Route::patch('admin/{id}/evaluation-garantite/{ev_id}/edit', 'admin\EvaluationGarantiteController@update')->name('evaluation.update');
Route::post('admin/{id}/evaluation-garantite', 'admin\EvaluationGarantiteController@store')->name('evaluation.store');
Route::get('admin/{id}/etat-recette', 'admin\EtatRecetteController@create');
Route::get('admin/{id}/situation-partimoniale', 'admin\SituationPartimonialeController@create')->name('situation.create');
Route::get('admin/{id}/situation-partimoniales', 'admin\SituationPartimonialeController@index')->name('situation.index');
Route::post('admin/{id}/situation-partimoniale/', 'admin\SituationPartimonialeController@store')->name('situation.store');
Route::get('admin/{id}/situation-partimoniale/{sit_id}/edit', 'admin\SituationPartimonialeController@edit')->name('situation.edit');
Route::patch('admin/{id}/situation-partimoniale/{sit_id}/edit', 'admin\SituationPartimonialeController@update')->name('situation.update');


############################################################################################
# Dossier de prêt
# Dossier soumis et traitement
############################################################################################
Route::get('admin/dossiers-de-pret', 'admin\DossierPretController@index');
Route::get('admin/soumission-dossierss', 'admin\DossierPretController@index');
############################################################################################
# INSTITUTIONS
# Accueil
############################################################################################

Route::get('admin/institutions-financieres', 'admin\FanepiaController@getInstitutionFinancieres')->name('if.all');
Route::get('admin/institution/{id}/{slug}', 'admin\FanepiaController@getInstitutionProduits')->name('if.produits');
Route::get('admin/institution/{id}/{slug}/produit-credit', 'admin\FanepiaController@getIfProduitCredit')->name('if.produit_credit');
Route::get('admin/institution/{id}/{slug}/produit-epargne', 'admin\FanepiaController@getIfProduitEpargne')->name('if.produit_epargne');


############################################################################################
# FANEPIA
# EBST Investment
############################################################################################
Route::group(['prefix'=>'admin/{id}/','middleware'=>['auth','DossierCredit']],function(){
  ############################
  # GET ROUTE
  ############################
  Route::get('ebst-business-as-usual/', 'admin\FanepiaController@indexEbst')->name('usual.index');
  Route::get('ebst-business-as-usual/create', 'admin\FanepiaController@create')->name('usual.create');
  Route::get('ebst-business-as-usual/mon-entreprise', 'admin\FanepiaController@mon_entreprise')->name('usual.entreprise');
  Route::get('ebst-business-as-usual/biens-equipements', 'admin\FanepiaController@biens_equipements')->name('usual.biens');
  Route::get('ebst-business-as-usual/activite/{act_id}/produits', 'admin\FanepiaController@activite_produits')->name('usual.activite.produits');
  Route::get('ebst-business-as-usual/activite/{act_id}', 'admin\FanepiaController@activite')->name('usual.activite.store');
  Route::get('ebst-business-as-usual/cout-de-production', 'admin\FanepiaController@cout_de_production')->name('cout_de_production')->name('usual.cout-de-production');
  Route::get('ebst-business-as-usual/revenu-net', 'admin\FanepiaController@getRevenuNet')->name('usual.revenu_net');
  Route::get('ebst-business-as-usual/activite/{act_id}/annee/{annee}', 'admin\FanepiaController@getParties')->name('usual.activite.edit_parties');
  Route::get('spvr', 'admin\FanepiaController@createSpvr')->name('spvr.create');
  Route::post('spvr', 'admin\FanepiaController@saveSpvr')->name('spvr.store');
  Route::get('spvr/{spvr_id}/edit', 'admin\FanepiaController@editSpvr')->name('spvr.edit');
  Route::patch('spvr/{spvr_id}', 'admin\FanepiaController@updateSpvr')->name('spvr.update');
  ############################
  # POST ROUTE
  ############################
  Route::get('ebst-business-as-usual/edit', 'admin\FanepiaController@edit')->name('usual.entreprise.edit');
  Route::post('ebst-business-as-usual/terre/store', 'admin\FanepiaController@terre_store')->name('usual.terre.store');
  Route::post('ebst-business-as-usual/entreprise/store', 'admin\FanepiaController@entreprise_store')->name('usual.entreprise.store');  
  Route::post('ebst-business-as-usual/activite/{act_id}/produits', 'admin\FanepiaController@produit_store')->name('usual.activite.store_produit');;
  Route::post('admin/ebst-investment/activite/store', 'admin\FanepiaInvestmentController@activite_store')->name('usual.activite.store');
  Route::post('ebst-business-as-usual/bien/store', 'admin\FanepiaController@bien_store')->name('usual.bien.store');;
  Route::patch('ebst-business-as-usual/entreprise/update', 'admin\FanepiaController@entreprise_update')->name('usual.entreprise.update');
  #Route::post('ebst-business-as-usual/activite/{act_id}', 'admin\FanepiaController@getActivite')
  Route::get('ebst-investment/activite/{act_id}', 'admin\FanepiaInvestmentController@activite');
  Route::post('ebst-investment/activite/{act_id}', 'admin\FanepiaInvestmentController@activite')->name('activite.choix_annee');
   ############################
  # UPDATE ROUTE
  ############################
  #Route::post('ebst-business-as-usual/entreprise/update', 'admin\FanepiaController@entreprise_update');
  Route::patch('ebst-business-as-usual/terre/update', 'admin\FanepiaController@terre_update')->name('usual.terre.update');
  
  Route::post('ebst-business-as-usual/activite/store', 'admin\FanepiaController@activite_store')->name('usual.activite.update');;
#Route::post('ebst-business-as-usual/produit/store', 'admin\FanepiaController@produit_store');

  
  
  


  ##############################################################################################
# GESTION DES REVENUS
##############################################################################################
Route::get('ebst-business-as-usual/activite/{act_id}/revenus/{annee}', 'admin\FanepiaController@editRevenus')->name('usual.activite.edit_revenus');
Route::post('ebst-business-as-usual/activite/{act_id}/revenus/{annee}', 'admin\FanepiaController@saveRevenus')->name('activite.save_revenus');
Route::patch('ebst-business-as-usual/activite/{act_id}/revenus/{annee}', 'admin\FanepiaController@updateRevenus')->name('activite.update_revenus');
 ##############################################################################################
# GESTION DES REVENUS
##############################################################################################
Route::get('ebst-business-as-usual/activite/{act_id}/investissement/{annee}', 'admin\FanepiaController@editInvestissement')->name('usual.activite.edit_invest');
Route::post('ebst-business-as-usual/activite/{act_id}/investissement/{annee}', 'admin\FanepiaController@saveInvestissement')->name('usual.activite.save_invest');

##############################################################################################
# GESTION DES COUT VARIABLES 
##############################################################################################
Route::get('ebst-business-as-usual/activite/{act_id}/cout-variable/{annee}', 'admin\FanepiaController@editCoutsVariables')->name('usual.activite.edit_couts_variables');
Route::post('ebst-business-as-usual/activite/{act_id}/cout-variable/{annee}', 'admin\FanepiaController@saveCoutsVariables')->name('usual.activite.save_couts_variables');
##############################################################################################
# GESTION DES COUT VARIABLES 
##############################################################################################
Route::get('ebst-business-as-usual/activite/{act_id}/cout-fixe/{annee}', 'admin\FanepiaController@editCoutsFixes')->name('usual.activite.edit_couts_fixes');
Route::post('ebst-business-as-usual/activite/{act_id}/cout-fixe/{annee}', 'admin\FanepiaController@saveCoutsFixes')->name('usual.activite.save_couts_fixes');
##############################################################################################
# GESTION DES CONSOMMATIONS 
##############################################################################################
Route::get('ebst-business-as-usual/activite/{act_id}/consommation/{annee}', 'admin\FanepiaController@editConsommations')->name('usual.activite.edit_consommations');
Route::post('ebst-business-as-usual/activite/{act_id}/consommation/{annee}', 'admin\FanepiaController@saveConsommations')->name('usual.activite.save_consommations');

Route::patch('ebst-business-as-usual/activite/{act_id}/cout-fixe/{annee}', 'admin\FanepiaController@updateCoutsFixes')->name('usual.activite.update_couts_fixes');
Route::get('ebst-business-as-usual/couts-fixes-generaux', 'admin\FanepiaController@couts_fixes_generaux')->name('couts_fixes_generaux');

###############################################################################
# BILAN 
###############################################################################
Route::get('ebst-business-as-usual/cashs-flow', 'admin\FanepiaController@getCashFlow')->name('usual.getCashFlow');
  /* Route::get('ebst-business-as-usual/bien-équipement/delete/{id}', 'admin\FanepiaController@bien_equipement_delete');*/


});//->middleware(['auth']);

Route::get('admin/ebst-business-as-usual/bien-équipement/edit/{bien_id}', 'admin\FanepiaController@bien_equipement_edit');
  Route::post('admin/ebst-business-as-usual/bien-équipement/update/{bien_id}', 'admin\FanepiaController@bien_equipement_update');
/*Route::get('admin/{id}/ebst-business-as-usual/', 'admin\FanepiaController@indexEbst');
Route::get('admin/ebst-business-as-usual/create', 'admin\FanepiaController@create');
Route::get('admin/ebst-business-as-usual/edit', 'admin\FanepiaController@edit');
Route::post('admin/ebst-business-as-usual/terre/store', 'admin\FanepiaController@terre_store');
Route::post('admin/ebst-business-as-usual/terre/update', 'admin\FanepiaController@terre_update');
Route::post('admin/ebst-business-as-usual/entreprise/store', 'admin\FanepiaController@entreprise_store');


Route::get('admin/ebst-business-as-usual/mon-entreprise', 'admin\FanepiaController@mon_entreprise');
Route::post('admin/ebst-business-as-usual/activite/store', 'admin\FanepiaController@activite_store');

Route::get('admin/ebst-business-as-usual/produits/activite/{id}', 'admin\FanepiaController@activite_produits');

*/
Route::get('admin/ebst-business-as-usual/biens-équipements', 'admin\FanepiaController@biens_equipements');
Route::post('admin/ebst-business-as-usual/bien/store', 'admin\FanepiaController@bien_store');
Route::get('admin/ebst-business-as-usual/bien-équipement/edit/{id}', 'admin\FanepiaController@bien_equipement_edit');
Route::post('admin/ebst-business-as-usual/bien-équipement/update/{id}', 'admin\FanepiaController@bien_equipement_update');
Route::get('admin/ebst-business-as-usual/bien-équipement/delete/{id}', 'admin\FanepiaController@bien_equipement_delete');

Route::get('admin/ebst-business-as-usual/investissement-en-immobilisations', 'admin\FanepiaController@investissement_immobilisations');
Route::post('admin/ebst-business-as-usual/immobilisation/store', 'admin\FanepiaController@immobilisation_store');
Route::get('admin/ebst-business-as-usual/immobilisation/edit/{id}', 'admin\FanepiaController@immobilisation_edit');
Route::post('admin/ebst-business-as-usual/immobilisation/update/{id}', 'admin\FanepiaController@immobilisation_update');
Route::get('admin/ebst-business-as-usual/immobilisation/delete/{id}', 'admin\FanepiaController@immobilisation_delete');


Route::get('admin/ebst-business-as-usual/finance/edit/{id}', 'admin\FanepiaController@finance_edit');
Route::post('admin/ebst-business-as-usual/finance/update/{id}', 'admin\FanepiaController@finance_update');
Route::get('admin/ebst-business-as-usual/finance/delete/{id}', 'admin\FanepiaController@finance_delete');
Route::post('admin/ebst-business-as-usual/finance/store', 'admin\FanepiaController@finance_store');


Route::get('admin/ebst-business-as-usual/menage/edit/{id}', 'admin\FanepiaController@menage_edit');
Route::post('admin/ebst-business-as-usual/menage/update/{id}', 'admin\FanepiaController@menage_update');
Route::get('admin/ebst-business-as-usual/menage/delete/{id}', 'admin\FanepiaController@menage_delete');
Route::post('admin/ebst-business-as-usual/menage/store', 'admin\FanepiaController@menage_store');

Route::post('admin/ebst-business-as-usual/revenu/store', 'admin\FanepiaController@revenu_store');
Route::get('admin/ebst-business-as-usual/revenu/edit/{id}', 'admin\FanepiaController@revenu_edit');
Route::post('admin/ebst-business-as-usual/revenu/update/{id}', 'admin\FanepiaController@revenu_update');
Route::get('admin/ebst-business-as-usual/revenu/delete/{id}', 'admin\FanepiaController@revenu_delete');


Route::get('admin/ebst-business-as-usual/cout-variable/edit/{id}', 'admin\FanepiaController@cout_variable_edit');
Route::post('admin/ebst-business-as-usual/cout-variable/update/{id}', 'admin\FanepiaController@cout_variable_update');
Route::get('admin/ebst-business-as-usual/cout-variable/delete/{id}', 'admin\FanepiaController@cout_variable_delete');
Route::post('admin/ebst-business-as-usual/couts-variables/store', 'admin\FanepiaController@couts_variables_store');

Route::post('admin/ebst-business-as-usual/cout-fixe/store', 'admin\FanepiaController@cout_fixe_store');
Route::get('admin/ebst-business-as-usual/cout-fixe/edit/{id}', 'admin\FanepiaController@cout_fixe_edit');
Route::post('admin/ebst-business-as-usual/cout-fixe/update/{id}', 'admin\FanepiaController@cout_fixe_update');
Route::get('admin/ebst-business-as-usual/cout-fixe/delete/{id}', 'admin\FanepiaController@cout_fixe_delete');

Route::post('admin/ebst-business-as-usual/consommation-domestique/store', 'admin\FanepiaController@consommation_domestique_store');
Route::get('admin/ebst-business-as-usual/consommation-domestique/edit/{id}', 'admin\FanepiaController@consommation_domestique_edit');
Route::post('admin/ebst-business-as-usual/consommation-domestique/update/{id}', 'admin\FanepiaController@consommation_domestique_update');
Route::get('admin/ebst-business-as-usual/consommation-domestique/delete/{id}', 'admin\FanepiaController@consommation_domestique_delete');






Route::get('admin/ebst-business-as-usual/bilan/activité/{id}', 'admin\FanepiaController@bilan_activite');
##############################################################################################
# GESTION DES COUT FIXES 
##############################################################################################

##############################################################################################
# GESTION DES UTILISATEURS 
##############################################################################################
Route::get('admin/utilisateurs', 'admin\UsersController@index')->name('users.index');
Route::get('admin/param-compte/{id}', 'admin\UsersController@getParam')->name('users.getParam');
Route::patch('admin/param-compte/{id}', 'admin\UsersController@updateParam')->name('users.updat_param');



########################################################################################################
# PRODUITS
########################################################################################################
Route::resource('admin/produit-credit', 'admin\ProduitCreditController');
Route::resource('admin/produit-epargne', 'admin\ProduitEpargneController');
########################################################################################################
# FANEPIA INVESTEMENT
#########################################################################################################


Route::get('admin/ebst-investment', 'admin\FanepiaController@indexInv');
Route::get('admin/ebst-investment/create', 'admin\FanepiaInvestmentController@create');
Route::get('admin/ebst-investment/edit', 'admin\FanepiaInvestmentController@edit');
Route::post('admin/ebst-investment/terre/store', 'admin\FanepiaInvestmentController@terre_store');
Route::post('admin/ebst-investment/terre/update', 'admin\FanepiaInvestmentController@terre_update');
Route::post('admin/ebst-investment/entreprise/store', 'admin\FanepiaInvestmentController@entreprise_store');
Route::post('admin/ebst-investment/entreprise/update', 'admin\FanepiaInvestmentController@entreprise_update');

Route::get('admin/ebst-investment/mon-entreprise', 'admin\FanepiaInvestmentController@mon_entreprise');

Route::get('admin/ebst-investment/biens-équipements', 'admin\FanepiaInvestmentController@biens_equipements');
Route::post('admin/ebst-investment/bien/store', 'admin\FanepiaInvestmentController@bien_store');
Route::get('admin/ebst-investment/bien-équipement/edit/{id}', 'admin\FanepiaInvestmentController@bien_equipement_edit');
Route::post('admin/ebst-investment/bien-équipement/update/{id}', 'admin\FanepiaInvestmentController@bien_equipement_update');
Route::get('admin/ebst-investment/bien-équipement/delete/{id}', 'admin\FanepiaInvestmentController@bien_equipement_delete');

Route::get('admin/ebst-investment/investissement-en-immobilisations', 'admin\FanepiaInvestmentController@investissement_immobilisations');
Route::post('admin/ebst-investment/immobilisation/store', 'admin\FanepiaInvestmentController@immobilisation_store');
Route::get('admin/ebst-investment/immobilisation/edit/{id}', 'admin\FanepiaInvestmentController@immobilisation_edit');
Route::post('admin/ebst-investment/immobilisation/update/{id}', 'admin\FanepiaInvestmentController@immobilisation_update');
Route::get('admin/ebst-investment/immobilisation/delete/{id}', 'admin\FanepiaInvestmentController@immobilisation_delete');


Route::get('admin/ebst-investment/finance/edit/{id}', 'admin\FanepiaInvestmentController@finance_edit');
Route::post('admin/ebst-investment/finance/update/{id}', 'admin\FanepiaInvestmentController@finance_update');
Route::get('admin/ebst-investment/finance/delete/{id}', 'admin\FanepiaInvestmentController@finance_delete');
Route::post('admin/ebst-investment/finance/store', 'admin\FanepiaInvestmentController@finance_store');


Route::get('admin/ebst-investment/menage/edit/{id}', 'admin\FanepiaInvestmentController@menage_edit');
Route::post('admin/ebst-investment/menage/update/{id}', 'admin\FanepiaInvestmentController@menage_update');
Route::get('admin/ebst-investment/menage/delete/{id}', 'admin\FanepiaInvestmentController@menage_delete');
Route::post('admin/ebst-investment/menage/store', 'admin\FanepiaInvestmentController@menage_store');

Route::post('admin/ebst-investment/revenu/store', 'admin\FanepiaInvestmentController@revenu_store');
Route::get('admin/ebst-investment/revenu/edit/{id}', 'admin\FanepiaInvestmentController@revenu_edit');
Route::post('admin/ebst-investment/revenu/update/{id}', 'admin\FanepiaInvestmentController@revenu_update');
Route::get('admin/ebst-investment/revenu/delete/{id}', 'admin\FanepiaInvestmentController@revenu_delete');


Route::get('admin/ebst-investment/cout-variable/edit/{id}', 'admin\FanepiaInvestmentController@cout_variable_edit');
Route::post('admin/ebst-investment/cout-variable/update/{id}', 'admin\FanepiaInvestmentController@cout_variable_update');
Route::get('admin/ebst-investment/cout-variable/delete/{id}', 'admin\FanepiaInvestmentController@cout_variable_delete');
Route::post('admin/ebst-investment/couts-variables/store', 'admin\FanepiaInvestmentController@couts_variables_store');

Route::post('admin/ebst-investment/cout-fixe/store', 'admin\FanepiaInvestmentController@cout_fixe_store');
Route::get('admin/ebst-investment/cout-fixe/edit/{id}', 'admin\FanepiaInvestmentController@cout_fixe_edit');
Route::post('admin/ebst-investment/cout-fixe/update/{id}', 'admin\FanepiaInvestmentController@cout_fixe_update');
Route::get('admin/ebst-investment/cout-fixe/delete/{id}', 'admin\FanepiaInvestmentController@cout_fixe_delete');

Route::post('admin/ebst-investment/consommation-domestique/store', 'admin\FanepiaInvestmentController@consommation_domestique_store');
Route::get('admin/ebst-investment/consommation-domestique/edit/{id}', 'admin\FanepiaInvestmentController@consommation_domestique_edit');
Route::post('admin/ebst-investment/consommation-domestique/update/{id}', 'admin\FanepiaInvestmentController@consommation_domestique_update');
Route::get('admin/ebst-investment/consommation-domestique/delete/{id}', 'admin\FanepiaInvestmentController@consommation_domestique_delete');


#Route::get('admin/ebst-business-as-usual', 'admin\FanepiaController@indexEbst');
#Route::get('admin/ebst-business-as-usual/create', 'admin\FanepiaController@create');

#Route::resource('admin/i-projet', 'admin\\IProjetController');
#Route::resource('admin/individu', 'admin\\IndividuController');
#Route::resource('admin/groupement', 'admin\\GroupementController');
#Route::resource('admin/epargne-dispo', 'admin\\EpargneDispoController');
#Route::resource('admin/experience-credit', 'admin\\ExperienceCreditController');
#Route::resource('admin/evaluation-garantite', 'admin\\EvaluationGarantiteController');
#Route::resource('admin/situation-partimoniale', 'admin\\SituationPartimonialeController');
#Route::resource('admin/etat-recette', 'admin\\EtatRecetteController');
Route::resource('admin/i-financieres', 'admin\IFinancieresController');


###################################################################################
# Soumission du dossier
###################################################################################
Route::get('admin/soumission-dossier', 'admin\DossierCreditController@soumissionDossier');
Route::get('admin/soumission-dossier/{id}/institution', 'admin\DossierCreditController@getSoumissionDossier')->name('getSoumissionDossier');
Route::post('admin/soumission-dossier/{id}/institution', 'admin\DossierCreditController@postSoumission')->name('postSoumissionDossierLink');
#######################################################################################
# Dossier soumis
#######################################################################################

Route::get('admin/dossiers-credit/{id}', 'admin\DossierCreditController@show')->name('getDossierSoumis');
Route::get('admin/dossiers-soumis/', 'admin\DossierCreditController@getDossierSoumis')->name('getDossierSoumis');
Route::get('admin/dossiers-recu/', 'admin\DossierCreditController@getDossierRecu')->name('getDossierRecus');
Route::get('admin/dossiers-recu/{id}/examination', 'admin\DossierCreditController@getExamDossier')->name('getExamDossier');
Route::post('admin/dossiers-recu/{id}/examination', 'admin\DossierCreditController@postExamDossier')->name('postExamDossier');
Route::get('admin/dossiers-credit/{id}/reaction-if', 'admin\DossierCreditController@getReactionDossier')->name('getReactionDossier');




Route::get('admin/ebst-business-as-usual/cash-flow', 'admin\FanepiaController@cash_flow')->name('cash_flow');
#Route::get('admin/ebst-business-as-usual/revenu-net', 'admin\FanepiaController@revenu_net')->name('revenu_net');

});
Route::resource('admin/autre-produit', 'admin\\AutreProduitController');