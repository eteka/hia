
@section('script')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() { $("#selectM").select2(); });
    </script>
    <style type="text/css">
        .select2-container--default .select2-selection--multiple,.select2-container--default.select2-container--focus .select2-selection--multiple{
            border: solid #ddd 1px !important;
            outline: 0;
            padding: 3px !important;
            border-radius: 3px !important;
            background: #fafafa !important;
            }
            .select2-container--default .select2-selection--multiple .select2-selection__choice{
                border-radius: 0px;
                background: #fff;
                border: 1px solid #dddddd;
            }
    </style>
@endsection

<?php
    $user=Auth::user();
    $user_id=$user->id;
    $data=\App\User::select('nom','prenom','id')->where('id','!=',$user_id)->get();//->prepend([''=>'selectionnez-']);
    $auteurs[$user_id]=$user->nom.' '.$user->prenom;
    foreach ($data as $d) {
        # code...
        $auteurs [$d['id']]=$d['nom'].' '.$d['prenom'];
    }
    $typdeD=\App\Models\TypeDocument::pluck('nom','id')->prepend('-Sélectionnez un type-','');
    $disciplines=\App\Models\Discipline::pluck('nom','id')->prepend('-Sélectionnez une discipline-','');

?>

  
    <div class="form-group row {{ $errors->has('annee_pub') ? 'has-error' : ''}}">
    {!! Form::label('annee_pub', trans('publication.annee_pub'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-4">
        {!! Form::number('annee_pub', null, ['class' => 'form-control','placeholder'=>'','required'=>TRUE]) !!}
        {!! $errors->first('annee_pub', '<p class="help-block">:message</p>') !!}
    </div>

        <!--div class="col-md-5">
                <div class="form-group row {{ $errors->has('volume') ? 'has-error' : ''}}">
                    {!! Form::label('volume', trans('publication.volume'), ['class' => 'col-md-5 text-right control-label']) !!}* :
                    <div class="col-md-6">
                        {!! Form::number('volume', null, ['class' => 'form-control','placeholder'=> '']) !!}
                        {!! $errors->first('volume', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
        </div-->
</div>

    <div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
        {!! Form::label('numero', trans('publication.numero'), ['class' => 'col-md-3 text-right control-label']) !!}* :
        <div class="col-md-4">
            {!! Form::text('numero', null, ['class' => 'form-control']) !!}
            {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('pages') ? 'has-error' : ''}}">
        {!! Form::label('pages', trans('publication.pages'), ['class' => 'col-md-3 text-right control-label']) !!}* :
        <div class="col-md-4">
            {!! Form::text('pages', null, ['class' => 'form-control','placeholder'=> 'ex: 30-55']) !!}
            {!! $errors->first('pages', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

<div class="form-group row {{ $errors->has('discipline_id') ? 'has-error' : ''}}">
    {!! Form::label('discipline_id', trans('publication.discipline_id'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-4">
        {!! Form::select('discipline_id',$disciplines, null, ['class' => 'form-control']) !!}
        {!! $errors->first('discipline_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('type_document_id') ? 'has-error' : ''}}">
    {!! Form::label('type_document_id', trans('publication.type_document_id'), ['class' => 'col-md-3 text-right control-label']) !!} * :
    <div class="col-md-4">
        {!! Form::select('type_document_id',$typdeD, null, ['class' => 'form-control']) !!}
        {!! $errors->first('type_document_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('publication.titre'), ['class' => 'col-md-3 text-right control-label']) !!}* : 
    <div class="col-md-8">
        {!! Form::text('titre', null, ['class' => 'form-control','autocomplete'=>'off','placeholder'=> 'Saisissez le titre de la publication']) !!}
        {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('journal') ? 'has-error' : ''}}">
    {!! Form::label('journal', trans('publication.journal'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::text('journal', null, ['class' => 'form-control','required'=>TRUE,]) !!}
        {!! $errors->first('journal', '<p class="help-block">:message</p>') !!}
    </div>
</div><!--div class="form-group row {{ $errors->has('categorie_journal') ? 'has-error' : ''}}">
    {!! Form::label('categorie_journal', trans('publication.categorie_journal'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::text('categorie_journal', null, ['class' => 'form-control']) !!}
        {!! $errors->first('categorie_journal', '<p class="help-block">:message</p>') !!}
    </div>
</div-->
<div class="form-group row {{ $errors->has('auteurs') ? 'has-error' : ''}}">
    {!! Form::label('auteurs', trans('publication.auteurs'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::select('auteurs[]',$auteurs, null, ['id'=>"selectM",'class' => 'form-control','multiple'=>'multiple']) !!}
        {!! $errors->first('auteurs', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('coauteurs') ? 'has-error' : ''}}">
    {!! Form::label('coauteurs', trans('publication.coauteur'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::text('coauteurs', null, ['class' => 'form-control']) !!}
        {!! $errors->first('coauteurs', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!--div class="form-group row {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('publication.description'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::textarea('description', null, ['class' => 'form-control','rows'=>5]) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div--><div class="form-group row {{ $errors->has('impactfactor') ? 'has-error' : ''}}">
    {!! Form::label('impactfactor', trans('publication.impactfactor'), ['class' => 'col-md-3 text-right control-label']) !!}&nbsp;&nbsp; :
    <div class="col-md-8">
            <div class="row">
                <div class="col-sm-4">
                    <div class="checkbox">
                            <label> {!! Form::radio('impactfactor', "Impact factor", false) !!} Impact factor</label>
                    </div>
                </div>   
                <div class="col-sm-4">  
                    <div class="checkbox">
                        <label> {!! Form::radio('impactfactor', "Indexé", false) !!} Indexé</label>
                    </div>
                </div>   
                <div class="col-sm-4">
                    <div class="checkbox">
                        <label> {!! Form::radio('impactfactor', "Comité de lecture", false) !!} Comité de lecture</label>
                    </div>
            
                </div>   
            </div>
        
        
        


        {!! $errors->first('ind', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('resume') ? 'has-error' : ''}}">
    {!! Form::label('resume', trans('publication.resume'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::textarea('resume', null, ['class' => 'form-control','required'=>TRUE,'placeholder'=> 'Rédigez ici le résumé de votre article']) !!}
        {!! $errors->first('resume', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('mots_cle') ? 'has-error' : ''}}">
    {!! Form::label('mots_cle', trans('publication.mots_cle'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::text('mots_cle', null, ['class' => 'form-control','placeholder'=> 'Economie, Agriculture, Gestion, Développement']) !!}
        {!! $errors->first('mots_cle', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!--div class="form-group row {{ $errors->has('centre_id') ? 'has-error' : ''}}">
    {!! Form::label('centre_id', trans('publication.centre_id'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::number('centre_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('centre_id', '<p class="help-block">:message</p>') !!}
    </div>
</div-->
<!--div class="form-group row {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('publication.etat'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        <div class="checkbox">
    <label>{!! Form::radio('etat', '1', true) !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('etat', '0') !!} No</label>
</div>
        {!! $errors->first('etat', '<p class="help-block">:message</p>') !!}
    </div>
</div-->
<!--div class="form-group row {{ $errors->has('visibilite_id') ? 'has-error' : ''}}">
    {!! Form::label('visibilite_id', trans('publication.visibilite_id'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::number('visibilite_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('visibilite_id', '<p class="help-block">:message</p>') !!}
    </div>
</div-->

<div class="form-group row {{ $errors->has('url_fichier') ? 'has-error' : ''}}">
    {!! Form::label('url_fichier', trans('publication.url_fichier'), ['class' => 'col-md-3 text-right control-label']) !!}* :
    <div class="col-md-8">
        {!! Form::file('url_fichier',  ['class' => 'form-control','accept'=>'.pdf']) !!}
        {!! $errors->first('url_fichier', '<p class="help-block">:message</p>') !!}
    </div>
</div><!--div class="form-group row {{ $errors->has('image_pub') ? 'has-error' : ''}}">
    {!! Form::label('image_pub', trans('publication.image_pub'), ['class' => 'col-md-3 text-right control-label']) !!} _ :
    <div class="col-md-8">
        {!! Form::file('image_pub',  ['class' => 'form-control','accept'=>'.jpeg,.jpg,.png,.gif,']) !!}
        {!! $errors->first('image_pub', '<p class="help-block">:message</p>') !!}
    </div>
</div-->

<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
