<?php

return [
    'denomination' => "Dénomination ",
'logo' => 'Logo',
'adresse' => 'Adresse',
'email' => "Email de l'institution",
'telephone' => 'Téléphone',
'ifu' => 'Numéro IFU',
'numeroagrement' => 'Numéro agrément',
'devise' => 'Devise',
'sigle' => 'Sigle',
'fax' => 'Fax',
'BP' => 'Boîte postale(BP)',
];
