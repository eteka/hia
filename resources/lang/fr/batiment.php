<?php

return [
    'intitule' => 'Batiment',
'duree_vie' => 'Durée de vie(ans)',
'date_achat' => 'Date achat',
'cout' => 'Cout',
'valeur_residuelle' => 'Valeur résiduelle',
'depreciation_annuelle' => 'Dépréciation annuelle',
'id_entreprise' => 'Entreprise',
];
