<?php

return [
'nom_grouppement' => 'Nom du grouppement',
'nom_president' => 'Nom et prénom du Président',
'adresse_president' => 'Adresse du Président',
'nom_secretaire' => 'Nom du (de la) Secrétaire',
'adresse_secretaire' => 'Adresse du (de la) Secrétaire',
'nom_tresorier' => 'Nom et prénom du (de la) Tresorier(ère)',
'adresse_tresorier' => 'Adresse du (de la) Tresorier(ère)',
'localisation' => 'Localisation du grouppement',
'montant_total' => 'Montant total demandé',
'objet_credit' => 'Objet de la demande de crédit',
'fichier' => 'Fichier attaché',
];
