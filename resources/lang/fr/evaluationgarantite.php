<?php

return [
    'garantie' => 'Nom de la garantie',
    'typegarantie' => 'Type de garantie',
'reference' => 'Référence',
'localisation' => 'Localisation',
'nom_proprietaire' => 'Nom propriétaire',
'valeur_garantie' => 'Valeur garantie',
'date_visite' => 'Date la visite',
'visiteurs' => 'Visiteurs',
];
