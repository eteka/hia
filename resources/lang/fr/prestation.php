<?php

return [
    'titre' => 'Titre de la prestation',
'slug' => 'Slug',
'image' => 'Image descriptive',
'contenu' => 'Détails sur la prestation',
'id_user' => 'User',
'cat' => 'Catégorie',
];
