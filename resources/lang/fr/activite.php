<?php

return [
    'intitule' => 'Activité',
	'nb_jours' => 'Durée',
	'date_debut' => 'Date début',
	'date_fin' => 'Date fin',
	'description' => 'Description',
	'id_entreprise' => 'Id Entreprise',
];
