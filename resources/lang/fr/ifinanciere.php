<?php

return [
    'denomination' => 'Dénomination',
'logo' => "Logo de l'institution",
'adresse' => 'Adresse',
'email' => 'Email',
'telephone' => 'Téléphone',
'ifu' => 'Ifu',
'numeroagrement' => 'Numéro agrément',
'devise' => "Devise de l'entreprise",
'sigle' => 'Sigle',

'BP' => 'Boite Postale',
'id_user' => 'User',
];
