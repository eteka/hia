<?php

return [
    'qte' => 'Quantité',
'prix' => "Prix d'entrée",
'date' => "Date de l'opération",
'dateop' => "Date",
'jour' => 'Jour',
'mois' => 'Mois',
'annee' => 'Annee',
'unite' => 'Unité',
'id_entreprise' => 'Entreprise :',
'stockinitial' => 'Stock initial',
'stockfinal' => 'Stock final',
'id_type_mouvement' => 'Type de mouvement',
'id_produit' => 'Produit',
'prixmarche' => 'Prix du marché',
];
