<?php

return [
    'nom_entreprise' => "Nom de l'entreprise",
    'localite' => "Localité",
    'adresse' => "Adresse",
    'telephone' => "Téléphone",
    'email' => "Email",
    'prenom' => "Prénom",
    'nom' => 'Nom de famille',
    'critere' => 'Critère',
'poids' => 'Poids',
'influence' => 'Influence',
'valeur' => 'Valeur',
];
