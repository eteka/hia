@extends('layouts.web')
@section('title',"Les membres de l'administration")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">Les membres de l'administration</h2></div>
								<div class="row">
									@if(isset($datas) && $datas->count())
									@foreach($datas as $a)
									<div class="col-md-4 col-sm-12 col-xs-6 fbt-vc-inner post-grid clearfix">
										<div class="post-item clearfix">
											<div class="img-thumb rond10">
												<a href="{{route('web.personnel',$a->id)}}"><div class="fbt-resize" style="background-image: url('{{asset($a->photo)}}')"></div></a>
											</div>
											<div class="post-content " style="height: 65px;">
												
												<a href="{{route('web.personnel',$a->id)}}"><h5>{{str_limit($a->prenom.' '.$a->nom,50)}}</h5></a>
												<div class="post-info clearfix">
													<span>{{$a->poste?$a->poste:''}}</span>
												</div>
											</div>
										</div>
									</div>

									@endforeach
									@else
									<div class="pad10">	
									<div class="borderc text-center rond3 pad-hug bge">	
										<h1><i class="fa fa-info-circle">	</i>	</h1>
										<h4>Aucune information disponible pour le moment</h4>
										<small class="text-muted">	La liste des membres du personnel apparaît ici</small>
									</div>
									</div>
									@endif
								</div>
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
