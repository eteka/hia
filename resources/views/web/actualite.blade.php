@extends('layouts.web')
@if(isset($data,$data->titre))
@section('title',$data->titre)
@endif
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">

							@if(!empty($data->photo))
							<div class=""><h2 class="page-header">{{$data->titre}}</h2></div>
							<div class="clearfix">
								<div class="img-crop">
									<img src="{{asset($data->photo)}}" class="img-responsive" alt="">
									<div class="img-credits">
										<div class="col-md-10">
											<div class="post-title"><h1>{{$data->titre}}</h1></div>
											<div class="post-info clearfix">
												
												<!--span class="sepr">-</span-->
												<span class="date"><i class="fa fa-clock-o"></i> {{date('d M, Y',strtotime($data->created_at))}}</span>
												<!--span class="sepr">-</span>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-o"></i>
												</span-->
											</div>
										</div>
									</div>
								</div><!-- img-crop -->
							</div>
							@else
							<div class=""><h2 class="page-header">{{$data->titre}}</h2></div>
							@endif
							
							<div class="row" style="transform: none;">
								<!-- Post Content Start -->
								<div class="fbt-col-lg-12 col-md-12 single-post-container clearfix">
									<div class="row">
										<div class="col-md-12">
											<!-- Post Share Start -->
											<!--div class="post-share clearfix">
												<ul>
													<li><a class="facebook df-share" data-sharetip="Share on Facebook!" href="#" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i> <span class="social-text">Facebook</span></a></li>
													<li><a class="twitter df-share" data-hashtags="" data-sharetip="Share on Twitter!" href="#" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> <span class="social-text">Tweeter</span></a></li>
													<li><a class="google df-pluss" data-sharetip="Share on Google+!" href="#" rel="nofollow" target="_blank"><i class="fa fa-google-plus"></i> <span class="social-text">Google+</span></a></li>
													<li><a class="pinterest df-pinterest" data-sharetip="Pin it" href="#" target="_blank"><i class="fa fa-pinterest-p"></i> <span class="social-text">Pinterest</span></a></li>
												</ul>
											</div--><!-- Post Share End -->
											
											<div class="clearfix"></div>
											@if($data->chapeau)
												<div class="post-text-content clearfix">
												<blockquote>  {!!$data->chapeau!!}</blockquote>
												</div>
											@endif

											<div class="post-text-content clearfix">
												{!!$data->corps!!}
											</div>

											
											<div class="clearfix"></div>
											<div class="fbt-related-posts clearfix">
												<!--div class="title-wrapper color-3">
													<h2><span>You Might Also Like</span></h2>
												</div-->
												
												
											</div><!-- Related Posts End -->
											
											
										
										</div>
									</div>
								</div><!-- Post Content End -->
								
								<div class="pad10">	
								<!-- Post Reviews Sidebar Start -->
								@if(isset($suggestions))
								<div class="widget fbt-vc-inner_ clearfix">
							<div class="widget-title ">
								<h4>A lire également</h4>
							</div>
							@foreach($suggestions as $s)
							<div class="post-item small">
								<div class="row">
									<div class="col-sm-4 col-xs-3">
										<div class="img-thumb">
											<a href="classic-post.html"><div class="fbt-resize" style="background-image: url({{asset($s->photo)}})"></div></a>
										</div>
									</div>
									<div class="col-sm-8 col-xs-9 no-padding-left">
										<div class="post-content">
											<a href="{{route('web.actualite',$s->slug)}}">
												<h3>{{str_limit($s->titre,90)}}</h3>
											</a>
											<div class="post-info clearfix">
												<span class="fa fa-clock-o"></span>
												<span>{{date('d M, Y',strtotime($s->created_at))}}</span>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
								</div>
								@endif
							</div>
							</div>
							
						</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
