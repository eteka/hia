@extends('layouts.web')
@section('title')
{!! 'Galerie images' !!}
@endsection
@section('content')

<style>
    .bg2f{background: #e2e2e2}
    @media (max-width: 1200px) {
        #photos {
            -moz-column-count:    5;
            -webkit-column-count: 5;
            column-count:         5;
        }
    }
    @media (max-width: 1000px) {
        #photos {
            -moz-column-count:    4;
            -webkit-column-count: 4;
            column-count:         4;
        }
    }
    @media (max-width: 800px) {
        #photos {
            -moz-column-count:    3;
            -webkit-column-count: 3;
            column-count:         3;
        }
    }
    @media (max-width: 400px) {
        #photos {
            -moz-column-count:    1;
            -webkit-column-count: 1;
            column-count:         1;
        }
    }
    #photos {
        /* Prevent vertical gaps */
        line-height: 0;

        -webkit-column-count: 4;
        -webkit-column-gap:   0px;
        -moz-column-count:    4;
        -moz-column-gap:      0px;
        column-count:         4;
        column-gap:           0px;  
    }

    #photos img {
        /* Just in case there are inline attributes */
        width: 98% !important;
        height: auto !important;
        margin-bottom: 3px;
    }

    .carousel-control .glyphicon,
    .carousel-control .icon-prev,
    .carousel-control .icon-next {
        position: absolute;
        top: 50%;
        left: 50%;
        z-index: 5;
        display: inline-block;
        width: 20px;
        height: 20px;
        margin-top: -10px;
        margin-left: -10px;
        /*font-family: serif;*/
    }
    h4.div_img_titles {
        position: absolute;
        color: #ffffff;
        text-shadow: 0 1px 1px #222;
        margin: 5px 10px;
    }
    .bas_titre,a:hover{text-decoration: none;}
    .bas_titre{
        background: #000000;
        background: -moz-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -ms-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -o-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: -webkit-linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        background: linear-gradient(top, rgba(255,255,255,.1), rgba(0,0,0,.91));
        font-size: 12px;
        padding: 3px 5px;
        text-decoration: none;
        color: #ffe41b;
        color: #ffffff;
        /*font-weight: bold;*/
        padding-top: 15px;
        height: 150px;
        text-shadow: 0 1px 1px #222;
        /*position: absolute;*/
        /*background: #000;*/
        z-index: 11110;
        overflow: hidden;
    }
    .pad2{padding: 1px}
</style>


<div class="container"> 
<div class="row">   
    <div class="outer-wrapper clearfix">    
    <div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
            @if(isset($datas))
            <div class="">
              <h2 class="page-header"> Galerie images</h2>                        

                <!--<h2 class="light   m0 rss page-header "></h2>-->
                <div class=" bgwhite">
                    <div class="menu-html-content mtop-10 margin-bottom-2">
                        <div id="photos_0" class="rows">
                           @if(isset($datas) && $datas->count())
                                    <div class="gallery">
                                        
                                        <div class="row">
                                            <div class="gallery-img">
                                                @foreach($datas as $g)
                                                <div class="col-md-4 col-xs-6 padding-1">
                                                    <div class="post-item clearfix">
                                                        <div class="img-thumb">
                                                            <a href="{{asset($g->image)}}" data-lightbox="roadtrip" data-lightbox="image-{{$g->id}}" data-title="{{ isset($g->titre) ? $g->titre : '' }}"><div class="fbt-resize" style="background-image: url('{{asset($g->image)}}')"></div></a>
                                                            <div class="img-credits">
                                                                <h3>{{$g->titre}}</h3>
                                                                <div class="post-info clearfix">
                                                                    <span>{{date('M d, Y h\H:i\'',strtotime($g->created_at))}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        
                                    </div>
                                    @endif

                        </div>
                        
                    </div> 
                    
                </div>
                <div class="clearfix"></div>
                <br> <hr>
                {{$datas->links()}}
            </div>
            @endif
           
        </div>
        <div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
        @include("web.includes.right1")
    </div>
    </div>
</div>
</div>
<script>
    $(function () {
        $('*').find('img').addClass('img-responsive');
        $('body').find('img').attr('data-lightbox', 'hh');
    })
</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('assets/plugins/lightbox/css/lightbox.min.css')}}">
@endsection
@section('script')
<script src="{{asset('assets/plugins/lightbox/js/lightbox.min.js')}}"></script>

    <script>
        jQuery(document).ready(function ($) {
            // Auto-handle direct jpeg links
            $('a[href$="jpg"], a[href$="png", a[href$="gif"]]').not('.gallery a').each(function () {
                $(this).lightbox();
            });
        });
    </script>
@endsection