@extends('layouts.web')
@section('title',$titre)
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
							<div class=""><h2 class="page-header">{{$data->titre}}</h2></div>

							
							<div class="row" style="transform: none;">
								<!-- Post Content Start -->
								<div class="fbt-col-lg-12 col-md-12 single-post-container clearfix">
									<div class="row">
										<div class="col-md-12">
											<!-- Post Share Start -->
											<!--div class="post-share clearfix">
												<ul>
													<li><a class="facebook df-share" data-sharetip="Share on Facebook!" href="#" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i> <span class="social-text">Facebook</span></a></li>
													<li><a class="twitter df-share" data-hashtags="" data-sharetip="Share on Twitter!" href="#" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> <span class="social-text">Tweeter</span></a></li>
													<li><a class="google df-pluss" data-sharetip="Share on Google+!" href="#" rel="nofollow" target="_blank"><i class="fa fa-google-plus"></i> <span class="social-text">Google+</span></a></li>
													<li><a class="pinterest df-pinterest" data-sharetip="Pin it" href="#" target="_blank"><i class="fa fa-pinterest-p"></i> <span class="social-text">Pinterest</span></a></li>
												</ul>
											</div--><!-- Post Share End -->
											
											<div class="clearfix"></div>
										
											<div class="post-text-content clearfix">
												{!!$data->contenu!!}
											</div>
											
											@if(isset($data->fichier))
											<!--div class="pad10 bge borderc">
											Cliquez ici pour télécharger le fichier <hr class="pad0 m0">
											<span>
												<!--a class=""   href="{{route('web.prmp.download',$data->id)}}">
												<a class="" target="_blanck"   href="{{url($data->fichier)}}"> 
													<i class="fa fa-download-alt"> </i> {{route('web.prmp.download',$data->id)}}</a></span>
											</div-->
											@endif


											
											<div class="clearfix"></div>
									
											<div class="fbt-related-posts clearfix">
											</div><!-- Related Posts End -->
										
										</div>
									</div>
								</div><!-- Post Content End -->
								
								
								
							</div>
							
						</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
