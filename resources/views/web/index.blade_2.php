
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<title>Fanepia - Votre application de gestion de vos outils d'échanges, de financement et d'epargnes</title>
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="CoopBank - Responsive HTML5 Template">
<meta name="author" content="iwthemes.com">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="{{asset('assets/web/css/style.css')}}" rel="stylesheet" media="screen">

<link href="{{asset('assets/web/css/theme-responsive.css')}}" rel="stylesheet" media="screen">

<link href="#" rel="stylesheet" media="screen" class="skin">

<link href="#" rel="stylesheet" media="screen" class="mix-skin">

<link rel="shortcut icon" href="{{asset('assets/web/mg/icons/favicon.ico')}}">
<link rel="apple-touch-icon" href="{{asset('assets/web/img/icons/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/web/img/icons/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/web/img/icons/apple-touch-icon-114x114.png')}}">

<script src="{{asset('assets/web/js/libs/modernizr.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript')}}"></script>
<!--[if IE]>
            <link rel="stylesheet" href="css/ie/ie.css">
        <![endif]-->
<!--[if lte IE 8]>
            <script src="js/responsive/html5shiv.js"></script>
            <script src="js/responsive/respond.js"></script>
        <![endif]-->
</head>
<body>






<div id="layout" class="layout-semiboxed">

<div id="fond-header" class="fond-header pattern-header-01"  style="background-color: white;"/></div>

<header id="header">
<div class="row">

<div class="col-md-4 col-lg-5">
<div class="logo">
<div class="icon-logo">
</div>
<a href="index.php" style="color: #00c292;">
PROFINA
<span>Votre application num&eacute;rique</span>
</a>
</div>
</div>


<div class="col-md-8 col-lg-7">
<div class="info-login" >
<div class="head-info-login" style="background-color:#00c292;">
<p style="background-color:#00c292;">Constituez vos dossiers de cr&eacute;dit en ligne</p>
<span style="background-color: #eb4436;">
<a href="{{url('register')}}" >Inscrivez-vous ici</a>
</span>
</div>

</div>
</div>

</div>
</header>




<div class="content-central">

<nav id="menu" style="">
<div class="navbar yamm navbar-default">
<div class="container">
<div class="row">
<div class="navbar-header">
<button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div id="navbar-collapse-1" class="navbar-collapse collapse" >

<ul class="nav navbar-nav">

<li class="dropdown">
<a href="{{url('/')}}"  class="dropdown-toggle">
ACCEUIL
</a>
</li>


<li class="dropdown">
<a href="#"  class="dropdown-toggle">
A PROPOS
</a>

</li>

<!--
<li class="dropdown">
<a href="template-gallery-2.html" data-toggle="dropdown" class="dropdown-toggle">
Gallery<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="template-gallery-2.html"> Gallery 2 Columns </a></li>
<li><a href="template-gallery-3.html"> Gallery 3 Columns </a></li>
<li><a href="template-gallery-4.html"> Gallery 4 Columns </a></li>
</ul>
</li>


<li class="dropdown">
<a href="template-services-options.html" data-toggle="dropdown" class="dropdown-toggle">
Our Services<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="template-services-options.html"> Services Options</a></li>
<li><a href="template-credit-simulator.html"> Credit Simulator </a></li>
<li><a href="template-user-area.html"> User Area </a></li>
<li><a href="template-archives-download.html"> Archives Download </a></li>
<li><a href="template-agreement-area.html">Agreement</a></li>
</ul>
</li>


<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle">
Portfolio<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li>

<div class="yamm-content">
<div class="row">
<ul class="col-md-4 list-unstyled">
<li>
<strong>
<i class="fa fa-bicycle"></i>
For Persons
</strong>
<span>Special For Financial institutions</span>
</li>
<li><a href="template-accounts.html"> Accounts </a></li>
<li><a href="template-credits.html"> Credits </a></li>
<li><a href="template-inversions.html"> Inversions </a></li>
</ul>
<ul class="col-md-4 list-unstyled">
<li>
<strong>
<i class="fa fa-database"></i>
For Companies
</strong>
<span>Focused for cooperatives</span>
</li>
<li><a href="template-accounts.html"> Accounts </a></li>
<li><a href="template-credits.html"> Credits </a></li>
<li><a href="template-inversions.html"> Inversions </a></li>
</ul>
<ul class="col-md-4 list-unstyled">
<li>
<strong>
<i class="fa fa-hand-peace-o"></i>
For Pymes
</strong>
<span>Employee Funds versions</span>
</li>
<li><a href="template-accounts.html"> Accounts </a></li>
<li><a href="template-credits.html"> Credits </a></li>
<li><a href="template-inversions.html"> Inversions </a></li>
</ul>
</div>
</div>
<i class="fa fa-coffee big-icon"></i>
</li>
</ul>
</li>
-->
 
<li class="dropdown">
<a href="actualites.php" class="dropdown-toggle">
ACTUALIT&Eacute;S</a>
</li>


<li class="dropdown">
<a href="#" class="dropdown-toggle">
CONTACTEZ-NOUS
</a>
</li>

</ul>


<ul class="nav navbar-nav navbar-right">

<li class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
<b class="glyphicon glyphicon-search"></b>
</a>
 <ul class="dropdown-menu">
<li>
<div class="yamm-content">
<form class="search-Form" action="#">
<div class="input-group">
<span class="input-group-addon">
<i class="fa fa-search-plus"></i>
</span>
<input class="form-control" placeholder="Votre recherche ici" name="search" type="text" required="required">
</div>
</form>
</div>
</li>
</ul>
</li>
</ul>

</div>
</div>
</div>
</div>
</nav>


<div class="content_info">
<div class="container">
<div class="row">

<div class="col-md-8 col-lg-9">

<div class="breadcrumbs">
<ul>
<li class="breadcrumbs-home">
<a href="#" title="Retoour">
<i class="fa fa-home"></i>
</a>
</li>
<li>
Votre application de gestion de vos outils d'échanges, de financement et d'epargnes</li>
</ul>
</div>


<div class="tp-banner-container">
<div class="tp-banner">

<ul>




<li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500">

<img src="{{asset('assets/web/5.jpg')}}" alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">


<div class="tp-caption large_bold_white sft stb" data-x="center" data-hoffset="" data-y="center" data-voffset="-60" data-speed="300" data-start="1000" data-splitin="lines" data-splitout="none" data-easing="easeOutExpo">Réaliser votre rêve
</div>

<div class="tp-caption small_light_white other_light sfb stb" data-x="center" data-hoffset="" data-y="center" data-voffset="" data-speed="500" data-start="1200" data-splitin="lines" data-splitout="none" data-easing="easeOutExpo">Avec les techniques de récoltes des producteurs
</div>

<div class="tp-caption lfb tp-resizeme" data-x="center" data-y="center" data-voffset="70" data-speed="300" data-start="1400" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
<a href='#' class='btn btn-primary'><i class="fa fa-eye"></i>Voir plus</a>
</div>

</li>

<li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500">

<img src="{{asset('assets/web/4.jfif')}}" alt="fullslide1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">


<div class="tp-caption medium_text other_text lft stl" data-x="left" data-hoffset="130" data-y="top" data-voffset="180" data-speed="300" data-start="800" data-splitin="lines" data-splitout="none" data-easing="easeOutExpo">Besoin d'aide pour renforcer vos cultures?
</div>

<div class="tp-caption large_bold_white sft stb" data-x="left" data-hoffset="130" data-y="top" data-voffset="210" data-speed="300" data-start="1000" data-splitin="lines" data-splitout="none" data-easing="easeOutExpo">Nous sommes disponible pour vous
</div>

<div class="tp-caption small_light_white sfb stb" data-x="left" data-hoffset="130" data-y="top" data-voffset="315" data-speed="500" data-start="1200" data-splitin="lines" data-splitout="none" data-easing="easeOutExpo">
</div>

<div class="tp-caption lfb tp-resizeme" data-x="left" data-hoffset="130" data-y="center" data-voffset="70" data-speed="300" data-start="1400" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
<a href='#' class='btn btn-primary'><i class="fa fa-eye"></i>Voir plus</a>
</div>




</li>


</ul>

<div class="tp-bannertimer"></div>
</div>
</div>

</div>
<div class="col-md-4 col-lg-3">

<ul class="nav nav-tabs" role="tablist" >
<li role="presentation" class="active" style="visibility:hidden"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Simu</a></li>
<li role="presentation" style="background-color:#00c292;"><a style="background-color:#00c292;" href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Connectez-vous ici</a></li>
</ul>


<div class="tab-content" id="tab-content" style="background-color:#00c292;">

<div role="tabpanel" class="tab-pane fade in active" id="tab1" style="background-color:#00c292;">
<form action="" class="form-theme">
<label>Identifiant</label>
<div class="selector">
<input type="text" placeholder="Renseignez votre identifiant" class="input">

</div>

<label>Mot de passe</label>
<div class="selector">
<input type="password" placeholder="Renseignez votre mot de passe" class="input">

</div>

<label>Type d'utilisateur</label>
<div class="" >
<select style="position: relative;
    min-width: 60px;
    width: 100%;
    line-height: 0;
    color: #999;
    font-size: .875rem;
    height: 36px;
    margin-bottom: 25px;">
<option selected>PME</option>
<option>SFD</option>
</select>
</div>
<a href="{{url('login')}}" class="btn" style="background-color: #eb4436;">Se connecter</a>
</form>
</div>


<div role="tabpanel" class="tab-pane fade" id="tab2">
<ul class="contact-list">
<li>
<h4><i class="fa fa-envelope-o"></i>Email:</h4>
<a href="#">Contact Customer Service</a>
</li>
<li>
<h4><i class="fa fa-fax"></i>Phones:</h4>
<h5>Miami:</h5>
<p>447 50 12</p>
<h5>Number Single National</h5>
<p>02 4000 4 56234</p>
</li>
<li>
<h4><i class="fa fa-life-ring"></i>Care centers:</h4>
<a href="#"><i class="fa fa-arrow"></i>
<i class="fa fa-arrow-circle-o-right"></i> Offices
</a>
<a href="#"><i class="fa fa-arrow"></i>
<i class="fa fa-arrow-circle-o-right"></i>Cashiers
</a>
<a href="#"><i class="fa fa-arrow"></i>
<i class="fa fa-arrow-circle-o-right"></i> Point friend
</a>
</li>
</ul>
</div>

 </div>

</div>



</div>
</div>
</div>


<br>
<br>

<section class="content_info">

<div class="content_resalt padding-bottom borders">

<div class="title-vertical-line">
<h2 style="color:#00c292;"><span>Nos dernières</span> offres</h2>
<p class="lead">Dans cette section vous trouverez la liste de nos récentes offres publiées par les PMEs et SFDs. Elles constituent 
leur marché pour les besoins du monde extérieur</p>
</div>


<div class="container">

<div class="row padding-top">

<div class="col-md-4">

<div class="item-blog-post">

<div class="head-item-blog-post">
<i class="fa fa-calculator" style="color:black"></i>
<h3 style="color:black">Titre: Nouvel Article</h3>
</div>


<div class="img-item-blog-post">
<img src="{{asset('assets/web/img/blog-img/thumbs/1.jpg')}}" alt="">
<div class="post-meta">
<ul>
<li>
<i class="fa fa-user"></i>
<a href="#">publieur</a>
</li>
<li>
<i class="fa fa-clock-o"></i>
<span>Dates</span>
</li>
<li>
<i class="fa fa-eye"></i>
<span>vues/ou com</span>
</li>
</ul>
</div>
</div>


<div class="info-item-blog-post">
<p>Find all the support and information they need to make all decisions about saving for your future.</p>
<a href="#"><i class="fa fa-plus-circle"></i> Lire plus</a>
</div>

</div>

</div>


<div class="col-md-4">

<div class="item-blog-post">

<div class="head-item-blog-post">
<i class="fa fa-calculator" style="color:black"></i>
<h3 style="color:black">Titre: Nouvel Article</h3>
</div>


<div class="img-item-blog-post">
<img src="{{asset('assets/web/img/blog-img/thumbs/2.jpg')}}" alt="">
<div class="post-meta">
<ul>
<li>
<i class="fa fa-user"></i>
<a href="#">publieur</a>
</li>
<li>
<i class="fa fa-clock-o"></i>
<span>Dates</span>
</li>
<li>
<i class="fa fa-eye"></i>
<span>vues/ou com</span>
</li>
</ul>
</div>
</div>


<div class="info-item-blog-post">
<p>Meet here all our range of products and services, rules of our products and everything related to your savings in pension.</p>
<a href="#"><i class="fa fa-plus-circle"></i> Lire plus</a>
</div>

</div>
 
</div>


<div class="col-md-4">

<div class="item-blog-post">

<div class="head-item-blog-post">
<i class="fa fa-calculator" style="color:black"></i><h3 style="color:black">Titre:  Nouvel Article</h3>
</div>


<div class="img-item-blog-post">
<img src="{{asset('assets/web/img/blog-img/thumbs/3.jpg')}}" alt="">
<div class="post-meta">
<ul>
<li>
<i class="fa fa-user"></i>
<a href="#">publieur</a>
</li>
<li>
<i class="fa fa-clock-o"></i>
<span>Dates</span>
</li>
<li>
<i class="fa fa-eye"></i>
<span>vues/ou com</span>
</li>
</ul>
</div>
</div>


<div class="info-item-blog-post">
<p>Accompany relevant share you mean, renewed and information of interest to learn to save you and your projects come true.</p>
<a href="#"><i class="fa fa-plus-circle"></i> Lire plus</a>
</div>

</div>

</div>

</div>

</div>

</div>

</section>




<div class="content_info">

<div class="title-vertical-line">
<h2 style="color:#00c292;"><span>Pourquoi nous</span> choisir ?</h2>
<p class="lead">Nous simplifions  et rendons plus rapide la tâche à la gestion physique des données qui constituent la gestion des PMEs par les SFDs
. Désormais avec notre application, beaucoup de choses peuvent se faire en ligne dans un temps record. Ci-dessous nos avantages: </p>
</div>


<div class="paddings">

<div class="container">
<div class="row">
<div class="col-md-12">

<div class="services-process">

<div class="item-service-process color-bg-1">
<div class="head-service-process">
<i class="fa fa-cubes"></i>
<h3>ASSURANCE</h3>
 </div>
<div class="divisor-service-process">
<span class="circle-top">1</span>
<span class="circle"></span>
</div>
<div class="info-service-process">
<h3>VOS ASSURANCES</h3>
<p>Votre tranquillité d'esprit n'a pas de prix, nous vous proposons un large portefeuille de solutions pour vous garantir ce que vous aimez le plus.</p>
</div>
</div>


<div class="item-service-process color-bg-2">
<div class="head-service-process">
<i class="fa fa-diamond"></i>
<h3>AVANTAGES</h3>
</div>
<div class="divisor-service-process">
<span class="circle-top">2</span>
<span class="circle"></span>
</div>
<div class="info-service-process">
<h3>VOS AVANTAGES</h3>
<p>Nous avons créé des alliances avec des entités reconnues qui contribuent à améliorer votre qualité de vie.</p>
</div>
</div>


<div class="item-service-process color-bg-3">
<div class="head-service-process">
<i class="fa fa-bicycle"></i>
<h3>RAPIDE</h3>
</div>
<div class="divisor-service-process">
<span class="circle-top">3</span>
<span class="circle"></span>
</div>
<div class="info-service-process">
<h3>Rapidité</h3>
<p>Notre application vous permet de gagner un maximum de temps pour trouver des solutions propres à votre demande.</p>
</div>
</div>


<div class="item-service-process color-bg-4">
<div class="head-service-process">
<i class="fa fa-hotel"></i>
<h3>S&Eacute;CURIT&Eacute;</h3>
</div>
<div class="divisor-service-process">
<span class="circle-top">4</span>
<span class="circle"></span>
</div>
<div class="info-service-process">
<h3>Sécurité garantie</h3>
 <p>Nous garantissons un niveau supérieur dans le traitement de vos données. Notre système de gestion de base de données est basé sur un algorithme crypté.</p>
</div>
</div>

</div>

</div>
</div>
</div>

</div>

</div>



</div>

<div class="container wow fadeInUp">
<div class="row">
<div class="col-md-12">

<div class="title-vertical-line">
<h2 style="color:#00c292;"><span>Nos</span> Partenaires</h2>
</div>
<ul class="owl-carousel carousel-sponsors tooltip-hover" id="carousel-sponsors">
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/1.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/2.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/3.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/4.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/5.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/6.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/7.png')}}" alt="Image"></a>
</li>
<li data-toggle="tooltip" title data-original-title="Name Sponsor">
<a href="#" class="tooltip_hover" title="Name Sponsor"><img src="{{asset('assets/web/img/sponsors/8.png')}}" alt="Image"></a>
</li>
</ul>

</div>
</div>
</div>



<footer id="footer">




<div class="container">
<div class="row paddings-mini">

<div class="col-sm-6 col-md-3">
<div class="border-right txt-right">
<h4>Contactez nous</h4>
<ul class="contact-footer">
<li>
<i class="fa fa-envelope"></i> <a href="#"><span class="__cf_email__" data-cfemail="a5d6c4c9c0d6e5c6cacad5c7c4cbce8bc6cac8">contact@fanepia.com</span></a>
</li>
<li>
<i class="fa fa-headphones"></i> <a href="#">+229 96 96 97 97</a>
</li>
<li class="location">
<i class="fa fa-home"></i> <a href="#"> Cotonou Bénin</a>
</li>
</ul>
<div class="logo-footer">
<div class="icon-logo">
<i class="fa fa-university"></i>
</div>
<a href="index.html">
FANEPIA
<span>Lorem upson mieu ndes dzid nd</span>
</a>
</div>
</div>
 </div>


<div class="col-sm-6 col-md-3">
<div class="border-right border-right-none">
<h4>Liens utiles</h4>
<ul class="list-styles">
<li><i class="fa fa-check"></i> <a href="{{url('login')}}">Se connecter</a></li>
<li><i class="fa fa-check"></i> <a href="{{url('register')}}">S'inscrire</a></li>
<li><i class="fa fa-check"></i> <a href="#">Actualités</a></li>
<li><i class="fa fa-check"></i> <a href="#">Offres</a></li>
</ul>
</div>
</div>


<div class="col-sm-6 col-md-4">
<div class="border-right txt-right">
<h4>Newsletter</h4>
<p>S'il vous plaît entrez votre e-mail et abonnez-vous à notre newsletter.</p>
<form id="newsletterForm" class="newsletterForm" action="">
<div class="input-group">
<span class="input-group-addon">
<i class="fa fa-envelope"></i>
</span>
<input class="form-control" placeholder="Adresse Email" name="email" type="email" required="required">
<span class="input-group-btn">
<button class="btn btn-primary" type="submit" name="subscribe" style="background-color: #eb4436;">VALIDER</button>
</span>
</div>
</form>
<div id="result-newsletter"></div>
</div>
</div>


<div class="col-sm-6 col-md-2">
<div class="border-right-none">
<h4>Nous suivre</h4>
<ul class="social">
<li class="facebook"><span><i class="fa fa-facebook"></i></span><a href="#">Facebook</a></li>
<li class="twitter"><span><i class="fa fa-twitter"></i></span><a href="#">Twitter</a></li>
<li class="github"><span><i class="fa fa-github"></i></span><a href="#">Github</a></li>
<li class="linkedin"><span><i class="fa fa-linkedin"></i></span><a href="#">Linkedin</a></li>
</ul>
</div>
</div>

</div>
</div>


<div class="footer-down">
<div class="container">
<div class="row">
<div class="col-md-7">

<ul class="nav-footer" style="visibility:hidden">
<li><a href="#">HOME</a> </li>
<li><a href="#">COMPANY</a></li>
<li><a href="#">SERVICES</a></li>
<li><a href="#">NEWS</a></li>
<li><a href="#">PORTFOLIO</a></li> 
<li><a href="#">CONTACT</a></li>
</ul>

</div>
<div class="col-md-5">
<p>&copy; 2019 Fanepia. Tous les droits réservés.</p>
</div>
</div>
</div>
</div>

</footer>



</div>



<script data-cfasync="false" src="{{asset('assets/web/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script src="{{asset('assets/web/js/libs/jquery.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript"></script>
<script src="{{asset('assets/web/js/libs/jquery-ui.1.10.4.min.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript')}}"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/totop/jquery.ui.totop.js')}}"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

<script src="{{asset('assets/web/js/maps/gmap3.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/fancybox/jquery.fancybox.js')}}"></script>

<script src="{{asset('assets/web/js/carousel/owl.carousel.min.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript"></script>

<script src="{{asset('assets/web/js/filters/jquery.isotope.js')}}" type="d9549c337d2e86117d7fdde0-text/javascript"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/parallax/parallax.min.js')}}"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/theme-options/theme-options.js')}}"></script>
<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/theme-options/jquery.cookies.js')}}"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/bootstrap/bootstrap.js')}}"></script>
<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/bootstrap/bootstrap-slider.js')}}"></script>

<script type="d9549c337d2e86117d7fdde0-text/javascript" src="{{asset('assets/web/js/main.js')}}"></script>


<script type="d9549c337d2e86117d7fdde0-text/javascript">
            jQuery(document).ready(function() {
                jQuery('.tp-banner').show().revolution({
                    dottedOverlay:"none",
                    delay:5000,
                    startwidth:1170,
                    startheight:490,
                    minHeight:350,
                    navigationType:"none",
                    navigationArrows:"solo",
                    navigationStyle:"preview"
                });             
            }); //ready
        </script>

<script src="{{asset('assets/web/ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js')}}" data-cf-settings="d9549c337d2e86117d7fdde0-|49" defer=""></script></body>

</html>