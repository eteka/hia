@extends('layouts.web')
@section('title',"Présentation")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">

							@if(!empty($data->image))
							<div class="clearfix">
								<div class="img-crop">
									<img src="{{asset($data->image)}}" class="img-responsive" alt="">
									<div class="img-credits">
										<div class="col-md-10">
											<a class="post-category" href="#">Pésentation</a><a class="post-category" href="#">HIA-CHU</a>
											<div class="post-title"><h1>{{$data->titre}}</h1></div>
											<div class="post-description"><p>{{$data->legende}}</p></div>
											<div class="post-info clearfix">
												
												<!--span class="sepr">-</span-->
												<span class="date"><i class="fa fa-clock-o"></i> {{date('d M, Y',strtotime($data->created_at))}}</span>
												<!--span class="sepr">-</span>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-o"></i>
												</span-->
											</div>
										</div>
									</div>
								</div><!-- img-crop -->
							</div>
							@else
							<div class=""><h2 class="page-header">{{$data->titre}}</h2></div>
							@endif
							
							<div class="row" style="transform: none;">
								<!-- Post Content Start -->
								<div class="fbt-col-lg-12 col-md-12 single-post-container clearfix">
									<div class="row">
										<div class="col-md-12">
											<!-- Post Share Start -->
											<!--div class="post-share clearfix">
												<ul>
													<li><a class="facebook df-share" data-sharetip="Share on Facebook!" href="#" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i> <span class="social-text">Facebook</span></a></li>
													<li><a class="twitter df-share" data-hashtags="" data-sharetip="Share on Twitter!" href="#" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> <span class="social-text">Tweeter</span></a></li>
													<li><a class="google df-pluss" data-sharetip="Share on Google+!" href="#" rel="nofollow" target="_blank"><i class="fa fa-google-plus"></i> <span class="social-text">Google+</span></a></li>
													<li><a class="pinterest df-pinterest" data-sharetip="Pin it" href="#" target="_blank"><i class="fa fa-pinterest-p"></i> <span class="social-text">Pinterest</span></a></li>
												</ul>
											</div--><!-- Post Share End -->
											
											<div class="clearfix"></div>
										
											<div class="post-text-content clearfix">
												{!!$data->contenu!!}
											</div>

											
											<div class="clearfix"></div>
									
											<div class="fbt-related-posts clearfix">
											</div><!-- Related Posts End -->
										
										</div>
									</div>
								</div><!-- Post Content End -->
								
								
								
							</div>
							
						</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
