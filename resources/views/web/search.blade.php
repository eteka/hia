@extends('layouts.web')
@section('title',"Recherche")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">Résultat de la recherche :</h2></div>
						<div class="well rond0">	
							<form class="navbar-forms" method="GET" action="{{route('web.search')}}" role="search">
							<div class="input-group">

                                    {!! Form::text('search', null, ['class' => 'form-control rond0  search-control','id'=>"searchs",'placeholder'=>"Rechercher dans les actualités... ?"]) !!}
                                    <span class="input-group-btn">
                                        <button class="btn btn-default search-btn"><i class="fa fa-search "></i></button>
                                    </span>
                                </div>
							
                                {{csrf_field()}}
                                 
                            </form>
						</div>
								<div class="rows">
									@if(isset($datas) && $datas->count())
									@foreach($datas as $a)
									<div class="row fbt-vc-inner post-grid clearfix">
										<div class="col-md-4 col-xs-12">	
												<div class="img-thumb">
												<a href="{{route('web.actualite',$a->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($a->photo)}}')"></div></a>
											</div>
										</div>
									<div class="col-md-8 col-xs-12 ">
										<div class="post-item clearfix">
											
											<div class="post-content">
												
												<a href="{{route('web.actualite',$a->slug)}}"><h3>{{$a->titre}}</h3></a>
												<div class="post-info clearfix">
													<span><a class="post-category" href="#">{{$a->categorie?$a->categorie->nom:''}}</a></span>
													<span>-</span>
													<span>{{date('M d, Y',strtotime($a->created_at))}}</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<hr class="pad0">	
									</div>
									</div>
									@endforeach
									@else
									<div class="pad10s">	
									<div class="borderc text-center rond3 pad-hug bge">	
										<h1><i class="fa fa-newspaper-o">	</i>	</h1>
										<h4 class="text-danger">Aucune actualité trouvé pour le moment</h4>
										<small class="text-muted">	Essayez avec un nouveau mot clé !</small>
									</div>
									</div>
									@endif
								</div>
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
