@extends('layouts.web')
@section('title',"Services Médico-techniques")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">Services Médico-techniques</h2></div>
						
								<div class="">
									@if(isset($datas) && $datas->count())
									Liste des services disponibles au <b>Services Médico-techniques</b> sont:
									<ul class="category-list list-on-bar">
									@foreach($datas as $a)
									<li> <a class="text-black" href="{{route('web.services',$a->slug)}}">{{$a->titre}}</a></li>
									@endforeach
									</ul>
									@else
									<div class="pad10">	
									<div class="borderc text-center rond3 pad-hug bge">	
										<h1><i class="fa fa-tag">	</i>	</h1>
										<h4>Aucun service disponible pour le moment</h4>
										<small class="text-muted">	Les services disponibles apparaissent ici</small>
									</div>
									</div>
									@endif
								</div>
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
