@extends('layouts.web')
@section('title',"Bienvenue sur à l'HIA-CHU")
@section('content')
<section id="fp-container" class="clearfix">
            <div class="container">
                <div class="row">
                    <div class="feuture-posts">
                        <!-- Slider Start -->
                        @if(isset($carrousels))
                        <div class="col-sm-7 big-section">
                            <div class='fbt-slide-nav'>
                                <span class='fbt-slide-pager'></span>
                            </div>
                            <div class="fp-slides">
                                @foreach($carrousels as $slide)
                                <div class="img-thumb">
                                    <a href="#"><div class="fbt-resize" style="background-image: url('{{asset($slide->image)}}')"></div></a>
                                    <div class="img-credits">
                                        <!--a class="post-category" href="#">Cooking</a-->
                                        <a href="{{url('#')}}"><h3>{{$slide->titre}}</h3></a>
                                         <span><a class="text-warning" href="#">{{$slide->sous_titre}}</a></span>
                                         <br>   
                                        <div class="post-info">
                                            <span>{{date('M d,Y',strtotime($slide->created_at))}}</span>
                                           
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                                
                            </div>
                            <nav class="nav-growpop">
                                <div>
                                    <a class="fp-prev" href="#fp-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                    <a class="fp-next" href="#fp-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </nav>
                        </div>
                        @endif
                        <!-- Slide Small Start -->
                        <div class="col-sm-5 small-section">
                            @if(isset($presentation))
                            <div class="img-thumb first">
                                <a href="{{route('web.getpage','presentation')}}"><div class="fbt-resize" style="background-image: url({{asset($presentation->image)}})"></div></a>
                                <div class="img-credits">
                                    <a class="post-category" href="#">HIA-CHU</a>
                                    <a href="#"><h3>{{$presentation->titre}}</h3></a>
                                    <div class="post-info">
                                        <span>{{date('M d,Y',strtotime($presentation->created_at))}}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="fp-small">
                                     @if(isset($service1))
                                    <div class="col-xs-6 last-small">
                                        <div class="img-thumb">
                                            <a href="{{route('web.service_med_tech')}}"><div class="fbt-resize" style="background-image: url('{{asset($service1->image)}}')"></div></a>
                                            <div class="img-credits">
                                                <a class="post-category" href="{{route('web.service_med_tech')}}">Service</a>
                                                <a href="{{route('web.service_med_tech')}}"><h3>{{$service1->titre}}</h3></a>
                                                <div class="post-info">
                                                    <span>{{date('M d,Y',strtotime($service1->updated_at))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                     @if(isset($service2))
                                    <div class="col-xs-6 last-small">
                                        <div class="img-thumb">
                                            <a href="{{route('web.getpage','informations-utiles')}}"><div class="fbt-resize" style="background-image: url('{{asset($service2->image)}}')"></div></a>
                                            <div class="img-credits">
                                                <a class="post-category" href="{{route('web.getpage','informations-utiles')}}">Informations</a>
                                                <a href="{{route('web.getpage','informations-utiles')}}"><h3>{{$service2->titre}}</h3></a>
                                                <div class="post-info">
                                                    <span>{{date('M d,Y',strtotime($service2->updated_at))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div><!-- Slide Small End -->
                    </div>
                </div>
            </div>
        </section>
        <section id="main-content" class="clearfix">
            <div class="container">
                <div class="row">
                    <div class="outer-wrapper clearfix">
                        <!-- Main Wrapper Start -->
                        <div class="fbt-col-lg-9 col-md-8 col-sm-6 main-wrapper clearfix">
                            <div class="row">
                               
                                
                                <!-- Content Start -->
                                <div class="fbt-col-lg-12 col-md-12">
                                    <div class="row">
                                    
                                        <!-- Vertical Mag 1 Start -->

                                        <div class="col-md-6 fbt-vc-inner clearfix">
                                            <div class="title-wrapper color-5">
                                                <h2><span>Mot de bienvenue</span></h2>
                                            </div>
                                             @if(isset($mot_bienvenu))
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="#"><div class="fbt-resize" style="background-image: url('{{asset($mot_bienvenu->image)}}')"></div></a>
                                                </div>
                                                <div class="post-content">
                                                    <a href="{{route('web.getpage',$mot_bienvenu->slug)}}"><h3>{{str_limit($mot_bienvenu->legende,70)}}</h3></a>
                                                    <div class="post-info clearfix">
                                                        <!--span><a href="#">{{$mot_bienvenu->legende}}</a></span-->
                                                        <span class="fa fa-clock"> </span>
                                                        <span>{{date('M d, Y', strtotime($mot_bienvenu->updated_at))}}</span>
                                                    </div>
                                                    <div class="text-content">
                                                        <?php
                                                            $content= strip_tags($mot_bienvenu->contenu);
                                                        ?>
                                                        <p>
                                                            {{str_limit($content,200)}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            
                                        </div><!-- Vertical Mag 1 End -->
                                        
                                        <!-- Vertical Mag 2 Start -->
                                        <div class="col-md-6 fbt-vc-inner clearfix">
                                            <div class="title-wrapper color-9">
                                                <h2><span>A LA UNE</span></h2>
                                            </div>
                                            @if(isset($une))
                                             <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="{{route('web.actualite',$une->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($une->photo)}}')"></div></a>
                                                </div>
                                                <div class="post-content">
                                                    <a href="{{route('web.actualite',$une->slug)}}"><h3>{{str_limit($une->titre,50)}}</h3></a>
                                                    <div class="post-info clearfix">
                                                        <!--span><a href="#">{{$mot_bienvenu->legende}}</a></span-->
                                                        <span class="fa fa-play"> </span>
                                                        <span>{{date('M d, Y', strtotime($une->updated_at))}}</span>
                                                    </div>
                                                    <div class="text-content">
                                                        <?php
                                                            $content= strip_tags($une->corps);
                                                           // dd($une);
                                                        ?>
                                                        <p>
                                                            {{str_limit($content,200)}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                             @endif
                                            
                                            
                                        </div><!-- Vertical Mag 2 End -->
                                    </div>
                                    @if(isset($news))
                                    <div class="main-carousel">
                                        <div class="title-wrapper color-14">
                                            <h2><span>Actualités</span></h2>
                                        </div>
                                        
                                        <div class="carousel-content-box owl-wrapper clearfix">
                                            <div class="owl-carousel" data-num="3">
                                                @foreach($news as $nactu)
                                                <div class="item fbt-hr-crs">
                                                    <div class="post-item clearfix">
                                                        <div class="img-thumb">
                                                            <a href="{{route('web.actualite',$une->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($nactu->photo)}}')"></div></a>
                                                        </div>
                                                        <div class="post-content">
                                                            <a href="{{route('web.actualite',$une->slug)}}"><h3>{{$nactu->titre}}.</h3></a>
                                                            <div class="post-info clearfix">
                                                                <span><a href="#">HIA-CHU</a></span>
                                                                <span>-</span>
                                                                <span>{{date('D d M, Y',strtotime($nactu->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                
                                                
                                                
                                            </div>

                                                            
                                        </div>
                                            <br>    
                                            <a class="btn btn-primary" href="{{route('web.actualites')}}"><i class="fa fa-plus">   </i> Toutes les actualités</a>
                                    </div>
                                    @endif
                                    <!-- Gallery Start -->
                                    @if(isset($galeries) && $galeries->count())
                                    <div class="gallery">
                                        <div class="title-wrapper color-6">
                                            <h2><span>Galerie images</span></h2>
                                        </div>
                                        <div class="row">
                                            <div class="gallery-img">
                                                @foreach($galeries as $g)
                                                <div class="col-md-4 col-xs-6 padding-1">
                                                    <div class="post-item clearfix">
                                                        <div class="img-thumb">
                                                            <a href="{{asset($g->image)}}" data-lightbox="roadtrip" data-lightbox="image-{{$g->id}}" data-title="{{ isset($g->titre) ? $g->titre : '' }}"><div class="fbt-resize" style="background-image: url('{{asset($g->image)}}')"></div></a>
                                                            <div class="img-credits">
                                                                <h3>{{$g->titre}}</h3>
                                                                <div class="post-info clearfix">
                                                                    <span>{{date('M d, Y h\H:i\'',strtotime($g->created_at))}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <br>    
                                            <a class="btn  btn-sm btn-block rond0 bge borderc" href="{{route('web.galerie_images')}}"><i class="fa fa-plus">   </i> Plus d'images</a>
                                    </div>
                                    @endif
                                    
                                    
                                    <div class="row">
                                        <!-- Biography Carousel Start -->
                                        @if(isset($personnels))
                                        <div class="col-md-5">
                                            <div class="biography-carousel clearfix">
                                                <div class="title-wrapper color-10">
                                                    <h2><span>Vue sur le Personnel</span></h2>
                                                </div>

                                                <div class="carousel-content-box owl-wrapper clearfix">
                                                    <div class="owl-carousel" data-num="1">
                                                        @foreach($personnels as $p)
                                                        <div class="item fbt-hr-crs">
                                                            <div class="post-item clearfix">
                                                                <div class="img-thumb">
                                                                    <a href="{{route('web.personnel',$p->id)}}"><div class="fbt-resize" style="background-image: url('{{asset($p->photo)}}')"></div></a>
                                                                    <div class="img-credits">
                                                                        <a href="#"><h3>{{$p->nom .' '.$p->prenom}}</h3></a>
                                                                        <div class="post-info clearfix">
                                                                            <span>{{date('M d, Y',strtotime($p->created_at))}}</span>
                                                                        </div>
                                                                        <div class="text-content">
                                                                            <p>{{$p->poste}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        
                                                    </div>  
                                            <a class="btn btn-danger rond0 btn-block" href="{{route('web.personnels')}}"><i class="fa fa-plus">   </i> Voir tout le personnel</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        
                                        <!-- Small List Start -->
                                        @if(isset($ressources) && $ressources->count())
                                        <div class="col-md-7 col-sm-12 fbt-vc-inner nude clearfix">
                                            <div class="title-wrapper color-11">
                                                <h2><span>Ressources</span></h2>
                                            </div>
                                            <br>    
                                            @foreach($ressources as $r)
                                            <div class="post-item small">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12">
                                                        <div class="post-content">
                                                            <a href="{{route('web.ressource.download',$r->id)}}"><h3> <i class="fa fa-file">  </i> {{$r->titre}}.</h3></a>
                                                            <div class="post-info_ clearfix">
                                                                <span><a class="btn btn-xs btn-success" href="{{route('web.ressource.download',$r->id)}}"><i class="fa fa-download"> </i> Télécharger</a></span>
                                                                <span>-</span>
                                                                <span>{{date('M d, Y',strtotime($r->created_at))}}</span>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>    
                                            @endforeach
                                            <br>    
                                            <a class="btn btn-sucess" href="{{route('web.ressources')}}"><i class="fa fa-plus">   </i> Plus de ressources</a>
                                            
                                            
                                        </div><!-- Small List End -->
                                        @endif
                                    </div>
                                    
                                    
                                 
                                    
                                </div><!-- Content End -->
                            </div>

                        </div><!-- Main Wrapper End -->
                        
                        <!-- Sidebar Start -->
                        <div class="fbt-col-lg-3 col-md-4 col-sm-6 sidebar">
                            <div class="theiaStickySidebar">
                                <!-- Social Counter Start -->
                                <div class="widget">
                                    <div class="social-counter">
                                        <div class="title-wrapper color-13">
                                            <h2><span>Restez connecté</span></h2>
                                        </div>
                                        <div class="social-item last clearfix">
                                            <div class="social-fb clearfix">
                                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <span class="left">14 Fans</span>
                                                <span class="right">Facebook</span>
                                            </div>
                                        </div>
                                        <div id="rs" class="tab-pane tab-pane fade in active">  
                                            <div class="fb-page" data-href="https://web.facebook.com/hia.chu.parakou" data-tabs="timeline" data-width="350" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- Sidebar Tabs Start -->
                                <div class="widget clearfix">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#home">Récents</a></li>
                                        <li><a data-toggle="tab" href="#menu1">Vidéos</a></li>
                                        <!--li><a data-toggle="tab" href="#menu2">Autres </a></li-->
                                    </ul>
                                    <div class="tab-content">
                                        <!-- Tab 1 -->
                                        <div id="home" class="tab-pane fade in active">
                                            <!-- Sidebar Small List Start -->
                                            <div class="fbt-vc-inner">
                                                @if(isset($news))
                                                @foreach($news as $new)
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="{{route('web.actualite',$new->slug)}}">
                                                                    <div class="fbt-resize" style="background-image: url('{{asset($new->photo)}}')"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="{{route('web.actualite',$new->slug)}}">
                                                                    <h3>{{str_limit($new->titre,60)}}</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Mar 13, 2016</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div><!-- Sidebar Small List End -->
                                        </div>
                                        <!-- Tab 2 -->
                                        <div id="menu1" class="tab-pane fade">
                                            <!-- Sidebar Vertical Mag 5 Start -->
                                            <div class="fbt-">
                                                 @if(isset($videos) && $videos->count())
                                                @foreach($videos as $v)
                                                    <div class="row">   
                                                    <div class="col-md-12">   
                                                        <div class="embed-responsive_ embed-responsive-21by9">
                                                            {!! $v->url_video_youtube !!} 
                                                        </div>
                                                    </div>
                                                    </div>
                                                @endforeach
                                                @endif
                                               
                                               
                                                
                                            </div><!-- Sidebar Vertical Mag 5 End -->
                                        </div>
                                        <!-- Tab 3 -->
                                        <!--div id="menu2" class="tab-pane fade">
                                            <div class="fbt-vc-inner">
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="#">
                                                                    <div class="fbt-resize" style="background-image: url('assets/web/img/img-7.jpg')"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="#">
                                                                    <h3>Aliquam metus mauris, litora orci ligula.</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Mar 13, 2016</span>
                                                                    <span>-</span>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="#">
                                                                    <div class="fbt-resize" style="background-image: url('assets/web/img/img-9.jpg')"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="#">
                                                                    <h3>China's drive to become a rugby union superpower.</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Mar 10, 2016</span>
                                                                    <span>-</span>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="#">
                                                                    <div class="fbt-resize" style="background-image: url(img/img-16.jpg)"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="#">
                                                                    <h3>Nam iusto delicata ne, eam dolore singulis maiestatis ex.</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Mar 3, 2016</span>
                                                                    <span>-</span>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="#">
                                                                    <div class="fbt-resize" style="background-image: url('assets/web/img/img-28.jpg')"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="#">
                                                                    <h3>Dolor ut a est maecenas, neque odio dui leo lacus varius.</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Mar 2, 2016</span>
                                                                    <span>-</span>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-item small">
                                                    <div class="row">
                                                        <div class="col-sm-4 col-xs-3">
                                                            <div class="img-thumb">
                                                                <a href="#">
                                                                    <div class="fbt-resize" style="background-image: url(img/img-22.jpg)"></div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8 col-xs-9 no-padding-left">
                                                            <div class="post-content">
                                                                <a href="#">
                                                                    <h3>Etiam duis nunc dui ad sagittis, mauris at rem, in nunc.</h3>
                                                                </a>
                                                                <div class="post-info clearfix">
                                                                    <span>Feb 23, 2016</span>
                                                                    <span>-</span>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- Sidebar Small List End -->
                                        </div-->
                                    </div>
                                </div><!-- Sidebar Tabs End -->
                                
                                <!-- Sidebar Carousel Start -->
                                @if(isset($breves))
                                <div class="widget sidebar-carousel clearfix">
                                    <div class="title-wrapper color-8">
                                        <h2><span>A lire dans les brèves</span></h2>
                                    </div>

                                    <div class="carousel-content-box owl-wrapper clearfix">
                                        <div class="owl-carousel" data-num="1">
                                            @foreach($breves as $breve)
                                            <div class="item fbt-hr-crs">
                                                <div class="post-item clearfix">
                                                    <div class="img-thumb">
                                                        <a href="#"><div class="fbt-resize" style="background-image: url('{{asset($breve->photo)}}')"></div></a>
                                                        <div class="img-credits">
                                                            <a href="{{route('web.breve',$breve->slug)}}"><h3>{{$breve->titre}}.</h3></a>
                                                            <div class="post-info clearfix">
                                                                <span>{{date('M d,Y',strtotime($breve->created_at))}}</span>
                                                            </div>
                                                            <div class="text-content">
                                                                <?php
                                                                    $content= strip_tags($breve->contenu);
                                                                ?>
                                                                <p>
                                                                    {{str_limit($content,200)}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                
                                
                            </div>
                        </div><!-- Sidebar End -->
                    </div>
                    
                    
                </div>
            </div>
            
            <!-- Big Gallery Start -->
            <!--div class="gallery bgallery clearfix">
                <div class="container">
                    <div class="row">
                        <div class="gallery-img clearfix">
                            <div class="col-sm-6 padding-1">
                                <div class="post-item clearfix">
                                    <div class="img-thumb">
                                        <a href="#"><div class="fbt-resize" style="background-image: url('assets/web/img/game-1.jpg')"><span class="video-icon"><i class="fa fa-play"></i></span></div></a>
                                        <div class="img-credits">
                                            <a class="post-category" href="#">Video-games</a>
                                            <a href="#"><h3>Nam iusto delicata ne, eam dolore singulis maiestatis ex.</h3></a>
                                            <div class="post-info clearfix">
                                                <span><a href="#">Mark Spenser</a></span>
                                                <span>-</span>
                                                <span>Apr 30, 2016</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 padding-1">
                                <div class="post-item clearfix">
                                    <div class="img-thumb">
                                        <a href="#"><div class="fbt-resize" style="background-image: url('assets/web/img/game-2.jpg')"><span class="video-icon"><i class="fa fa-play"></i></span></div></a>
                                        <div class="img-credits">
                                            <a class="post-category" href="#">Video-games</a>
                                            <a href="#"><h3>Sem tincidunt arcu pellentesque suscipit accumsan.</h3></a>
                                            <div class="post-info clearfix">
                                                <span><a href="#">Mark Spenser</a></span>
                                                <span>-</span>
                                                <span>Apr 18, 2016</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div--><!-- Big Gallery End -->
            
                  
        </section>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('assets/plugins/lightbox/css/lightbox.min.css')}}">
@endsection
@section('script')
<script src="{{asset('assets/plugins/lightbox/js/lightbox.min.js')}}"></script>

@endsection