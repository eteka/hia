@extends('layouts.web')
@section('title',"Contactez nous !")
@section('content')

<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">
	<div class="col-md-12">	

		<div class=""><h2 class="page-header">Contact</h2></div>
		<div >	
		<p class="text-white">
                             @if(isset($bcontact))
                            
                            {!! $bcontact->contenu !!}
                            @else
                            <p class="">
                                Hôpital d’Instruction des Armées-Centre Hospitalier Universitaire de Parakou.  <br>  
                            <b> Courrier Postal : </b>02  BP : 1324 Parakou-Bénin <br>      
                            <b>E-mail</b> : hiachu2@gmail.com <br>  
                            <b>Tel : </b>+229 90 97 20 12
                            </p>
                            @endif
                            <hr>	
		</div>

	</div>	
	<div class="col-md-8 post-wrapper " style="transform: none;">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3937.1914172326974!2d2.6039021689825153!3d9.316300937250784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe1709a2e9f43aade!2sH%C3%B4pital%20Des%20Instructions%20Des%20Arm%C3%A9es!5e0!3m2!1sfr!2sbj!4v1567457388663!5m2!1sfr!2sbj" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>			
						
	</div>
	<div class="col-md-4">
		
		@if(isset($sendmessage) && $sendmessage)
		<fieldset class="fieldset">	
			<br>	
			<br>	
			<br>	
		<div class="card bg-white-in">	
			<div class="card-body ">	
				<h4>Votre message a été envoyé avec succès !</h4>
				<small class="text-small text-success">Nous vous remercions pour la confiance. Nous allons traiter votre requête afin de vous répondre le plus vite possible.
				</small><br>	
			</div>
		</div>
		</fieldset>
		@else
		{!! Form::open(['method' => 'POST', 'url' => '/contact', 'class' => 'navbar-formnavbar-right mt30', 'role' => 'search',])  !!}
	<fieldset class="fieldset">	
		<div class="card">	
			<div class="card-body">	
				<h4>	Laissez nous votre message</h4>
						<div class="form-group row  {{ $errors->has('nom') ? 'has-error' : ''}}">
    <div class="col-md-12">
        {!! Form::text('nom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','placeholder'=>"Votre nom de famille"]) !!}

        {!! $errors->first('nom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('prenom') ? 'has-error' : ''}}">
   <div class="col-md-12">
        {!! Form::text('prenom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','placeholder'=>"Votre prénom "]) !!}

        {!! $errors->first('prenom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('tel') ? 'has-error' : ''}}">
    <div class="col-md-12">
        {!! Form::text('tel', null, ['class' => 'form-control form-control-sm','placeholder'=>"Téléphone /email"]) !!}

        {!! $errors->first('tel', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('objet') ? 'has-error' : ''}}">
   <div class="col-md-12">
        {!! Form::text('objet', null, ['class' => 'form-control form-control-sm','placeholder'=>"Objet du message"]) !!}

        {!! $errors->first('objet', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('message') ? 'has-error' : ''}}">
    <div class="col-md-12">
        {!! Form::textarea('message', null, ['class' => 'form-control pad15 rond5 form-control-sm no-resize','rows'=>5,'placeholder'=>"Rédigez le message"]) !!}

        {!! $errors->first('message', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Envoyer le message', ['class' => 'btn btn-success']) !!}
    </div>
</div>
@endif
			</div>
		</div>
	</fieldset>
	</form>

	</div>
	
</div>
</div>
</div>
@endsection('content')
