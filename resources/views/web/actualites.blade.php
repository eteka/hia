@extends('layouts.web')
@section('title',"Actualités")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">Actualités</h2></div>
								<div class="row">
									@if(isset($datas) && $datas->count())
									@foreach($datas as $a)
									<div class="col-md-6 col-sm-12 col-xs-6 fbt-vc-inner post-grid clearfix">
										<div class="post-item clearfix">
											<div class="img-thumb">
												<a href="{{route('web.actualite',$a->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($a->photo)}}')"></div></a>
											</div>
											<div class="post-content">
												
												<a href="{{route('web.actualite',$a->slug)}}"><h3>{{$a->titre}}</h3></a>
												<div class="post-info clearfix">
													<span><a class="post-category" href="#">{{$a->categorie?$a->categorie->nom:''}}</a></span>
													<span>-</span>
													<span>{{date('M d, Y',strtotime($a->created_at))}}</span>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									@else
									<div class="pad10">	
									<div class="borderc text-center rond3 pad-hug bge">	
										<h1><i class="fa fa-info-circle">	</i>	</h1>
										<h4>Aucune actualité disponible pour le moment</h4>
										<small class="text-muted">	Les actualités disponibles apparaissent ici</small>
									</div>
									</div>
									@endif
								</div>
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
