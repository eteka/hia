@extends('layouts.web')
@section('title',isset($titre)&& $titre!=""?$titre:'Passation des marchés')
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">{{isset($titre) && $titre!=""?$titre:"Passation des marchés"}}</h2></div>
								
									@if(isset($datas) && $datas->count())
									@foreach($datas as $r)
									<?php
									$contenu=strip_tags(str_limit($r->eontenu,200));
									?>
									<div class="row">
										<div class="col-sm-12 col-md-2">
											<i style="font-size: 90px;color:#aaa" class="fa fa-folder">  </i>
										</div>	
                                        <div class="col-sm-12 col-sm-10">
                                                        <div class="post-content">
                                                            <a href="{{route('web.page_prmp',$r->slug)}}"><h5>  {{$r->titre}}.</h5></a>
                                                            <div class="post-info clearfix">
                                                               <div>{{$contenu}} </div>
                                                               <div>
                                                                <span>{{date('M d, Y',strtotime($r->created_at))}}</span>
                                                                </div>
                                                            </div>
                                                            
                                                        </div><br>
                                                         <span>
                                                         	<a  class="btn  btn-primary btn-xs "  target="_blanck"   href="{{url($r->fichier)}}"> 
                                                         		<i class="fa fa-download-alt"> </i> Télécharger</a></span>
                                        </div>
                                        <div class="col-md-12">
                                                    <hr class="pad0">	
                                        </div>
                                        </div>

									@endforeach
									@else
									<div class="pad10">	
									<div class="borderc text-center rond3 pad-hug bge">	
										<h1><i class="fa fa-info-circle">	</i>	</h1>
										<h4>Aucun fichier disponible pour le moment</h4>
										<small class="text-muted">	La liste des fichiers apparaît ici</small>
									</div>
									</div>
									@endif
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
