<!DOCTYPE html>
<html lang="en" class="no-js">
	<!-- Head -->
	
<!-- Mirrored from htmlstream.com/preview/awesome-dashboard-ui-kit/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 May 2019 15:43:30 GMT -->
<head>
		<title>@yield('title') - Administration | HIA-CHU de Parakou</title>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.png" type="image/x-icon">

		<!--  Social tags -->
		<meta name="keywords" content="Administration , Hopital, HIA, Parakou, Bénin, CHU, HIA-CHU, Hospital, Armés">
		<meta name="description" content="HIA-CHU Parakou -  Hôpital d’Instruction des Armées-Centre Hospitalier Universitaire de Parakou.">
		<meta name="author" content="ETEKA Wilfried">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<script>
	        window.Laravel = <?php echo json_encode([
	            'csrfToken' => csrf_token(),
	        ]); ?>
    	</script>
		<!-- Schema.org -->
		<meta itemprop="name" content="Administration du site HIA-CHU Parakou">

		<!-- Web Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

		<!-- Components plugins Styles -->
		<link rel="stylesheet" href="{{asset('assets/plugins/themify-icons/themify-icons.css')}}">
		<link rel="stylesheet" href="{{asset('assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}">

		<!-- Theme Styles -->
		<link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">
		@yield('css')
	</head>
	<!-- End Head -->

	<!-- Body -->
	<body>
		<!-- Header (Topbar) -->
		<header class="u-header">
			<!-- Header Left Section -->
			<div class="u-header-left">
				<!-- Header Logo -->
				<a class="u-header-logo" href="{{url('/')}}">
					<!--img class="u-header-logo__icon" src="{{asset('assets/images/logo-hia-mini.jpg')}}" alt="HIA"-->
					<img src="{{asset('assets/images/logo-hia.png')}}" width="190" height="auto" alt="HIA-CHU"/>
				</a>
				<!-- End Header Logo -->
			</div>
			<!-- End Header Left Section -->

			<!-- Header Middle Section -->
			<div class="u-header-middle">
				<!-- Sidebar Invoker -->
				<div class="u-header-section">
					<a class="js-sidebar-invoker u-header-invoker u-sidebar-invoker" href="#"
					   data-is-close-all-except-this="true"
					   data-target="#sidebar">
						<span class="ti-align-left u-header-invoker__icon u-sidebar-invoker__icon--open"></span>
						<span class="ti-align-justify u-header-invoker__icon u-sidebar-invoker__icon--close"></span>
					</a>
				</div>
				<!-- End Sidebar Invoker -->

				<!-- Header Search -->
				<div class="u-header-section justify-content-sm-start flex-grow-1 py-0">
					<div class="u-header-search"
					     data-search-mobile-invoker="#headerSearchMobileInvoker"
					     data-search-target="#headerSearch">
						<!-- Header Search Invoker (Mobile Mode) -->
						<a id="headerSearchMobileInvoker" class="u-header-search__mobile-invoker align-items-center" href="#">
							<span class="ti-search"></span>
						</a>
						<!-- End Header Search Invoker (Mobile Mode) -->

						<!-- Header Search Form -->
						<!--div id="headerSearch" class="u-header-search-form">
							<form action="https://htmlstream.com/" class="w-100">
								<div class="input-group h-100">
									<button class="btn-link input-group-prepend u-header-search__btn" type="submit">
										<span class="ti-search"></span>
									</button>
									<input class="form-control u-header-search__field" type="search" placeholder="Type to search…">
								</div>
							</form>
						</div-->
						<!-- End Header Search Form -->
					</div>
				</div>
				<!-- End Header Search -->

				<!-- Activities -->
				<!--div class="u-header-section">
					<div class="u-header-dropdown dropdown pt-1">
						<a id="activitiesInvoker" class="u-header-invoker d-flex align-items-center" href="#" role="button" aria-haspopup="true" aria-expanded="false"
						   data-toggle="dropdown"
						   data-offset="20">
						  <span class="position-relative">
								<span class="ti-email u-header-invoker__icon"></span>
								<span class="u-indicator u-indicator-top-right u-indicator-xxs bg-success"></span>
							</span>
						</a>

						<div class="u-header-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="activitiesInvoker" style="width: 360px;">
							<div class="card p-3">
								<div class="card-body p-0">
									<div class="list-group list-group-flush">
										
										<a class="list-group-item link-dark px-0" href="#">
											<div class="media align-items-start">
												<img class="u-avatar-sm rounded-circle mr-3" src="assets/img-temp/avatars/img1.jpg" alt="Image description">

												<div class="media-body">
													<h5 class="h4 mb-1">Essie Thompson</h5>
													<p class="text-muted text-lh-1_4 mb-0">There is one way people can make</p>
												</div>
											</div>
										</a>
										<a class="list-group-item link-dark px-0" href="#">
											<div class="media align-items-start">
												<img class="u-avatar-sm rounded-circle mr-3" src="assets/img-temp/avatars/img2.jpg" alt="Image description">

												<div class="media-body">
													<h5 class="h4 mb-1">Chad Harris</h5>
													<p class="text-muted text-lh-1_4 mb-0">Adwords Keyword research</p>
												</div>
											</div>
										</a>
										<a class="list-group-item link-dark px-0" href="#">
											<div class="media align-items-start">
												<img class="u-avatar-sm rounded-circle mr-3" src="assets/img-temp/avatars/img3.jpg" alt="Image description">

												<div class="media-body">
													<h5 class="h4 mb-1">Frederick Henderson</h5>
													<p class="text-muted text-lh-1_4 mb-0">There is no better advertisement</p>
												</div>
											</div>
										</a>
										<a class="list-group-item link-dark px-0" href="#">
											<div class="media align-items-start">
												<img class="u-avatar-sm rounded-circle mr-3" src="assets/img-temp/avatars/img4.jpg" alt="Image description">

												<div class="media-body">
													<h5 class="h4 mb-1">Eliza Rios</h5>
													<p class="text-muted text-lh-1_4 mb-0">There are several ways people</p>
												</div>
											</div>
										</a>
										<a class="list-group-item link-dark px-0" href="#">
											<div class="media align-items-start">
												<img class="u-avatar-sm rounded-circle mr-3" src="assets/img-temp/avatars/user-unknown.jpg" alt="Image description">

												<div class="media-body">
													<h5 class="h4 mb-1">Alfred Moreno</h5>
													<p class="text-muted text-lh-1_4 mb-0">A gentleman from New York discovered</p>
												</div>
											</div>
										</a>
									</div>
								</div>

								<hr class="my-3">

								<div class="card-footer border-0 p-0">
									<a class="font-weight-semi-bold" href="#">All emails</a>
								</div>
							</div>
						</div>
					</div>
				</div-->
				<!-- End Activities -->

				
				<!-- End Notifications -->

				<!-- Apps -->
				<!--div class="u-header-section">
					<div class="u-header-dropdown dropdown pt-1">
						<!--a id="appsInvoker" class="u-header-invoker d-flex align-items-center" href="#" role="button" aria-haspopup="true" aria-expanded="false"
						   data-toggle="dropdown"
						   data-offset="20">
						  <span class="position-relative">
								<span class="ti-layout-grid3 u-header-invoker__icon"></span>
							</span>
						</a-->

						
					</div>
				</div-->
				<!-- End Apps -->

				<!-- User Profile -->
				<div class="u-header-section u-header-section--profile">
					<div class="u-header-dropdown dropdown">
						<a class="link-muted d-flex align-items-center" href="#" role="button" id="userProfileInvoker" aria-haspopup="true" aria-expanded="false"
						   data-toggle="dropdown"
						   data-offset="0">
							@if(!empty(Auth::user()->image))
				              <img class="u-header-avatar img-fluid_ rounded-circle mr-md-3" src="{{asset(Auth::user()->image)}}" alt="{{Auth::user()->prenom}}">

				              @else
				              <img class="u-header-avatar img-fluid_ rounded-circle mr-md-3" src="{{asset('uploads/statics/default-image.jpg')}}" alt="{{Auth::user()->prenom}}">
				            @endif
							<span class="text-black bold d-none d-md-inline-flex align-items-center">
			                {{Auth::user()->prenom}}
			                <span class="ti-angle-down text-muted ml-4"></span>
			              </span>

						</a>

						<div class="u-header-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="userProfileInvoker" style="width: 260px;">
							<div class="card p-3">
								

								<div class="card-body p-0">
									<ul class="list-unstyled mb-0">
										<li class="mb-3">
					                      <a class="link-dark" href="{{route('profil',Auth::user()->pseudo)}}"><i class="fa ti-user"></i> Mon Profile</a>
					                    </li>
					                    <li class="mb-3">
					                      <a class="link-dark" href="{{route('edit-profil',Auth::user()->pseudo)}}"><i class="ti-pencil"></i> Modifier  mon compte </a>
					                    </li>
					                    <li class="mb-3">
					                      <a class="link-dark" href="{{route('password',Auth::user()->pseudo)}}"><i class="ti-lock"></i> Changer mot de passe </a>
					                    </l>

					                    <li>
					                        <a class="link-dark" href="{{ url('/logout') }}"
					                            onclick="event.preventDefault();
					                                     document.getElementById('logout-form').submit();">
					                            <i class="ti-reload"></i>  Déconnexion
					                        </a>

					                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
					                            {{ csrf_field() }}
					                        </form>
					                    </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End User Profile -->
			</div>
			<!-- End Header Middle Section -->
		</header>
		<!-- End Header (Topbar) -->

		<!-- Main -->
		<main class="u-main">
			<!-- Sidebar -->
			<aside id="sidebar" class="u-sidebar">
				<div class="u-sidebar-inner">
					<!-- Sidebar Header -->
					<header class="u-sidebar-header">
						<!-- Sidebar Logo -->
						<a class="u-sidebar-logo" href="{{url('/')}}">
							<img src="{{asset('assets/images/logo-hia.png')}}" width="170" height="auto" alt="HIA-CHU"/>
						</a>
						<!-- End Sidebar Logo -->
					</header>
					<!-- End Sidebar Header -->

					<!-- Sidebar Nav -->
					@include('admin.sidebar')
					<!-- End Sidebar Nav -->
				</div>
			</aside>
			<!-- End Sidebar -->

			<!-- Content -->
			<div class="u-content">
				<!-- Content Body -->
				<div class="u-body">
					<div class="row">	
						<div class="col-md-11">	
							 @if(Session::has('line'))
            <div class="alert alert-dars border bg-dark2  rond0 fade show" role="alert">
                  <div class="col-sm-2 col-md-1">
                    <span class="alert-icons ti-user mr-3s"></span>
                  </div>
                  <div class="col-sm-10 col-md-9">
                    {!! Session::get('flash_message') !!}
                  </div>

                  <!--button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button-->
                </div>
              @endif
             @if(Session::has('flash_message'))
                <div class="alert alert-primary fade show" role="alert">
                  <span class="alert-icon ti-flag-alt mr-3"></span>
                  {!! Session::get('flash_message') !!}
                  <button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button>
                </div>
              @endif
              @if(Session::has('danger'))
                <div class="alert alert-danger-soft fade show" role="alert">
                  <span class="alert-icon ti-alert mr-3"></span>
                  {!! Session::get('danger') !!}
                  <button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button>
                </div>
              @endif
              @if(Session::has('info'))
                <div class="alert alert-info fade show" role="alert">
                  <span class="alert-icon ti-info-alt mr-3"></span>
                  {!! Session::get('info') !!}
                  <button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button>
                </div>
              @endif
              @if(Session::has('warning'))
                <div class="alert alert-warning-soft fade show" role="alert">
                  <span class="alert-icon ti-help-alt mr-3"></span>
                  {!! Session::get('warning') !!}
                  <button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button>
                </div>
              @endif
              @if(Session::has('success'))
                <div class="alert alert-secondary fade show" role="alert">
                  <span class="alert-icon ti-check mr-3"></span>
                  {!! Session::get('success') !!}
                  <button class="close" type="button" aria-label="Close" data-dismiss="alert">
                    <span class="ti-close" aria-hidden="true"></span>
                  </button>
                </div>
              @endif
							@yield('content')
						</div>
					</div>
				</div>
				<!-- End Content Body -->

				<!-- Footer -->
				<footer class="u-footer d-md-flex align-items-md-center text-center text-md-left text-muted">
					<!-- Footer Menu -->
					<ul class="list-inline mb-3 mb-md-0">
						
						<li class="list-inline-item mr-4">
							<a class="text-muted" href="https://www.facebook.com/hia-chu" target="_blank"><i class="ti-info-alt">	</i>A propos du CHU</a>
						</li>
						<li class="list-inline-item mr-4">
							<a class="text-muted" href="https://www.facebook.com/eteka.wilfried" target="_blank"> <i class="ti-user">	</i>A propos du concepteur</a>
						</li>
						
					</ul>
					<!-- End Footer Menu -->

					<!-- Copyright -->
					<span class="text-muted ml-auto">&copy; Copyright <a class="text-muted" href="https://hia-chu.com/" target="_blank">HIA-CHU</a> {{date('Y')}}</a>. Tout droit réservé</span>
					<!-- End Copyright -->
				</footer>
				<!-- End Footer -->
			</div>
			<!-- End Content -->
		</main>
		<!-- End Main -->

		<!-- Global plugins -->
		<script src="{{asset('assets/plugins/jquery/dist/jquery.min.js')}}"></script>
		<script src="{{asset('assets/plugins/jquery-migrate/jquery-migrate.min.js')}}"></script>
		<script src="{{asset('assets/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
		<script src="{{asset('assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		@yield('script')
		<!-- Plugins -->
		<script src="{{asset('assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
		<script src="{{asset('assets/plugins/chart.js/dist/Chart.min.js')}}"></script>
		<script src="assets/plugins/chartjs-plugin-style/dist/chartjs-plugin-style.min.js')}}"></script>

		<!-- Initialization  -->
		<script src="{{asset('assets/js/sidebar-nav.js')}}"></script>
		<script src="{{asset('assets/js/main.js')}}"></script>

		<script src="{{asset('assets/js/charts/area-chart.js')}}"></script>
		<script src="{{asset('assets/js/charts/area-chart-small.js')}}"></script>
		<script src="{{asset('assets/js/charts/doughnut-chart.js')}}"></script>
	</body>
	<!-- End Body -->

<!-- Mirrored from htmlstream.com/preview/awesome-dashboard-ui-kit/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 May 2019 15:43:44 GMT -->
</html>