
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="no-js"> 

<!-- Mirrored from fbtemplates.net/html/glossymag/classic.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Mar 2019 08:15:59 GMT -->
<head>
  <!-- Basic -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="keywords" content="Hopital, Parakou, CHU, HIA, Santé, Bénin, Soins, Mairie Parakou, Borgou" />
  <meta name="description" content="HIA-CHU Parakou, Hopital d'Instruction des Armées-Centre Hospitalier Universitaire de Parakou" />
  
  <title>@yield('title')  | HIA-CHU de Parakou</title>
  
  <link rel="shortcut icon" href="favicon.ico">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/font-awesome/css/font-awesome.min.css')}}" media="screen">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
  <!-- Stylesheets -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/bootstrap.min.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/colors.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/owl.carousel.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/owl.theme.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/style.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/classic.css')}}" media="screen">
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v4.0&appId=1214967528615548&autoLogAppEvents=1"></script>
  
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  @yield('css')
  <style type="text/css">
      .headline-title {
    background: #ec0f0f;
    }
    .form-text{font-size: 10px}
    .page-header{
        margin:10px 0 20px;
    }
    .navbar-brand img{
        border: 3px solid #ffffff;
        border-radius: 5px;
    }
    h2.page-header{
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 28px;
        text-transform: uppercase;
    }
    .header-wrapper{
        padding: 0;
    }
    .logo img{
        margin-top: 40px;
        margin-left: 10px;
    }
    #fp-container{
        padding: 10px 0;
    }
    /*********************************************************************/
    fieldset.fieldset {
            border: 0px solid #ccc;
            margin-bottom: 30px;
            background: #eee;
            background: -webkit-linear-gradient(top, rgba(212,228,239,1) 0%, rgba(134,174,204,1) 100%);
            background: -webkit-linear-gradient(top, rgb(14, 184, 255) 0%, rgb(254, 254, 254) 50%, rgb(255, 255, 255) 51%, rgba(254,254,254,1) 100%);
            background: linear-gradient(top, #eceff3 0%, rgb(254, 254, 254) 50%, rgb(255, 255, 255) 51%, rgba(254,254,254,1) 100%)
            background: -webkit-linear-gradient(top, #eceff3 0%, rgb(254, 254, 254) 50%, rgb(255, 255, 255) 51%, rgba(254,254,254,1) 100%);
            /*box-shadow: 0 4px 10px rgba(60, 64, 67, 0.3);*/
            margin-left: -30px;
            min-height: 450px
        }
        .no-resize{
            resize: none;
        }
        .pad15{padding: 15px;}
        .fieldset .card-body{
            padding: 15px;

        }
        .rond5{border-radius: 5px}
        .fieldset .form-control{
            background-color: #fff;
    background-image: none;
    border: 1px solid #eee9e9;
    box-shadow: none;
    border-radius: 30px;
        }
        .fieldset .btn {
             border-radius: 30px;
        }
        .text-small{font-size: 11px}
        .bg-white-in{
            background: rgba(255,255,255,.5);
            margin:30px;
            text-align: center;
        }
        .fbt-col-lg-9.col-md-12.single-post-container, .single-post-container *{
            font-size: 100% !important;
        }
    /*********************************************************************/
    .pad0{padding: 0}
    .bge{background: #eeeeee;}
    .headline-wrapper{
        background: #065d68;
        background: #009966;/*#166374;*/
        border-bottom: 2px solid #ffe422;
    }
    ul.ticker li a{color: #ffe422}
    .navbar-default.dark {
    background: #2a5d68;
    background: #232f3e;
    }
    ul.ticker li a:hover {
    //color: #ec0f0f;
    transition: ease .28s;
    //letter-spacing: 2px
    }
    ul.dropdown-menu > li {
    
    background: #064460;
    border-top:1px solid #1c5c79;
    }
    img{
        max-width: 100%;
    }
    .rond0{border-radius: 0}
    .rond5{border-radius: 5px}
    .rond10{border-radius: 10px}
    .borderc{border: 1px solid #ccc}
    .pad-hug{padding:30px;}
    .text-white{color:#ffffff;}
    .pad10{padding: 10px}
    .pad5{padding: 5px}
    a:link {
    -webkit-tap-highlight-color: #ffcb0c;
    }
    .dropdown-menu{
        border-top: 0px solid #e89319!important
    }
    ul.dropdown-menu.fullwidth>li{
        /*background: #1d2863;*/background: #064460;
    }
   
.ad-space{
    max-height: 150px;
}
    .navbar-default{
        height: 52px;
        background: #1d2863;
        background: #064460;
        border-bottom: 3px solid #03A9F4;

        background: #009966;
        border-bottom: 3px solid #ffe422;

    }
    .embed-responsive_ {
    position: relative;
    display: block;
    width: 100%;
    padding: 0;
    overflow: hidden;
    border-radius: 5px;
}
.list-on-bar{
    padding: 0 15px;
    padding: 15px 0;
}
.list-on-bar li a{ display: block; }
.list-on-bar li:first-child{ 
     border-top: 1px solid #eee;
}
.list-on-bar li{
    list-style-type: none;
    padding: 10px 10px;
    font-weight: bold;
    border-bottom: 1px solid #eee;
}
/*.embed-responsive .embed-responsive-item, .embed-responsive iframe, .embed-responsive embed, .embed-responsive object, .embed-responsive video {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
}*/
.embed-responsive_ iframe{
    height: auto !important;
     border-radius: 5px;
}
.pad0{padding: 0px;}
.m0{margin: 0}
.bge{background: #eeeeee;}
.borderc{border: 1px solid #ccc}
.pad10{padding: 10px}
    .navbar-default .navbar-form{
        border: 0px solid #ffffff;
    }
    ul.dropdown-menu > li > a:focus, ul.dropdown-menu > li > a:hover{
        background: #03A9F4;
        background: #009966;
        color: #ffe422;
        transition: ease .2s all;
        border-left: 5px solid #ffe422;
    }
    .navbar-default .navbar-nav > li > a{color: #ffffff}
    @media (max-width: 767px){
        .navbar-default .navbar-nav > li > a{color: #333333;text-transform: uppercase;}
        .navbar-default .navbar-nav .open .dropdown-menu>li>a{color: #ffffff}
        .dropdown .caret{float: right;margin-right: 15px;margin-top: 15px}
        .navbar-default .navbar-toggle{border-color: #ffffff;border-radius: 0px}
   .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{background: #dedbe673;}
   .navbar-default .navbar-toggle .icon-bar{background: #ffffff;}
   .navbar-default{
    background: #ffffff;
    background: #009966;

   }
   fieldset.fieldset{
    margin-left: 0 !important;
   }
    }
  </style>

</head>
<body>
    <div class="navbar-fixed-top"></div>
    <div class="container-box">
        <!-- Headline Start -->
        <section id="newsticker">
            <div class="headline-wrapper">
                <div class="container">
                    <div class="row">
                        @if(isset($top_communiques) && $top_communiques->count())
                        <!-- Newsticker start -->
                        <div class="col-md-2 col-sm-3 col-xs-5">
                            <div class="headline-title">                
                                <h5>COMMUNIQUE</h5>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-9 col-xs-7 no-padding">
                            
                            <ul class="ticker clearfix">
                                @foreach($top_communiques as $c)
                                <li><a href="{{route('web.get_communique',$c->slug)}}">{{$c->titre}}.</a></li>
                                @endforeach
                                </ul>
                               
                        </div>
                        @else
                        <div class="col-md-7 col-sm-9 col-xs-7 no-padding"></div>
                         @endif
                        <!-- Social Icons Start -->
                        <div class="col-md-3 hidden-sm hidden-xs">
                            <div class="fa-icon-wrap">
                                <a class="facebook" target="_blanck" href="https://web.facebook.com/hiachupkou" data-toggle="tooltip" data-placement="left" title="Facebook"><i aria-hidden="true" class="fa fa-facebook"></i></a>
                                <a class="twitter"  target="_blanck" href="https://twitter.com/" data-toggle="tooltip" data-placement="left" title="Twitter"><i aria-hidden="true" class="fa fa-twitter"></i></a>
                                <a class="youtube" target="_blanck" href="https://youtube.com/" data-toggle="tooltip" data-placement="left" title="Youtube"><i aria-hidden="true" class="fa fa-youtube"></i></a>
                            </div>
                        </div><!-- Social Icons End -->
                    </div>
                </div>
            </div>
        </section><!-- Headline End -->
        
        <!-- Header Start -->
        <section class="header-wrapper clearfix">
            <div class="container pad0">
                <div class="row">
                    <div class="col-md-4 col-sm-3">
                        <h1 class="logo"><a href="{{url('/')}}"><img class="img-responsive" src="{{asset('assets/images/logo-hia.png')}}" alt="logo HIA-CHU Parakou"/></a></h1>
                    </div>
                    <div class="col-md-8 col-sm-9 pad0">
                        <div class="ad-space ads-768">
                            <img src="{{asset('assets/web/img/baniere-min.jpg')}}" alt="HIA"/>

                        </div>
                        <div class="ad-space ads-468">
                            <img src="{{asset('assets/web/img/baniere-min.jpg')}}" alt="HIA"/>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- Header End -->
        
        <!-- Menu Navigation Start -->
        <div class="navbar darks navbar-default megamenu clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#mainmenu" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{url('/')}}"><img class="img-responsive" src="{{asset('assets/images/logo-hia.png')}}" alt="HIA-CHU"/></a>
                        </div>
                        <div id="mainmenu" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="{{isset($page) && $page=='home'?'active':''}}"><a  href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                                <li class="dropdown {{isset($page) && $page=='presentation'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">L'Hôpital <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{route('web.getpage','presentation')}}"><i class="ti-plus"> </i>Historique et Présentation </a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.getpage','la-direction')}}">La direction</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.personnels','la')}}">L'équipe de direction</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.getpage','etudes-projets-et-rapports')}}">Etudes projets et Rapports</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.getpage','partenariats-et-jumelages')}}">Partenariats et Jumélages</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.getpage','organisation-des-travaux')}}">Organisation des travaux</a>
                                        </li>
                                        <li>
                                            <a href="{{url('galerie/images')}}">Galerie images</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown {{isset($page) && $page=='services'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.service_med_tech')}}"><i class="ti-plus"> </i>Services Médicotechniques </a>
                                        </li>
                                       <li>
                                            <a href="{{route('web.getpage','services-administratifs-et-financiers')}}"><i class="ti-plus"> </i>Services Administratifs et Financiers</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.getpage','assurances-et-partenaires')}}"><i class="ti-plus"> </i>Assurances et partenaires</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown full-cont {{isset($page) && $page=='actualite'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Actualités <b class="caret"></b></a>
                                    <!-- Mega Menu Start -->
                                     @if(isset($top4_actus))
                                    <ul class="dropdown-menu fullwidth">
                                        <li>
                                            <a class="btnbtn-default" href="{{route('web.actualites')}}">Toutes les actualités</a>
                                        </li>
                                        <li class="default clearfix">
                                            <div class="row">
                                                @foreach($top4_actus as $a)
                                                <div class="col-md-3 col-sm-6 mega-item">
                                                    <div class="img-thumb">
                                                        <a href="{{route('web.actualite',$a->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($a->photo)}}')"></div></a>
                                                        <div class="img-credits">
                                                            <h3>{{$a->titre}}</h3>
                                                            <div class="post-info">
                                                                <span>{{date('M d,Y',strtotime($a->updated_at))}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                
                                            </div>
                                        </li>

                                    </ul><!-- Mega Menu End -->
                                    @endif
                                </li>
                                <li class="dropdown {{isset($page) && $page=='infos'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Infos <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.getpage','informations-utiles')}}"><i class="ti-plus"> </i>Informations utiles </a>
                                        </li>
                                       <li>
                                            <a href="{{route('web.getpage','autres-informations')}}"><i class="ti-plus"> </i>Autres Informations</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown {{isset($page) && $page=='prmp'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">PRMP <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a target="_blanck" href="{{url('https://marches-publics.bj/index.php?option=com_detailrealisation&task=list&ref=559436&plangestion=2019&parentID=1&detail=1&planID=559393&autorite=527951&Itemid=90')}}">Plateforme SIGMAP</a>
                                        </li>
                                        <li >
                                            <a href="{{route('web.passation-marches','ppmp')}}">Plan de passation des marchés publics</a>
                                        </li>
                                         <li aria-haspopup="true">
                                            <a href="{{route('web.passation-marches','dc')}}">Demandes de cotation et autres</a>
                                        </li>
                                          <li aria-haspopup="true">
                                            <a href="{{route('web.passation-marches','apac')}}">Avis public d’appel à candidature</a>
                                        </li>
                                         <li aria-haspopup="true">
                                            <a href="{{route('web.passation-marches','pva')}}">PV d’attribution et autres</a>
                                        </li>
                                        <!--li >
                                            <a href="{{route('web.avis_attribution')}}">Avis d'attributions</a>
                                        </li-->
                                    </ul>
                                </li>
                                <!--li><a href="#">Sports</a></li-->
                                <li class="{{isset($page) && $page=='contact'?'active':''}}"><a href="{{url('/contact')}}">Contact</a></li>
                                
                            </ul>
                            <!-- Search Form start -->
                            <form class="navbar-form dark_ navbar-right" method="GET" action="{{route('web.search')}}" role="search">
                                {{csrf_field()}}
                                 {!! Form::text('search', null, ['class' => 'form-control form-control-sm','id'=>"search",'placeholder'=>"Rechercher..."]) !!}
                                
                                <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
                            </form><!-- Search Form end -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Menu Navigation End -->
        
        <!-- Featured Slide Start -->
        <main id="main" class="content">  
            <div class="containers">  
            @yield('content')
            </div>
        </main>
        <section class="footer-wrapper clearfix">
            <div class="container">
                <div class="row">
                    <!-- Footer Widgets Start -->
                    <div class="col-md-4 col-sm-6">
                        <!-- Text Widget Start -->
                        <div class="widget text-widget">
                            <div class="widget-title">
                                <h2>HIA-CHU Parakou</h2>
                            </div>
                                @if(isset($bcontact))
                                
                                {!!$bcontact->contenu!!}
                                @else
                                <p class="text-white">
                                    Hôpital d’Instruction des Armées-Centre Hospitalier Universitaire de Parakou.  <br>  
                                <b> Courrier Postal : </b>02  BP : 1324 Parakou-Bénin <br>      
                                <b>E-mail</b> : hiachu2@gmail.com <br>  
                                <b>Tel : </b>+229 90 97 20 12
                                </p>
                                @endif

                            <!-- Social Icons Start -->
                            <div class="fa-icon-wrap">
                                <a class="facebook" target="_blanck" href="https://web.facebook.com/hiachupkou"><i aria-hidden="true" class="fa fa-facebook"></i></a>
                                <a class="twitter" target="_blanck" href="https://twitter.com/"><i aria-hidden="true" class="fa fa-twitter"></i></a>
                                <a class="youtube" target="_blanck" href="https://youtube.com/"><i aria-hidden="true" class="fa fa-youtube"></i></a>
                            </div><!-- Social Icons End -->
                        </div><!-- Text Widget End -->
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <!-- Categories Widget Start -->
                        <div class="widget categories-widget">
                            <div class="widget-title">
                                <h2>A Découvrir</h2>
                            </div>
                            <ul class="category-list">
                                <li>
                                    <a href="{{route('web.getpage','presentation')}}"><i class="ti-plus"> </i><i class="fa fa-plus"></i> Présentation</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','la-direction')}}"><i class="fa fa-plus"></i> La direction </a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','etudes-projets-et-rapports')}}"><i class="fa fa-plus"></i> Etudes projets et Rapports</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','partenariats-et-jumelages')}}"><i class="fa fa-plus"></i> Partenariats et Jumélages</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','organisation-des-travaux')}}"><i class="fa fa-plus"></i> Organisation des travaux</a>
                                </li> 
                                <li>
                                    <a href="{{route('web.getpage','assurances-et-partenaires')}}"><i class="fa fa-plus"> </i>Assurances et partenaires</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','informations-utiles')}}"><i class="fa fa-plus"> </i> Informations utiles </a>
                                </li>
                                
                               </ul>
                        </div><!-- Categories Widget End -->
                    </div>
                    <div class="col-md-4 col-sm-6">
                         <div class="widget categories-widget">
                            <div class="widget-title">
                                <h2>Actualités et Services </h2>
                            </div>
                            <ul class="category-list">
                               
                                <li>
                                    <a href="{{route('web.breves')}}"><i class="fa fa-plus"></i> Brèves </a>
                                </li> 
                                <li>
                                    <a href="{{route('web.actualites')}}"><i class="fa fa-plus"></i> Actualités </a>
                                </li>  
                                <li>
                                    <a href="{{route('web.ressources')}}"><i class="fa fa-plus"></i> Ressources</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','avis-dattributions')}}"><i class="fa fa-plus"></i> Galerie images</a>
                                </li>
                              
                                <li>
                                    <a href="{{route('web.getpage','avis-dattributions')}}"><i class="fa fa-plus"></i> Avis d'attributions</a>
                                </li>
                                <li>
                                    <a href="{{route('web.getpage','demandes-de-cotation')}}"><i class="fa fa-plus"></i> Demandes de cotation</a>
                                </li>
                                
                                 <li>
                                    <a href="{{route('web.getpage','services-medicotechniques')}}"><i class="fa fa-plus"></i> Services Médicotechniques </a>
                                </li>
                               <li>
                                    <a href="{{route('web.getpage','services-administratifs-et-financiers')}}"><i class="fa fa-plus"></i> Services Administratifs et Finances</a>
                                </li>
                                 
                                
                            </ul>
                    </div>
                    </div>
                    
                </div><!-- Footer Widgets End -->
                <!-- Copyrights Start -->
                <div class="copyrights">
                    <div class="row">
                        <div class="col-md-6">
                            <p>COPYRIGHT &copy;  {{date('Y')}} HIA-CHU Parakou </p>
                        </div>
                        <div class="col-md-6">
                            <div class="fbt-footer-nav">
                                <ul>
                                    <li><a href="{{url('/')}}">Accueil</a></li>
                                    <li><a href="{{url('/admin')}}">Administration du site</a></li>
                                    <li><a href="{{url('/contact')}}">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Copyrights End -->
            </div>
        </section><!-- Footer Sidebar End -->
        
        <a href="#" id="BackToTop"><i class="fa fa-angle-up"></i></a>
    </div>
    <div class="navbar-fixed-bottom"></div>
    
    <!-- JAVASCRIPTS -->
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/jquery-1.12.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/smoothscroll.js')}}"></script>
    @yield('script')
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/cycle.all.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/resizesensor.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/theia-sticky-sidebar.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/functions/main.js')}}"></script>
</body>

</html>