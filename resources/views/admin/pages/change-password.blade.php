@extends('layouts.admin')
@section('title',isset($titre)?$titre:''." - Profil du compte")
@section('content')
<style>
	#head-profil{
		height: 230px;
		background: #fff
	}
	.img-border {
    border: 8px solid #edf5f3;
    box-shadow: 0px 0 5px #aaa;
}
.iborder{
	border:1px solid #dddddd;
}
#cover_title,#cover_title .text-muted,#cover_title p{
	color: #ffffff;
	text-shadow: 0 0 3px #000000;
}
#cover_titles{
	background: linear-gradient(to top,rgba(0,0,0,.8) 0,rgba(0,0,0,0) 100%);
	color: #ffffff;
	position: absolute;
	bottom: 0;
	width: 100%;
}
.nav-tabs .nav-link.active {
    border-left: 0.325rem solid #444bf8;
    border-top: 0 solid #444bf8;
    background: #f2f2f2;
    font-weight: 900;
    }
</style>
	<div class="row">

		@include('admin.includes.profil-top')
		<div class="col-lg-4">
                    <!-- .card -->
                    <div class="card card-fluid">
                      <h6 class="card-header"> Paramètres du compte </h6>
                      <!-- .nav -->
                      <nav class="nav nav-tabs flex-column text-uppercases">
                        <a href="{{route('profil',$compte->pseudo)}}" class="nav-link "><span class="ti-user"></span> Profile</a>
                        <a href="{{route('edit-profil',$compte->pseudo)}}" class="nav-link "><span class="ti-pencil"></span> Modifier mon compte </a>
                        <a href="{{route('password',$compte->pseudo)}}" class="nav-link active"><span class="ti-lock"></span>  Changer mot de passe</a>
                      </nav>
                      <!-- /.nav -->
                    </div>
                    <!-- /.card -->
        </div>
        <div class="col-lg-8">
                    <!-- .card -->
                      {!! Form::open([
                                        'method' => 'PATCH',
                                        'route' => ['update-password',$compte->pseudo],
                                        'class' => 'form-horizontal',
                                        'files' => true
                                    ]) !!}
                    <div class="card card-fluid">
                      <h6 class="card-header"> Modification du mot de passe </h6>
                      <!-- .card-body -->
                      <div class="card-body">
                        <!-- form -->
                        <form method="post">
                          {{ csrf_field() }}
                        <div class="row">   
                            <div class="col-md-12">   
                                <div class="form-group{{ $errors->has('lastpassword') ? ' has-error' : '' }}">
                                    <label for="nom" class="col-md-4control-label">Ancien mot de passe</label>

                                    <div class="col-md-6__">
                                        <input id="lastpassword" type="password" class="form-control form-control-sm " name="lastpassword"  required autofocus>

                                         @if ($errors->has('lastpassword'))
                                                <span class="help-block invalid-feed text-danger ">
                                                    <strong>{{ $errors->first('lastpassword') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>
                            </div>         
                            <div class="col-md-12">   
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="prenom" class="col-md-4control-label">Nouveau mot de passe</label>

                                        <div class="col-md-6__">
                                            <input id="nom" type="password" class="form-control form-control-sm brd1card" name="password" placeholder="*****************" required autofocus>

                                            @if ($errors->has('password'))
                                                <span class="help-block invalid-feed text-danger ">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div> 
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation" class="col-md-4control-label">Confirmation du nouveau mot de passe actuel</label>

                            <div class="col-md-6__">
                                <input id="password_confirmation" type="password" placeholder="*****************" class="form-control form-control-sm brd1card" name="password_confirmation" required>

                                @if ($errors->has('password'))
                                    <span class="help-block invalid-feed text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                          <!-- /.form-group -->
                          <hr>
                          <!-- .form-actions -->
                          <div class="form-actionss">
                            <button type="submit" class="btn btn-primary" >Modifier</button>
                          </div>
                          <!-- /.form-actions -->
                        </form>
                        <!-- /form -->
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                  </div>
		
		
		
	</div>
@endsection