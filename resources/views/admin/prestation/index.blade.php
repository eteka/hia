@extends('layouts.admin')

@section('title',"Prestation")
@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Prestations</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/prestation/') }}"> Prestations</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/prestation', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Prestations" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
 <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100 ">
                        <header class="card-header">
                           
                            <div class="row">
                                <div class="col-md-8">
                                 <h2 class="h3 card-header-title "> Les Prestations</h2>
                               
                                </div>
                                <div class="col-md-4 text-right">
                                <a href="{{ url('/admin/prestation/create') }}" class="btn btn-primary btn-sm btn-xs mt-0 pull-rights " id="addbtns" title="Ajouter un Prestation">
            <span class="ti-plus"></span> Ajouter
            </a>
                                
                                </div>
                        </header>
                        <div class="card-body pt-0">
                            <div class="table-responsive mt-3">
                            @if(isset($prestation) && $prestation->count())
                                <table class="table table-sm table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('prestation.titre') }}</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($prestation as $item)
                                        <?php
                                            $i++;
                                        ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td><b>{{ $item->titre }} </b><br>
                                            <?php
                                                $p=str_limit(strip_tags($item->contenu),120);
                                            ?>
                                            <small class="text-muted">{{$p}}</small><br>

                                                @if($item->categorie==1 )
                                              <span class="badge badge-success"> Services Médicotechniques  </span>
                                              @elseif($item->categorie==2 )
                                              <span class="badge badge-primary"> Services administratifs et financiers </span>
                                              @endif
                                         </td>
                                            
                                            <td width="10%" nowrap="nowrap">
                                                <a href="{{ url('/admin/prestation/' . $item->id) }}" title="Voir ce Prestation"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/prestation/' . $item->id . '/edit') }}" title="Modifier ce Prestation"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/prestation', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce Prestation',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach

                                 </tbody>
                                </table>
                                 <div class="pagination-wrapper"> {!! $prestation->appends(['search' => Request::get('search')])->render() !!} </div> 
                                @else($i==0)
                                   
                                    <div class="col-md-12 ">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="card-body borderc text-center text-muted">
                                            <h1 class="h1"><i class="ti-folder"></i></h1>
                                               <h4 class="text-muted">   Aucune Prestation disponible <br>   
                                               <small>  Les Prestations enrégistrées apparaissent ici</small></h4>  
                                               <a href="{{ url('/admin/prestation/create') }}" class="btn btn-outline-primary  btn-xs  btn-minimt-0 pull-rights " id="addbtns" title="Ajouter un Dossier de credit">
                                               <span class="ti-plus"></span> Ajouter  Prestation 
                                               </a>
                                            </div>
                                          </div>
                                    </div>
                                     </div>
                                @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
