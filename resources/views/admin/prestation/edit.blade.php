@extends('layouts.admin')
@section('title',"Modification de  Prestation")
@section('content')
<div >
       <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Prestations</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/prestation/') }}"> Prestations</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/prestation', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Prestations" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100">
                    <header class="card-header">
                     <div class="row">
                                <div class="col-md-8">
                                    <h2 class="h3 card-header-title">Edition </h2> 
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ url('/admin/prestation') }}" title="Back"><button class="btn btn-outline-primary btn-sm btn-xs mt20"><i class="ti-arrow-left" aria-hidden="true"></i> Retour</button></a>
                                </div>
                            </div>                           
                        </header>
                        <div class="card-body pt-3">
                                   
                                    <br />
                                    <br />

                                    @if ($errors->any())
                                        <ul class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif

                                    {!! Form::model($prestation, [
                                        'method' => 'PATCH',
                                        'url' => ['/admin/prestation', $prestation->id],
                                        'class' => 'form-horizontal',
                                        'files' => true
                                    ]) !!}

                                    @include ('admin.prestation.form', ['submitButtonText' => 'Mettre à jour'])

                                    {!! Form::close() !!}

                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
