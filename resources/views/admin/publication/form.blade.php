<div class="form-group row  {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('publication.titre'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', trans('publication.type'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('type', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('type', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('contenu') ? 'has-error' : ''}}">
    {!! Form::label('contenu', trans('publication.contenu'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('contenu', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('contenu', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('publication.image'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::file('image', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('image', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
