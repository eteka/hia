@extends('layouts.admin')

@section('title',"Publication")
@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Publications</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/publication/') }}"> Publications</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/publication', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Publications" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
 <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100 ">
                        <header class="card-header">
                           
                            <div class="row">
                                <div class="col-md-8">
                                 <h2 class="h3 card-header-title "> Les Publications</h2>
                               
                                </div>
                                <div class="col-md-4 text-right">
                                <a href="{{ url('/admin/publication/create') }}" class="btn btn-primary btn-sm btn-xs mt-0 pull-rights " id="addbtns" title="Ajouter un Publication">
            <span class="ti-plus"></span> Ajouter
            </a>
                                
                                </div>
                        </header>
                        <div class="card-body pt-0">
                            <div class="table-responsive mt-3">
                            @if(isset($publication) && $publication->count())
                                <table class="table table-sm table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <!--th>#</th--><th>{{ trans('publication.titre') }}</th><th>{{ trans('publication.type') }}</th><th>{{ trans('publication.contenu') }}</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($publication as $item)
                                        <?php
                                            $i++;
                                        ?>
                                        <tr>
                                            <!--td>{{ $item->id }}</td-->
                                            <td>{{ $item->titre }}</td><td>{{ $item->type }}</td><td>{{ $item->contenu }}</td>
                                            <!--td width='10%' nowrap="nowrap">
                                            <!-- Actions Menu >
                                              <div class="dropdown-menu dropdown-menu-right show" style="width: 150px; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-88px, -73px, 0px);" x-placement="top-end">
                                                  <div class="card border-0 p-3">
                                                      <ul class="list-unstyled mb-0">
                                                          <li class="mb-3">
                                                              <a href="{{ url('/admin/publication/' . $item->id) }}" title="View Publication"><button class="btn btn-info btn-xs"><i class="ti-eye" aria-hidden="true"></i> Voir</a>
                                                          </li>
                                                          <li class="mb-3">
                                                              a href="{{ url('/admin/publication/' . $item->id . '/edit') }}" title="Modifier Publication"><button class="btn btn-xs  btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                                          </li>
                                                          <li>
                                                              {!! Form::open([
                                                                'method'=>'DELETE',
                                                                'url' => ['/admin/publication', $item->id],
                                                                'style' => 'display:inline'
                                                            ]) !!}
                                                                {!! Form::button('<i class="ti-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                                        'type' => 'submit',
                                                                        'class' => 'btn btn-danger btn-xs',
                                                                        'title' => 'Suppression Publication',
                                                                        'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                                )) !!}
                                                            {!! Form::close() !!}
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                              <!-- End Actions Menu >

                                            </td-->
                                            <td width="10%" nowrap="nowrap">
                                                <a href="{{ url('/admin/publication/' . $item->id) }}" title="Voir ce Publication"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/publication/' . $item->id . '/edit') }}" title="Modifier ce Publication"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/publication', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce Publication',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach

                                 </tbody>
                                </table>
                                 <div class="pagination-wrapper"> {!! $publication->appends(['search' => Request::get('search')])->render() !!} </div> 
                                @else($i==0)
                                    <div class="cards mt-4 ">
                                    <div class="card-body text-center text-muted">
                                <h1 class="h1"><i class="ti-info-alt"></i></h1>
                                       <h4>   Zéro (0) Publication à afficher </h4>
                                    </div>
                                    </div>
                                @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
