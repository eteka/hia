@extends('layouts.admin')

@section('title',"Passation des marchés")
@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Passation des marchés</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/passation-marche/') }}"> Passation des marchés</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/passation-marche', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Passation des marchés" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
 <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100 ">
                        <header class="card-header">
                           
                            <div class="row">
                                <div class="col-md-8">
                                 <h2 class="h3 card-header-title "> Les Passations des marchés</h2>
                               
                                </div>
                                <div class="col-md-4 text-right">
                                <a href="{{ url('/admin/passation-marche/create') }}" class="btn btn-primary btn-sm btn-xs mt-0 pull-rights " id="addbtns" title="Ajouter un PassationMarche">
            <span class="ti-plus"></span> Ajouter
            </a>
                                
                                </div>
                        </header>
                        <div class="card-body pt-0">
                            <div class="table-responsive mt-3">
                            @if(isset($passationmarche) && $passationmarche->count())
                                <table class="table table-sm table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>{{ trans('passationmarche.titre') }}</th><th class="text-center">{{ trans('Etat') }}</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                        $cats=[
                                            "PPMP"=>"Plan de passation des marchés publics",
                                            "DC"=>"Demandes de cotations et autres",
                                            "APAC"=>"Avis public d'Appel à candidature",
                                            "PVA"=>"PV d'attribution et autres",
                                        ];
                                        $tbadges=[
                                        "PPMP"=>"badge-warning",
                                            "DC"=>"badge-info",
                                            "APAC"=>"badge-primary",
                                            "PVA"=>"badge-success",
                                        ];
                                    ?>
                                    @foreach($passationmarche as $item)
                                        <?php
                                            $i++;
                                            $type_p=isset($cats[$item->type])?$cats[$item->type]:'';
                                            $badge=isset($tbadges[$item->type])?$tbadges[$item->type]:'';
                                        ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td><b>{{ $item->titre }}</b>
                                                <p>
                                                    <a href="{{asset($item->fichier)}}" target="_blanck">{{asset($item->fichier)}}</a><br>
                                                    <span class="badge {{$badge}}">
                                                        {{$type_p}}
                                                    </span>
                                               <br>
                                                <small class="text-muted text-sm">{{date('d/m/Y H\h:i\m\i\n :s\s',strtotime($item->created_at))}}</small> 
                                            </p>

                                           <td class="text-center">

                                                @if($item->etat==1 )
                                              <span class="badge badge-success"> Activé </span>
                                              @else
                                              <span class="badge badge-danger"> Désactivé </span>
                                              @endif
                                            </td>
                                            
                                            </td>
                                            
                                            <td width="10%" nowrap="nowrap">
                                                <a href="{{ url('/admin/passation-marche/' . $item->id) }}" title="Voir ce PassationMarche"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/passation-marche/' . $item->id . '/edit') }}" title="Modifier ce PassationMarche"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/passation-marche', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce PassationMarche',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach

                                 </tbody>
                                </table>
                                 <div class="pagination-wrapper"> {!! $passationmarche->appends(['search' => Request::get('search')])->render() !!} </div> 
                                @else($i==0)
                                   
                                    <div class="col-md-12 ">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="card-body borderc text-center text-muted">
                                            <h1 class="h1"><i class="ti-shopping-cart-full"></i></h1>
                                               <h4 class="text-muted">   Aucun fichier disponible <br>   
                                               <small>  Les fichiers de passation des marchés apparaissent ici</small></h4>  
                                               <a href="{{ url('/admin/passation-marche/create') }}" class="btn btn-outline-primary  btn-xs  btn-minimt-0 pull-rights " id="addbtns" title="Ajouter un Dossier de credit">
                                               <span class="ti-plus"></span> Ajouter  un fichier 
                                               </a>
                                            </div>
                                          </div>
                                    </div>
                                     </div>
                                @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
