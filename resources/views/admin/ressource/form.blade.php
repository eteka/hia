<div class="form-group row  {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('ressource.titre'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', trans('ressource.code'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('code', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('code', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('fichier') ? 'has-error' : ''}}">
    {!! Form::label('fichier', trans('ressource.fichier'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::file('fichier',  ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('fichier', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('actualite.etat'), ['class' => 'col-md-2 text-right control-label']) !!} :
    <div class="col-md-9">
        <div class="row">
 <div class="col-md-2">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '1', true,['id'=>'etat_i','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_i'>Activé </label>
        </div>
</div>

 <div class="col-md-2">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '0', false,['id'=>'etat_j','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_j'>Désactivé </label>
        </div>
</div>
</div>
<!--div class="form-group row  {{ $errors->has('format') ? 'has-error' : ''}}">
    {!! Form::label('format', trans('ressource.format'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('format', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('format', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div-->


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
