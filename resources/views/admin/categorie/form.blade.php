<div class="form-group row  {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('categorie.nom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('nom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('nom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row  {{ $errors->has('detail') ? 'has-error' : ''}}">
    {!! Form::label('detail', trans('categorie.detail'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::textarea('detail', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('detail', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
