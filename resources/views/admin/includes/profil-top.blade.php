<style>
  #gradient_cover{
    background: linear-gradient(to top,rgba(0,0,0,.8) 0,rgba(0,0,0,0) 100%);
  color: #ffffff;
  }
</style>
<div class="col-sm-12 mb-3" id="head-profil ">
			<div class="card card-fluid">
				<div class="card-bod" style="background: url({{asset($compte->couverture)}}) no-repeat 0 center;background-size: cover;">
					<header class="page-cover" id="gradient_cover" ><br>
              <div class="text-center mt-2">
                <a href="{{route('profil',$compte->pseudo)}}" class="user-avatar user-avatar-xl">
                	@if(!empty(Auth::user()->image))
                  <img class="u-header-avatamg-fluid rounded-circle mr-md-3 img-border" height="150px" src="{{asset(Auth::user()->image)}}" alt="{{Auth::user()->prenom}}">

				@else
				<img class="u-header-avatamg-fluid rounded-circle mr-md-3 img-border" height="150px" src="{{asset('uploads/statics/default-image.jpg')}}" alt="{{Auth::user()->prenom}}">
				@endif
                </a>
                <div id="cover_title">
                <h2 class="h3 bold mt-2 mb-0"> {{$compte->prenom." ".$compte->nom}} </h2>
                
                <div class=""> 
                	
                <div> {{$compte->poste}} </div>
                <span class="text-muted_ badge badge-success">{{$compte->entreprise}} </span>
            	</div>
              </div>
              </div><br>
            </header>
				</div>
			</div>
			
		</div>