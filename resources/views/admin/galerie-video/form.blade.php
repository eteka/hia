<div class="form-group row  {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('galerievideo.titre'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row  {{ $errors->has('url_video_youtube') ? 'has-error' : ''}}">
    {!! Form::label('url_video_youtube', trans('galerievideo.url_video_youtube'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::textarea('url_video_youtube', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('url_video_youtube', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('galerievideo.description'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::textarea('description', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('description', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('galerievideo.etat'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        <div class="row">
 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '1', true,['id'=>'etat_i','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_i'>Activé </label>
        </div>
</div>

 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '0', false,['id'=>'etat_j','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_j'>Désactivé </label>
        </div>
</div>
</div>

        {!! $errors->first('etat', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
