@extends('layouts.admin')

@section('title',"Communique")
@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Communiques</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/communique/') }}"> Communiques</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/communique', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Communiques" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
 <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100 ">
                        <header class="card-header">
                           
                            <div class="row">
                                <div class="col-md-8">
                                 <h2 class="h3 card-header-title "> Les Communiques</h2>
                               
                                </div>
                                <div class="col-md-4 text-right">
                                <a href="{{ url('/admin/communique/create') }}" class="btn btn-primary btn-sm btn-xs mt-0 pull-rights " id="addbtns" title="Ajouter un Communique">
            <span class="ti-plus"></span> Ajouter
            </a>
                                
                                </div>
                        </header>
                        <div class="card-body pt-0">
                            <div class="table-responsive mt-3">
                            @if(isset($communique) && $communique->count())
                                <table class="table table-sm table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <th>#</th><th>{{ trans('communique.titre') }} </th><th class="text-center">{{ trans('communique.etat') }} </th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($communique as $item)
                                        <?php
                                            $i++;
                                            $contenu=strip_tags($item->contenu);
                                           // (str)
                                        ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td width="70%"><b> {{ $item->titre }}</b> <br> <small class="text-muted"> {{ substr($contenu,0,150)}}</small>
                                            </td>
                                            <td class="text-center">
                                              @if($item->etat==1 )
                                              <span class="badge badge-success"> Activé </span>
                                              @else
                                              <span class="badge badge-danger"> Désactivé </span>
                                              @endif
                                            </td>
                                            <!--td width='10%' nowrap="nowrap">
                                            <!-- Actions Menu >
                                              <div class="dropdown-menu dropdown-menu-right show" style="width: 150px; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-88px, -73px, 0px);" x-placement="top-end">
                                                  <div class="card border-0 p-3">
                                                      <ul class="list-unstyled mb-0">
                                                          <li class="mb-3">
                                                              <a href="{{ url('/admin/communique/' . $item->id) }}" title="View Communique"><button class="btn btn-info btn-xs"><i class="ti-eye" aria-hidden="true"></i> Voir</a>
                                                          </li>
                                                          <li class="mb-3">
                                                              a href="{{ url('/admin/communique/' . $item->id . '/edit') }}" title="Modifier Communique"><button class="btn btn-xs  btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                                          </li>
                                                          <li>
                                                              {!! Form::open([
                                                                'method'=>'DELETE',
                                                                'url' => ['/admin/communique', $item->id],
                                                                'style' => 'display:inline'
                                                            ]) !!}
                                                                {!! Form::button('<i class="ti-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                                        'type' => 'submit',
                                                                        'class' => 'btn btn-danger btn-xs',
                                                                        'title' => 'Suppression Communique',
                                                                        'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                                )) !!}
                                                            {!! Form::close() !!}
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                              <!-- End Actions Menu >

                                            </td-->
                                            <td width="10%" nowrap="nowrap">
                                                <a href="{{ url('/admin/communique/' . $item->id) }}" title="Voir ce Communique"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/communique/' . $item->id . '/edit') }}" title="Modifier ce Communique"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/communique', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce Communique',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach

                                 </tbody>
                                </table>
                                 <div class="pagination-wrapper"> {!! $communique->appends(['search' => Request::get('search')])->render() !!} </div> 
                                @else($i==0)
                                   
                                    <div class="col-md-12 ">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="card-body borderc text-center text-muted">
                                            <h1 class="h1"><i class="ti-flag"></i></h1>
                                               <h4 class="text-muted">   Aucun Communiqué disponible <br>   
                                               <small>  Les Communiques enrégistrées apparaissent ici</small></h4>  
                                               <a href="{{ url('/admin/communique/create') }}" class="btn btn-outline-primary  btn-xs  btn-minimt-0 pull-rights " id="addbtns" title="Ajouter un Dossier de credit">
                                               <span class="ti-plus"></span> Ajouter un  Communiqué 
                                               </a>
                                            </div>
                                          </div>
                                    </div>
                                     </div>
                                @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
