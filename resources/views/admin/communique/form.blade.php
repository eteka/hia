@section('css')
<link href="{{asset('assets/plugins/summernote/dist/summernote.css')}}" rel="stylesheet">
<style type="text/css">
.note-popover .popover-content, .panel-heading.note-toolbar{
    background: #eaeff5;
} 
</style>
@endsection
@section('script')
<script src="{{asset('assets/plugins/summernote/dist/summernote.js')}}"></script>

<script type="text/javascript">
var $editor = $('.summernote');

$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
//    height: 250,
    height: 200,
    placeholder: 'Rédigez votre communiqué',
    dialogsInBody: true,
    lang: 'fr-FR',
});


</script>

@endsection
<div class="form-group row  {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('communique.titre'), ['class' => 'col-md-2 text-right control-label']) !!} : 
    <div class="col-md-9">
        {!! Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row  {{ $errors->has('contenu') ? 'has-error' : ''}}">
    {!! Form::label('contenu', trans('communique.contenu'), ['class' => 'col-md-2 text-right control-label']) !!} :
    <div class="col-md-9">
        {!! Form::textarea('contenu', null, ['class' => 'form-control form-control-sm summernote', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('contenu', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row  {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('communique.etat'), ['class' => 'col-md-2 text-right control-label']) !!} :
    <div class="col-md-5">
        <div class="row">
 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '1', false,['id'=>'etat_i','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_i'>Activé </label>
        </div>
</div>

 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '0', false,['id'=>'etat_j','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_j'>Désactivé </label>
        </div>
</div>
</div>

        {!! $errors->first('etat', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-2">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
