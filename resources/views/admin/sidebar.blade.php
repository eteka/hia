

<nav class="u-sidebar-nav">
            <!-- Sidebar Nav Menu -->
            <ul class="u-sidebar-nav-menu u-sidebar-nav-menu--top-level">
              <!-- Dashboard -->
              <!--li class="u-sidebar-nav-menu__item">
                <a class="u-sidebar-nav-menu__link active" href="index.html">
                  <span class="ti-dashboard u-sidebar-nav-menu__item-icon"></span>
                  <span class="u-sidebar-nav-menu__item-title">Dashboard</span>
                </a>
              </li-->
              <?php
                $p=isset($page)?$page:'-';
              ?>
             @foreach($laravelAdminMenus->menus as $section)
        
              @if($section->items && Auth::user())
         
            <!--h5 class="duik-sidebar__heading text-muted">{{ $section->section }}</h5--> 
                          
              @foreach($section->items as $menu)
               
                <!--a class="duik-sidebar__link " href="{{url($menu->url)}}"> <i class=" {{ (isset($menu->icone))?$menu->icone:'' }}"></i> {{ $menu->title }} {!! $section=='Modules'?'<span class="fa fa-chevron-right pull-right"></span>':''!!}</a-->

                <li class="u-sidebar-nav-menu__item">
                <a class="u-sidebar-nav-menu__link {{$p==$menu->id?'active':''}}" id="{{url($menu->url)}}" href="{{url($menu->url)}}"
                   >
                  <span class="{{ (isset($menu->icone))?$menu->icone:'' }} u-sidebar-nav-menu__item-icon"></span>
                  <span class="u-sidebar-nav-menu__item-title">{{ $menu->title }} {!! $section=='Modules'?'<span class="ti-angle-down u-sidebar-nav-menu__item-arrow">':''!!}</span>
                  </span>
                </a>
              </li>
              @endforeach
            </ul>
      @endif
    @endforeach
            </ul>
            <!-- End Sidebar Nav Menu -->
          </nav>
