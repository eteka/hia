@extends('layouts.admin')
@section('title',"Tableau de bord")
@section('content')
<div class="row">
	<div class="col-sm-6 col-xl-3 mb-5">
		<!-- Card -->
		<div class="card">
			<!-- Card Body -->
			<div class="card-body">
				<!-- Chart with Info -->
				<div class="media align-items-center py-2">
					<!-- Chart with Info - Info -->
					<div class="media-body">
						<h5 class="h5 text-muted mb-2">Total des actualités</h5>
						<span class="h2 font-weight-normal mb-0">{{isset($nb_actu)?$nb_actu:0}}</span>
					</div>
					<!-- End Chart with Info - Info -->

					<!-- Chart with Info - Chart -->
					<div class="text-right ml-2" style="max-width: 70px;">
						<div class="mb-2">
							<canvas class="js-area-chart-small"
							        width="100"
							        height="20"
							        data-extend='[{
												"data": {{isset($courb1)?$courb1:[]}},
              "borderColor": "#444bf8"
											}]'></canvas>
						</div>

						<span class="text-success">
							+5.2%
							<span class="ti-arrow-up"></span>
						</span>
					</div>
					<!-- End Chart with Info - Chart -->
				</div>
				<!-- End Chart with Info -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Card -->
	</div>

	<div class="col-sm-6 col-xl-3 mb-5">
		<!-- Card -->
		<div class="card">
			<!-- Card Body -->
			<div class="card-body">
				<!-- Chart with Info -->
				<div class="media align-items-center py-2">
					<!-- Chart with Info - Info -->
					<div class="media-body">
						<h5 class="h5 text-muted mb-2">Images ajoutées</h5>
						<span class="h2 font-weight-normal mb-0">{{isset($nb_g_image)?$nb_g_image:0}}</span>
					</div>
					<!-- End Chart with Info - Info -->

					<!-- Chart with Info - Chart -->
					<div class="text-right ml-2" style="max-width: 70px;">
						<div class="mb-2">
							<canvas class="js-area-chart-small"
							        width="100"
							        height="20"
											data-extend='[{
												"data": {{isset($courb2)?$courb2:[]}},
              "borderColor": "#2cd2f6"
											}]'></canvas>
						</div>

						<span class="text-success">
							+9.0%
							<span class="ti-arrow-up"></span>
						</span>
					</div>
					<!-- End Chart with Info - Chart -->
				</div>
				<!-- End Chart with Info -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Card -->
	</div>

	<div class="col-sm-6 col-xl-3 mb-5">
		<!-- Card -->
		<div class="card">
			<!-- Card Body -->
			<div class="card-body">
				<!-- Chart with Info -->
				<div class="media align-items-center py-2">
					<!-- Chart with Info - Info -->
					<div class="media-body">
						<h5 class="h5 text-muted mb-2">Messages reçus</h5>
						<span class="h2 font-weight-normal mb-0">{{isset($nb_message)?$nb_message:0}}</span>
					</div>
					<!-- End Chart with Info - Info -->

					<!-- Chart with Info - Chart -->
					<div class="text-right ml-2" style="max-width: 70px;">
						<div class="mb-2">
							<canvas class="js-area-chart-small"
							        width="100"
							        height="20"
							        data-extend='[{
												"data": {{isset($courb3)?$courb3:[]}},
              "borderColor": "#f12559"
											}]'></canvas>
						</div>

						<span class="text-danger">
							-4.2%
							<span class="ti-arrow-down"></span>
						</span>
					</div>
					<!-- End Chart with Info - Chart -->
				</div>
				<!-- End Chart with Info -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Card -->
	</div>

	<div class="col-sm-6 col-xl-3 mb-5">
		<!-- Card -->
		<div class="card">
			<!-- Card Body -->
			<div class="card-body">
				<!-- Chart with Info -->
				<div class="media align-items-center py-2">
					<!-- Chart with Info - Info -->
					<div class="media-body">
						<h5 class="h5 text-muted mb-2">Ressources</h5>
						<span class="h2 font-weight-normal mb-0">{{isset($nb_rs)?$nb_rs:0}}</span>
					</div>
					<!-- End Chart with Info - Info -->

					<!-- Chart with Info - Chart -->
					<div class="text-right ml-2" style="max-width: 70px;">
						<div class="mb-2">
							<canvas class="js-area-chart-small"
							        width="100"
							        height="20"
							        data-extend='[{
												"data": {{isset($courb4)?$courb4:[]}},
              "borderColor": "#f1be25"
											}]'></canvas>
						</div>

						<span class="text-success">
							+6.2%
							<span class="ti-arrow-up"></span>
						</span>
					</div>
					<!-- End Chart with Info - Chart -->
				</div>
				<!-- End Chart with Info -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Card -->
	</div>
</div>
<!-- End Doughnut Chart -->

<div class="row">
	<div class="col-md-6 mb-5">
		<!-- Revenue Chart -->
		<div class="card h-100">
			<!-- Card Header -->
			<header class="card-header d-flex align-items-center justify-content-between">
				<h2 class="h4 card-header-title">Evolutions des contenus</h2>

				<!-- Card Icons -->
				<ul class="list-inline mb-0">
					<li class="list-inline-item dropdown">
						<a id="revenueMenuInvoker" class="u-icon-sm link-muted" href="#" role="button" aria-haspopup="true" aria-expanded="false"
						   data-toggle="dropdown"
						   data-offset="8">
							<span class="ti-more"></span>
						</a>

						<!-- Card Menu -->
						<!--div class="dropdown-menu dropdown-menu-right" aria-labelledby="revenueMenuInvoker" style="width: 150px;">
							<div class="card border-0 p-3">
								<ul class="list-unstyled mb-0">
									<li class="mb-3">
										<a class="d-block link-dark" href="#">Add</a>
									</li>
									<li>
										<a class="d-block link-dark" href="#">Remove</a>
									</li>
								</ul>
							</div>
						</div-->
						<!-- Card Menu -->
					</li>
				</ul>
				<!-- End Card Icons -->
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body pt-0">
				<!-- Chart Legends -->
				<ul class="list-inline mb-4">
					<li class="list-inline-item d-inline-flex align-items-center mr-4">
						<span class="u-indicator u-indicator-xxs position-static bg-primary border-0 mr-2"></span>
						Actualités
					</li>
					<li class="list-inline-item d-inline-flex align-items-center mr-4">
						<span class="u-indicator u-indicator-xxs position-static bg-success border-0 mr-2"></span>
						Fichiers
					</li>
				</ul>
				<!-- End Chart Legends -->

				<!-- Chart -->
				<div class="mx-n3" style="height: 340px;">
					<canvas class="js-area-chart"
					        width="100"
					        height="320"></canvas>
				</div>
				<!-- End Chart -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Revenue Chart -->
	</div>

	<div class="col-md-6 mb-5">
		<!-- Performance Chart -->
		<div class="card h-100">
			<!-- Card Header -->
			<header class="card-header d-flex align-items-center justify-content-between">
				<h2 class="h4 card-header-title">Types de contenus</h2>

				<!-- Card Icons -->
				<ul class="list-inline mb-0">
					<li class="list-inline-item dropdown">
						<a id="performanceMenuInvoker" class="u-icon-sm link-muted" href="#" role="button" aria-haspopup="true" aria-expanded="false"
						   data-toggle="dropdown"
						   data-offset="8">
							<span class="ti-more"></span>
						</a>

						<!-- Card Menu -->
						<!--div class="dropdown-menu dropdown-menu-right" aria-labelledby="performanceMenuInvoker" style="width: 150px;">
							<div class="card border-0 p-3">
								<ul class="list-unstyled mb-0">
									<li class="mb-3">
										<a class="d-block link-dark" href="#">Add</a>
									</li>
									<li>
										<a class="d-block link-dark" href="#">Remove</a>
									</li>
								</ul>
							</div>
						</div-->
						<!-- Card Menu -->
					</li>
				</ul>
				<!-- End Card Icons -->
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body">
				<!-- Chart -->
				<div class="mx-auto mb-6" style="max-width: 240px; max-height: 240px;">
					<canvas class="js-doughnut-chart"
					        width="240"
					        height="240"></canvas>
				</div>
				<!-- End Chart -->

				<!-- Chart Legends -->
				<ul class="list-inline d-flex align-items-center justify-content-center text-center mb-0">
					<li class="list-inline-item px-5 mr-0">
						<div class="h2 font-weight-normal text-primary mb-1">{{isset($taux_actu)?$taux_actu:0}}%</div>
						<div class="text-muted">Actualités</div>
					</li>
					<li class="list-inline-item px-5 mr-0">
						<div class="h2 font-weight-normal text-info mb-1">{{isset($taux_image)?$taux_image:0}}%</div>
						<div class="text-muted">Images</div>
					</li>
					<li class="list-inline-item px-5 mr-0">
						<div class="h2 font-weight-normal text-success mb-1">{{isset($taux_fichiers)?$taux_fichiers:0}}%</div>
						<div class="text-muted">Fichiers</div>
					</li>
					<li class="list-inline-item px-5 mr-0">
						<div class="h2 font-weight-normal text-danger mb-1">{{isset($taux_msg)?$taux_msg:0}}%</div>
						<div class="text-muted">Message</div>
					</li>
				</ul>
				<!-- End Chart Legends -->
			</div>
			<!-- End Card Body -->
		</div>
		<!-- End Performance Chart -->
	</div>

	
	
</div>

@endsection