<div class="form-group row  {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('galerieimage.image'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::file('image',  ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('image', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('titre') ? 'has-error' : ''}}">
    {!! Form::label('titre', trans('galerieimage.titre'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('titre', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('titre', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row  {{ $errors->has('detail') ? 'has-error' : ''}}">
    {!! Form::label('detail', trans('galerieimage.detail'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::textarea('detail', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('detail', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('etat') ? 'has-error' : ''}}">
    {!! Form::label('etat', trans('galerieimage.etat'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        <div class="row">
 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '1', true,['id'=>'etat_i','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_i'>Activé </label>
        </div>
</div>

 <div class="col-md-3">
        <div class="custom-control custom-radio ">
        {!! Form::radio('etat', '0', false,['id'=>'etat_j','class'=>"custom-control-input type_sit"]) !!}
          <label class="custom-control-label" for='etat_j'>Désactivé </label>
        </div>
</div>
</div>

        {!! $errors->first('etat', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
