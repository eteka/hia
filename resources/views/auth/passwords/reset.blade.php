@extends('layouts.app')

@section('content')
     <div class="container py-11 my-auto">
                <div class="row align-items-center">
                    
                    <div class="col-md-6 col-lg-6 offset-lg-3 mb-4 mb-md-0">
                        <!-- Card -->
                        <div class="card">
                            <!-- Card Body -->
                            <div class="card-body p-4 p-lg-7">
                               
                                <h2 class="text-centers mb-4">Réinitialisation de mot de passe</h2>

                                <div class="panel panel-default">

                                <div class="panel panel-default">
                
                                    <div class="panel-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="token" value="{{ $token }}">

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4control-label">Adresse email</label>

                                                <div class="col-md-6__">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block text-danger">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="col-md-4control-label">Mot de passe</label>

                                                <div class="col-md-6__">
                                                    <input id="password" type="password" class="form-control" name="password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="help-block text-danger">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="password-confirm" class="col-md-4 control-label">Confirmer le mot de passe</label>
                                                <div class="col-md-6__">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block text-danger">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6__col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Réinitialiser le mot de passe
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Card Body -->
                        </div>
                        <!-- End Card -->
                    </div>

                    
                </div>
            </div>


@endsection
