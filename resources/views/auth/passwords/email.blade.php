@extends('layouts.login')
@section('title','Réinitialisation du mot de passe - Mot de passe oublié')
@section('content')

            <div class="container py-11 my-auto">
                <div class="row align-items-center">
                    
                    <div class="col-md-6 col-lg-6 offset-lg-3 mb-4 mb-md-0">
                        <!-- Card -->
                        <div class="card brd2card">
                            <!-- Card Body -->
                            <div class="card-body p-4 p-lg-7">
                               
                                <h2 class="text-centers mb-4">Mot de passe oublié</h2>

                                <div class="panel panel-default">

                                <div class="panel-body">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4control-label">Votre adresse email</label>

                                            <div class="col-md-6__">
                                                <input id="email" type="email" placeholder="email@domain.com" class="form-control form-control-sm brd1card" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block text-danger">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6col-md-offset-4">
                                                <button type="submit" class="btn btn-block btn-primary">
                                                   Envoyer le lien de réinitialisation
                                                </button>
                                            </div>
                                        </div>
                                       <div class="form-group text-center">
                                    <a class="btn btn-link mt-2  text-muted"  href="{{route('register')}}">
                                    Créer un compte
                                    </a>
                                <a class="btn btn-link text-muted btn-sm mt-2" href="{{ route('login') }}">
                                   Se connecter
                                </a>
                        </div>
                        <!-- End Card -->
                    </div>
                                    </form>
                                </div>
                            </div>
                            </div>
                            <!-- End Card Body -->
                        </div>
                        <!-- End Card -->
                    </div>

                    
                </div>
            </div>

  

@endsection
