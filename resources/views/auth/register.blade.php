@extends('layouts.login')

@section('title','Création de compte')
@section('content')
<div id="bg">
        <div class="container-fluid py-11 my-auto">
        
                <div class="row align-items-top">
                    
                    <div class="col-md-4 col-lg-4 offset-lg-4  mb-md-0">
                        <!-- Card -->
                        <div class="card brd2card mb-4">
                            <!-- Card Body -->
                            <div class="card-body p-4 p-lg-6">

                                <h2 class="text-center text-noir mb-4">Inscription</h2>

                                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">   
                            <div class="col-md-12">   
                                <div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
                                    <label for="nom" class="col-md-4control-label">Nom de famille</label>

                                    <div class="col-md-6__">
                                        <input id="nom" type="text" class="form-control form-control-sm brd1card" name="nom" value="{{ old('nom') }}" required autofocus>

                                        @if ($errors->has('nom'))
                                            <span class="help-block invalid-feed text-danger ">
                                                <strong>{{ $errors->first('nom') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>         
                            <div class="col-md-12">   
                                    <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
                                        <label for="prenom" class="col-md-4control-label">Prénom(s)</label>

                                        <div class="col-md-6__">
                                            <input id="nom" type="text" class="form-control form-control-sm brd1card" name="prenom" value="{{ old('nom') }}" required autofocus>

                                            @if ($errors->has('prenom'))
                                                <span class="help-block invalid-feed text-danger ">
                                                    <strong>{{ $errors->first('prenom') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div> 
                            </div>
                        </div>
                        
                        
                        <!--div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                           
                           <fieldset class="text-small mb-2">   

                                        <legend class="text-noir h5">Type de compte</legend>
                                        <div class="card-body pad10">
                                                <div class="row">                              
                                    
                                                        <div class="col-md-5 text-center">
                                                                <div class="custom-control custom-radio ">
                                                                    {!! Form::radio('type', 'PME', false,['id'=>'type1','class'=>"custom-control-input type_sit"]) !!}
                                                                    <label class="custom-control-label etat" for="type1">Promoteur</label>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-7 ">
                                                                <div class="custom-control custom-radio ">
                                                                    {!! Form::radio('type', 'SFD', false,['id'=>'type2','class'=>"custom-control-input type_sit"]) !!}
                                                                    <label class="custom-control-label etat" for="type2">Institution financière</label>
                                                                </div>
                                                        </div>
                                                            {!! $errors->first('type', '<p class="form-text text-danger help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                           </fieldset-->

                            

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4control-label">Email</label>

                            <div class="col-md-6__">
                                <input id="email" type="email" class="form-control form-control-sm brd1card" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block invalid-feed text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4control-label">Mot de passe</label>

                            <div class="col-md-6__">
                                <input id="password" type="password" placeholder="*****************" class="form-control form-control-sm brd1card" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block invalid-feed text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4control-label">Confirmation </label>

                            <div class="col-md-6__">
                                <input id="password-confirm" type="password" placeholder="*****************" class="form-control form-control-sm brd1card" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6_ col-md-offset-0">
                                <button type="submit" class="btn btn-block btn-primary">
                                   Créer un compte
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- Sign in Form -->
                    <div class="form-group text-center">
                        <a class="btn btn-link mt-2  text-muted"  href="{{ route('password.request') }}">
                                    Mot de passe oublié ?
                                </a>
                                <a class="btn btn-link text-muted btn-sm mt-2" href="{{ route('login') }}">
                                    Connexion !
                                </a>
                        </div>
                        <!-- End Card -->
                    </div>

                                <!-- End Sign in Form -->
                            </div>

                            <!-- End Card Body -->
                        </div>
                        


                </div>
            </div>
            </div>



@endsection
